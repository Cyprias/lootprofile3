# LootProfile3.js, Copyright 2015	cyprias@gmail.com

LootProfile allows you to create profiles with a list of rules for an item to meet. Additionally some rules will calculate the number of tinks required to reach a value allowing you to create profiles that factor in potential tinked stats. 

## Installation
- Install Decal. [decaldev.com](https://www.decaldev.com)
- Install SkunkWorks for Asheron's Call. [SkunkWorks35-500.exe](https://sourceforge.net/projects/skunkworks/files/SkunkWorks/3.5/SkunkWorks35-500.exe/download)
- Download updated SkunkWorks skapi.dll file. [SkunkWorks3.5.509.zip](https://sourceforge.net/projects/skunkworks/files/SkunkWorks/3.5/SkunkWorks3.5.509.zip/download)
- Extract SkunkWorks3.5.509.zip to your SkunkWorks directory, overwriting the existing skapi.dll file.
- Download [LootProfile3](https://gitlab.com/Cyprias/lootprofile3/tags) and extract it to your SkunkWorks directory. 
- Log into Asheron's Call, click the SkunkWorks icon on your Decal bar and select LootProfile3.swx from the dropdown menu. You may need to use the browser [...] button to locate it.


## Standalone usage
#### Main Panel, Profiles tab. 
![Main panel](doc/MainPanel.jpg)

Displays a list of loaded profiles with their enabled, status, icon, name and delete button. 
Clicking [New] will open the profile editor to create a new profile. 
Clicking [Select] will print a string of stats for your selected item and a list of matching profiles.

#### Main Panel, Detection Tab
![Detection Tab](doc/DetectionTab.jpg)

LP3 can scan corpses, containers and trade windows for items matching enabled profiles and list them in the Detection tab. Clicking the list will select the item and print the matching profile(s) and a string of stats.


#### Main Panel, Tink tab. 
![Tink Tab](doc/TinkTab.jpg)

Input boxes for your tinkering skills, chance threshold and salvage workmanship. Used for tink prediction. 

#### Profile Edtitor, Rules tab
![Profile Editor - rules](doc/ProfileEdtior_rules.jpg)

Editor panel to change the name of a profile, change icon button. 
Rules tab shows the list of rules for the profile along with a enable button and delete button. Clicking the text area will open the Modify tab. 
Clicking [Eval] will evaulate your selected item against all the rules and tell you if your item meets all the requirments or which rule(s) it fails on including if the number of tinks is unachievable. 

#### Profile Edtitor, Add tab
![Profile Editor - add](doc/ProfileEdtior_add.jpg)

To add a rule, simply select a property from the drop down menu, a operator type (==, <, =>, ect) and input a value to compare against. Some properties use raw values like skill IDs, for those the Suggestion menu shows a list of names which when selected will input the proper value into the Value box. Alternatively you can click the [Selected] button and whatever item you have selected will have its value input into the value box. 

#### Profile Edtitor, Config tab
![Profile Editor - config](doc/ProfileEdtior_config.jpg)

Profile specific config settings. At the moment only max tink count in order to meet all the rules. 


#### Inventory Search
![Inventory Search](doc/InventorySearch.png)  

Inventory search will search your character(s) inventory for items that match a selected profile.


## Properties
LootProfile contains every item property available (value, burden, icon, armor value, damage amount, spells, ect) and some more complex properties like 

- Armor colour palettes (Raw ID, RGB, Heu, Saturation & Lightness). For RGB there's a special operator for colour variance where you input a target RGB and a variance value and it'll determin if it's a close enough match. 

## API Reference
LootProfile has API functions to get profile data when accessed via an external script.
TODO: add example.

## License
MIT License	(http://opensource.org/licenses/MIT)
/*
	http://asheron.wikia.com/wiki/Combat
	
	Animation times were done on a lvl 5 with 100 quickness and swiftkiller for 0 speed weapons. 
	Characers with higher quickness will see quicker animations, so it's important we test using the same quickness. 
	Or, get a baseline using your unarmed skill, match it to the one here (maneuver #100) then normalize any new animation Ids. 
	
	In the event mesage is animation_speed which seems to be affected by the weapon's speed. If you have swift killer on making the weapon very fast (0), speed always reports 1.5. 
	If the weapon says very fast (10), the animation_speed is 1.3636363744735717.
	So make sure testing is done on a weapon with 0 speed.
	
	Stance affects how a animation (maneuvers) playout as well as their duration. Ie a shield stance attack looks different than a dual wield attack.
	For our stats I just used a weapon eqiped with no shield/dual weapon equiped. Obviously the stats will be different but I figure
	so long as all our stats were done under the same conditions, other stances will see similar increases/decreases in speed. /crosses fingers.
*/

// Stance IDs
var stance_1H			= 62;	// No shield/offhand equiped.
var stance_1H_Shield	= 64;	// Not used
var stance_1H_DuelWield	= 70;	// Not used.
var stance_2H			= 68;	// 2H weapons.
var stance_Bow			= 63;
var stance_XBow			= 65;
var stance_Atlatl		= 315;


// Animation IDs
var maneuver_DoubleSlash_High	= 289;
var maneuver_DoubleSlash_Low	= 287;
var maneuver_DoubleSlash_Mid	= 288;
var maneuver_Slash_High			= 94;
var maneuver_Slash_Low			= 96;
var maneuver_Slash_Mid			= 95;
var maneuver_Stab_High			= 90;
var maneuver_Stab_Low			= 89;
var maneuver_Stab_Mid			= 88;
var maneuver_TrueUnarmed_High	= 98;
var maneuver_Unarmed_High		= 101;
var maneuver_Unarmed_Low		= 100;
var maneuver_Unarmed_Mid		= 99;

var maneuver_Bow_Low	    	= 38;// Seems there's several animations for low/high depending on the angle. Same for xbow/atlatl.
var maneuver_Bow_Mid	    	= 30;
var maneuver_Bow_High	    	= 31;
var maneuver_XBow_Low	    	= 38;
var maneuver_XBow_Mid	    	= 30;
var maneuver_XBow_High	    	= 31;
var maneuver_Atlatl_Low	    	= 38;
var maneuver_Atlatl_Mid	    	= 30;
var maneuver_Atlatl_High	    = 31;


// Weapon animation types.
var animation_1H_DoubleSlash    = {stance: stance_1H,	low:maneuver_DoubleSlash_Low,	mid:maneuver_DoubleSlash_Mid,	high:maneuver_DoubleSlash_High};	// daggers with their back and forth double attack animation.
var animation_1H_Slash          = {stance: stance_1H,	low:maneuver_Slash_Low,			mid:maneuver_Slash_Mid,			high:maneuver_Slash_High};
var animation_1H_Stab           = {stance: stance_1H,	low:maneuver_Stab_Low,			mid:maneuver_Stab_Mid,			high:maneuver_Stab_High};
var animation_1H_Unarmed        = {stance: stance_1H,	low:maneuver_Unarmed_Low,		mid:maneuver_Unarmed_Mid, 		high:maneuver_Unarmed_High};
var animation_2H_DoubleSlash    = {stance: stance_2H,	low:maneuver_Slash_Low,			mid:maneuver_Slash_Mid,			high:maneuver_Slash_High};
var animation_2H_DoubleStab     = {stance: stance_2H,	low:maneuver_Stab_Low,			mid:maneuver_Stab_Mid, 			high:maneuver_Stab_High};

var animation_Bow               = {stance: stance_Bow,	low:maneuver_Bow_Low,	mid:maneuver_Bow_Mid,	high:maneuver_Bow_High};
var animation_XBow              = {stance: stance_XBow,	low:maneuver_XBow_Low,	mid:maneuver_XBow_Mid,	high:maneuver_XBow_High};
var animation_Atlatl            = {stance: stance_Atlatl,	low:maneuver_Atlatl_Low,	mid:maneuver_Atlatl_Mid,	high:maneuver_Atlatl_High};

	
var animationTimes = new Object();// Animation times in miliseconds.
animationTimes[stance_1H] = new Object();	// 1H weapon, no shield.
animationTimes[stance_1H][maneuver_Unarmed_Low] 		= 760;	// Unarmed low
animationTimes[stance_1H][maneuver_Unarmed_High] 		= 760;	// Unarmed high
animationTimes[stance_1H][maneuver_DoubleSlash_Low] 	= 1730/2;	// swingtwice low. These are long but hit twice, so for lazy calc lets just devide by 2.
animationTimes[stance_1H][maneuver_DoubleSlash_Mid] 	= 1770/2;	// swingtwice mid
animationTimes[stance_1H][maneuver_DoubleSlash_High] 	= 1770/2;	// swingtwice high
animationTimes[stance_1H][maneuver_Stab_Mid] 			= 965;	// thrust mid
animationTimes[stance_1H][maneuver_Stab_Low] 			= 955;	// thrust low
animationTimes[stance_1H][maneuver_Stab_High] 			= 960;	// thrust high
animationTimes[stance_1H][maneuver_Slash_High] 		= 910;	// swing high
animationTimes[stance_1H][maneuver_Slash_Mid] 			= 910;	// swing mid
animationTimes[stance_1H][maneuver_Slash_Low]			= 970;	// swing low
animationTimes[stance_1H][maneuver_TrueUnarmed_High]	= 765;	// True unarmed high
animationTimes[stance_1H][maneuver_Unarmed_Mid]		= 795;	// Unarmed mid
// Missing double stab for 1H weapons. I couldn't find any 0 wield req weapons with that property.
animationTimes[stance_2H] = new Object();	// 2H weapon.
animationTimes[stance_2H][maneuver_Slash_Low]		= 1275;	//swing low
animationTimes[stance_2H][maneuver_Slash_Mid]		= 1275;	//swing mid
animationTimes[stance_2H][maneuver_Slash_High]		= 1275;	//swing high
animationTimes[stance_2H][maneuver_Stab_Low]		= 1275;	//thrust low
animationTimes[stance_2H][maneuver_Stab_Mid]		= 1280;	//thrust mid
animationTimes[stance_2H][maneuver_Stab_High]		= 1280;	//thrust high

// Missile attacks. These were done at point blank range, real world results will differ depending on range.
animationTimes[stance_Atlatl] = {};
animationTimes[stance_Atlatl][maneuver_Atlatl_Low]  = 800;
animationTimes[stance_Atlatl][maneuver_Atlatl_Mid]  = 770;
animationTimes[stance_Atlatl][maneuver_Atlatl_High] = 780;
animationTimes[stance_XBow] = {};
animationTimes[stance_XBow][maneuver_XBow_Low]      = 950;
animationTimes[stance_XBow][maneuver_XBow_Mid]      = 870;
animationTimes[stance_XBow][maneuver_XBow_High]     = 875;
animationTimes[stance_Bow] = {};
animationTimes[stance_Bow][maneuver_Bow_Low]        = 680;
animationTimes[stance_Bow][maneuver_Bow_Mid]        = 590;
animationTimes[stance_Bow][maneuver_Bow_High]       = 585;

var weaponAnimations = new Object();
//weaponAnimations[""]					= {low:100,mid:99, high:98};// True Unarmed
weaponAnimations["Bastone"]             = animation_1H_Stab;
weaponAnimations["Battle Axe"]          = animation_1H_Slash;
weaponAnimations["Board with Nail"]     = animation_1H_Slash;//Guessing
weaponAnimations["Broad Sword"]         = animation_1H_Stab;
weaponAnimations["Budiaq"]              = animation_1H_Stab;
weaponAnimations["Cestus"]              = animation_1H_Unarmed;// high attack doesn't match the unarmed. unarmed is a high punch while this is a high swing.
weaponAnimations["Claw"]                = animation_1H_Unarmed;//Guessing
weaponAnimations["Club"]                = animation_1H_Slash;
weaponAnimations["Dabus"]               = animation_1H_Slash;
weaponAnimations["Dagger"]              = animation_1H_DoubleSlash;
weaponAnimations["Dericost Blade"]      = animation_1H_Stab;
weaponAnimations["Dirk"]                = animation_1H_Stab;
weaponAnimations["Dirk"]                = animation_1H_Stab;//Guessing
weaponAnimations["Dolabra"]             = animation_1H_Slash;
weaponAnimations["Epee"]                = animation_1H_DoubleSlash;
weaponAnimations["Fist"]                = animation_1H_Unarmed;
weaponAnimations["Flamberge"]           = animation_1H_Stab;
weaponAnimations["Flanged Mace"]        = animation_1H_Slash;
weaponAnimations["Hammer"]              = animation_1H_Slash; 
weaponAnimations["Hand Axe"]            = animation_1H_Slash;
weaponAnimations["Hand Wraps"]          = animation_1H_Unarmed;//Guessing
weaponAnimations["Hatchet"]             = animation_1H_Slash;
weaponAnimations["Jambiya"]             = animation_1H_DoubleSlash;
weaponAnimations["Jitte"]               = animation_1H_Slash;
weaponAnimations["Jo"]                  = animation_1H_Stab;
weaponAnimations["Kaskara"]             = animation_1H_Stab;
weaponAnimations["Kasrullah"]           = animation_1H_Slash;
weaponAnimations["Katar"]               = animation_1H_Unarmed;
weaponAnimations["Ken"]                 = animation_1H_Stab;
weaponAnimations["Khanjar"]             = animation_1H_Slash;
weaponAnimations["Knife"]               = animation_1H_DoubleSlash;
weaponAnimations["Knuckles"]            = animation_1H_Unarmed;
weaponAnimations["Lancet"]              = animation_1H_Stab;//Does both pierce and slash. 
weaponAnimations["Long Sword"]          = animation_1H_Stab;
weaponAnimations["Lugian Hammer"]       = animation_1H_Slash;//Guessing
weaponAnimations["Mace"]                = animation_1H_Slash;
weaponAnimations["Mazule"]              = animation_1H_Slash;
weaponAnimations["Morning Star"]        = animation_1H_Slash;
weaponAnimations["Nabut"]               = animation_1H_Stab;
weaponAnimations["Naginata"]            = animation_1H_Stab;//Does both pierce and slash. 
weaponAnimations["Nekode"]              = animation_1H_Unarmed;
weaponAnimations["Ono"]                 = animation_1H_Slash;
weaponAnimations["Partizan"]            = animation_1H_Stab;
weaponAnimations["Poniard"]             = animation_1H_Stab;
weaponAnimations["Quarter Staff"]       = animation_1H_Stab;
weaponAnimations["Rapier"]              = animation_1H_Stab;//Guessing, looks like a pointy weapon.
weaponAnimations["Sabra"]               = animation_1H_Stab;
weaponAnimations["Schlager Blade"]      = animation_1H_DoubleSlash;
weaponAnimations["Schlager"]            = animation_1H_DoubleSlash;
weaponAnimations["Scimitar"]            = animation_1H_Stab;
weaponAnimations["Shamshir"]            = animation_1H_Stab;
weaponAnimations["Short Sword"]         = animation_1H_Stab;
weaponAnimations["Shou-ono"]            = animation_1H_Slash;
weaponAnimations["Silifi"]              = animation_1H_Slash;
weaponAnimations["Simi"]                = animation_1H_Stab;
weaponAnimations["Spada"]               = animation_1H_Stab;
weaponAnimations["Spear"]               = animation_1H_Stab;
weaponAnimations["Spiked Club"]         = animation_1H_Slash;//Guessing
weaponAnimations["Spine Glaive"]        = animation_1H_Stab;//Guessing
weaponAnimations["Stick"]               = animation_1H_Stab;//Guessing Low: 956
weaponAnimations["Stiletto"]            = animation_1H_DoubleSlash;
weaponAnimations["Sword"]               = animation_1H_Stab;
weaponAnimations["Tachi"]               = animation_1H_Stab;
weaponAnimations["Takuba"]              = animation_1H_Stab;
weaponAnimations["Tofun"]               = animation_1H_Slash;
weaponAnimations["Trident"]             = animation_1H_Stab;//Guessing
weaponAnimations["Tungi"]               = animation_1H_Slash;
weaponAnimations["War Axe"]             = animation_1H_Slash;//Guessing
weaponAnimations["War Hammer"]          = animation_1H_Slash;
weaponAnimations["Yaoji"]               = animation_1H_Stab;
weaponAnimations["Yari"]                = animation_1H_Stab;

// 2H weapons, these hit twice. 
weaponAnimations["Spadone"]             = animation_2H_DoubleSlash;
weaponAnimations["Shashqa"]             = animation_2H_DoubleSlash;
weaponAnimations["Tetsubo"]             = animation_2H_DoubleSlash;
weaponAnimations["Khanda-handled Mace"] = animation_2H_DoubleSlash;
weaponAnimations["Magari Yari"]         = animation_2H_DoubleStab;
weaponAnimations["Greataxe"]            = animation_2H_DoubleSlash;
weaponAnimations["Great Star Mace"]     = animation_2H_DoubleSlash;
weaponAnimations["Corsesca"]            = animation_2H_DoubleStab;
weaponAnimations["Pike"]                = animation_2H_DoubleStab;
weaponAnimations["Assagai"]				= animation_2H_DoubleStab;
weaponAnimations["Nodachi"]				= animation_2H_DoubleSlash;
weaponAnimations["Quadrelle"]           = animation_2H_DoubleSlash;

// Missile weapons.
// Bows
weaponAnimations["Longbow"]             = animation_Bow;
weaponAnimations["Nayin"]               = animation_Bow;
weaponAnimations["Panaq"]               = animation_Bow;
weaponAnimations["Shortbow"]            = animation_Bow;
weaponAnimations["Shouyumi"]            = animation_Bow;
weaponAnimations["War Bow"]             = animation_Bow;
weaponAnimations["Yag"]                 = animation_Bow;
weaponAnimations["Yumi"]                = animation_Bow;
weaponAnimations[" Bow"]                = animation_Bow;

// Xbows
weaponAnimations["Arbalest"]            = animation_XBow;
weaponAnimations["Heavy Crossbow"]      = animation_XBow;
weaponAnimations["Light Crossbow"]      = animation_XBow;
weaponAnimations["Kalindan"]            = animation_XBow;
weaponAnimations["Crossbow"]            = animation_XBow;

//Atlatls
weaponAnimations["Atlatl"]              = animation_Atlatl;
weaponAnimations["Atlatl"]              = animation_Atlatl;
weaponAnimations["Flinger"]             = animation_Atlatl;




if (typeof module === 'object' && module.exports) {
	module.exports = {
		animationTimes: animationTimes,
		weaponAnimations: weaponAnimations
	};
} else if (typeof LootProfile33 !== "undefined") {
	LootProfile33.setDataKey("animationTimes", animationTimes);
	LootProfile33.setDataKey("weaponAnimations", weaponAnimations);
}
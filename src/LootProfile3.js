/******************************************************************************\
	Script:		LootProfile3.js
	Author:		Cyprias (cyprias@gmail.com)
	License:	MIT License	(http://opensource.org/licenses/MIT)
	Requires:	- Decal (http://decaldev.sourceforge.net)
				- SkunkWorks (http://skunkworks.sourceforge.net)
\******************************************************************************/
var MAJOR = "LootProfile-3.4";
var MINOR = 220324;

(function (factory) {

	// Load our dependencies.
	var dependencies = {};
	dependencies.EventEmitter   = (typeof EventEmitter !== "undefined" && EventEmitter) || require("modules\\SkunkSuite\\EventEmitter"); 
	dependencies.TinkeringUtil   = (typeof TinkeringUtil12 !== "undefined" && TinkeringUtil12) || require("modules\\TinkeringUtil"); 
	dependencies.LibDisappeared   = (typeof LibDisappeared10 !== "undefined" && LibDisappeared10) || require("modules\\LibDisappeared.js"); 

	require("SkunkSuite\\LibStub");
	require("SkunkSuite\\SkunkScript-1.0");
	require("SkunkSuite\\SkunkLogger-1.0");
	require("SkunkSuite\\SkunkTimer-1.0");
	require("SkunkSuite\\SkunkEvent-1.0");
	require("SkunkSuite\\SkunkSchema-1.0");
	require("SkunkSuite\\SkunkAssess-1.0");
	require("SkunkSuite\\SkunkDB-1.0");
	require("SkunkSuite\\SkunkSynchronous-1.0");
	require("SkunkSuite\\SkunkFS-1.0");

	dependencies.ObjectFilter   = (typeof ObjectFilter10 !== "undefined" && ObjectFilter10) || require("modules\\ObjectFilter"); 

	//require("iterative-permutation\\iterative-permutation");

	dependencies.async_js = typeof async_js !== "undefined" && async_js || require("async");
	
	if (typeof module === 'object' && module.exports) {
		module.exports = factory(dependencies);
	} else {
		LootProfile34 = factory(dependencies);
	}

}(function (dependencies) {

	var EventEmitter = dependencies.EventEmitter;
	var async_js = dependencies.async_js;
	var TinkeringUtil = dependencies.TinkeringUtil;
	var LibDisappeared = dependencies.LibDisappeared;
	var ObjectFilter = dependencies.ObjectFilter;
	
	var core = LibStub("SkunkScript-1.0").newScript(
		new EventEmitter(), 
		MAJOR, 
		"SkunkTimer-1.0", 
		"SkunkEvent-1.0", 
		"SkunkLogger-1.0", 
		"SkunkAssess-1.1", 
		"SkunkSynchronous-1.0", 
		"SkunkSchema-1.0", 
		"SkunkFS-1.0", 
		"SkunkCache-1.0",
		"SkunkActions-1.0");
		
	core.setShortName("LP3");

	var _name           = "LootProfile";
	var _version        = "3.4." + MINOR; // Major, minor (date)
	var _title          = MAJOR + "." + MINOR;

	//var _debug          = false;
	var _debugOpm       = (opmConsole); // default to making debug output to the console and Debug Port. // | opmDebugLog | opmChatWnd
	var _dir = ".\\";
	if (typeof __dirname !== "undefined") {
		_dir = __dirname + "\\";
	}

	var maxCallSize = 500; // Call callback within a timer if call stack goes above 500. SkunkWorks has a call stack limit of 750. 
	
	var data = {};
	data.palettes = {};

	var viewWidth = 210;
	var viewHeight = 300;
	
	var editorWidth = 200;
	var editorHeight = 200;

	var SkunkDB;
	var SkunkSchema;
	var SkunkAssess;
	
	var suitCantrips = {};
	suitCantrips['Acid Ward'] = true;
	suitCantrips['Alchemical Prowess'] = true;
	suitCantrips['Arcane Prowess'] = true;
	suitCantrips['Armor Tinkering Expertise'] = true;
	suitCantrips['Armor'] = true;
	suitCantrips['Bludgeoning Ward'] = true;
	suitCantrips['Cooking Prowess'] = true;
	suitCantrips['Coordination'] = true;
	suitCantrips['Creature Enchantment Aptitude'] = true;
	suitCantrips['Deception Prowess'] = true;
	suitCantrips['Dirty Fighting Prowess'] = true;
	suitCantrips['Dual Wield Aptitude'] = true;
	suitCantrips['Endurance'] = true;
	suitCantrips['Fealty'] = true;
	suitCantrips['Finesse Weapon Aptitude'] = true;
	suitCantrips['Flame Ward'] = true;
	suitCantrips['Fletching Prowess'] = true;
	suitCantrips['Focus'] = true;
	suitCantrips['Frost Ward'] = true;
	suitCantrips['Healing Prowess'] = true;
	suitCantrips['Health Gain'] = true;
	suitCantrips['Heavy Weapon Aptitude'] = true;

	//suitCantrips['Impenetrability'] = true;
	suitCantrips['Impregnability'] = true;
	suitCantrips['Invulnerability'] = true;
	suitCantrips['Item Enchantment Aptitude'] = true;
	suitCantrips['Item Tinkering Expertise'] = true;
	suitCantrips['Jumping Prowess'] = true;
	suitCantrips['Leadership'] = true;
	suitCantrips['Life Magic Aptitude'] = true;
	suitCantrips['Light Weapon Aptitude'] = true;
	suitCantrips['Lockpick Prowess'] = true;
	suitCantrips['Magic Item Tinkering Expertise'] = true;
	suitCantrips['Magic Resistance'] = true;
	suitCantrips['Mana Conversion Prowess'] = true;
	suitCantrips['Mana Gain'] = true;
	suitCantrips['Missile Weapon Aptitude'] = true;
	suitCantrips['Monster Attunement'] = true;
	suitCantrips['Person Attunement'] = true;
	suitCantrips['Piercing Ward'] = true;
	suitCantrips['Quickness'] = true;
	suitCantrips['Recklessness Prowess'] = true;
	suitCantrips['Salvaging Aptitude'] = true;
	suitCantrips['Shield Aptitude'] = true;
	suitCantrips['Slashing Ward'] = true;
	suitCantrips['Sneak Attack Prowess'] = true;
	suitCantrips['Sprint'] = true;
	suitCantrips['Stamina Gain'] = true;
	suitCantrips['Storm Ward'] = true;
	suitCantrips['Strength'] = true;
	suitCantrips['Summoning Prowess'] = true;
	suitCantrips['Two Handed Combat Aptitude'] = true;
	suitCantrips['Void Magic Aptitude'] = true;
	suitCantrips['War Magic Aptitude'] = true;
	suitCantrips['Weapon Tinkering Expertise'] = true;
	suitCantrips['Willpower'] = true;
	core.suitCantrips = suitCantrips;
	
	var suitSlots = {};
	suitSlots["Abdomen Outer"]	    = eqmAbdomenOuter;
	suitSlots["Abdomen Under"]      = eqmAbdomenUnder;
	suitSlots["Chest Outer"]        = eqmChestOuter;
	suitSlots["Chest Under"]        = eqmChestUnder;
	suitSlots["Feet"]               = eqmFeet;
	suitSlots["Hands"]              = eqmHands;
	suitSlots["Head"]               = eqmHead;
	suitSlots["Left Bracelet"]      = eqmLBracelet;
	suitSlots["Left Ring"]          = eqmLRing;
	suitSlots["Lower Arms Outer"]   = eqmLowerArmsOuter;
	suitSlots["Lower Legs Outer"]   = eqmLowerLegsOuter;
	suitSlots["Necklace"]           = eqmNecklace;
	suitSlots["Right Bracelet"]     = eqmRBracelet;
	suitSlots["Right Ring"]         = eqmRRing;
	suitSlots["Upper Arms Outer"]   = eqmUpperArmsOuter;
	suitSlots["Upper Legs Outer"]   = eqmUpperLegsOuter;
	suitSlots["Trinket"]            = 0x04000000;

	var dmtyNether                  =                      0x0400;

	core.WaitEvent = {};
	core.AsyncEvent = {};
	
	
	core.setDataKey = function setDataKey(key, value) {
		data[key] = value;
	};

	core.end = function end() {
		/******************************************************************************\
			end: End the script.
		\******************************************************************************/
		setRunning(false);
		fUnregisterEvents(_handler);
	};

	core.getTitle = function getTitle() {
		/******************************************************************************\
			getTitle: Return the script title including the version.
		\******************************************************************************/
		return _title;
	};
	
	core.getVersion = function getVersion() {
		/******************************************************************************\
			getVersion: Return the version.
		\******************************************************************************/
		return _version;
	};

	function getProfileDirectory() {
		/******************************************************************************\
			getProfileDirectory: Return the directory path of our profiles folder.
		\******************************************************************************/
		return _dir + "src\\data\\profiles\\" + skapi.szWorld;
	}

	var reminders = {};
	function fReminder(key, szMsg) {
		if (typeof reminders[key] === "undefined") {
			core.info("[Reminder] " + szMsg);
			reminders[key] = true;
		}
	}

	function fNow() {
		/******************************************************************************\
			Now: Return the current date and time.
		\******************************************************************************/
		return new Date();
	}

	core.round = function round(rnum, rlength) { // Arguments: number to round, number of decimal places
		/******************************************************************************\
			round: Round a number.
		\******************************************************************************/
		if (rlength == null) rlength = 0;
		return Math.round(rnum * Math.pow(10, rlength)) / Math.pow(10, rlength);
	};

	function fHex(a) {
		/******************************************************************************\
			fHex: Convert decimal to hex.
		\******************************************************************************/
		if (!a) {
			a = 0;
		}
		return (a < 0 ? 4294967296 + a : a).toString(16);
	}

	function fUnregisterEvents(handler) {
		/******************************************************************************\
			fUnregisterEvents: Unregister our events.
		\******************************************************************************/
		//skapi.RemoveHandler(mtyCreateObject,handler);
	}

	core.dumpException = function dumpException(e, comment) {
		/******************************************************************************\
			dumpException: Print error.
		\******************************************************************************/
		try {
			var now = new Date();
			core.warn("Error " + e + ", comment: " + comment);
			core.warn("Error Number = " + (e.number & 0xFFFF));
			core.warn("Error Description = " + e.description);

			//core.warn("Error Message = " + e.message);
		} catch(e) {
			core.warn("Error in dumpException. Unhandled exception.");
		}
	};

	/*
	var oidPalettesPath = _dir + "src\\data\\palettes\\" + skapi.szWorld + ".sdb";
	core.setPalettes = function setPalettes( params ) {
		var oid = params.oid;
		var palettes = params.palettes;
		
		if (!core.existsSync(oidPalettesPath)) {
			core.touchSync(oidPalettesPath);
		}
		
		SkunkDB.insert().into(oidPalettesPath).values([{
			oid:oid,
			palettes:palettes
		}]).execute();
	};
	
	core.getPalettes = function getPalettes( params ) {
		var oid = params.oid;
		
		var results = SkunkDB.select().from(oidPalettesPath).where({
			oid:oid
		}).limit(1).execute();
	
		if (results.rows.length > 0) {
			return results.rows[0].palettes;
		}
		
	};
	*/
	
	core.getPalettes = function getPalettes(params) {};
	
	/*
	function onCreateObject(payload) {
		if (payload.model && payload.model.palettes && payload.model.palettes.length > 0) {
			if (typeof payload.game.coverage !== "undefined") {
				var palettes = [];
				var p;
				for (var i = 0; i < payload.model.palettes.length; i++) {
					p = payload.model.palettes[i];
					palettes.push(p.palette);
				}
				data.palettes[payload.object] = palettes; // Kept in memory in case we need to save it later.

				var aco = skapi.AcoFromOid(payload.object);
				if (aco) {
					if (core.AcoBelongsToMe(aco)) {
						//core.setPalettes({oid:payload.object, palettes:palettes});
						// TODO replace this
					}
				}
			}
			return;
		}
		//debug("<onCreateObject> " + JSON.stringify(payload, null, "\t"));
	};
	*/

	core.on("onInitialize", function onInitialize() {
		core.debug("<onInitialize>");
		
		
		ObjectFilter            = LibStub("ObjectFilter-1.0");
		SkunkDB                 = LibStub("SkunkDB-1.0");
		SkunkSchema                 = LibStub("SkunkSchema-1.0");
		SkunkAssess                 = LibStub("SkunkAssess-1.1");
		LibDisappeared = LibStub("LibDisappeared-1.0");

		////ObjectFilter.addCallback(onCreateObject);
		
		LoadColourTable();
	
		fLoadSettings();
		
		var animations = require("..\\LootProfile3\\src\\data\\animations.js");
		core.setDataKey("animationTimes", animations.animationTimes);
		core.setDataKey("weaponAnimations", animations.weaponAnimations);

		core.loadProfiles();
	});
	
	core.on("onEnable", function onEnable() {
		
		fLoadSettings();

		core.addHandler(evidOnAddToInventory); // Helps with palette saving.
		//core.addHandler(evidOnObjectDestroy); // Purge forgotten item palettes from memory.
		
		//core.console("szScript: " + console.szScript);
		if (console.szScript.match("LootProfile3.swx")) { // Watch Start3D to redraw the UI if we relog. Since we can only capture armor palettes at login we may want to logout and back in to capture our inventory.
		
			core.addHandler(evidOnStart3D);
		}
		
		if (skapi.plig == pligAtLogin) {
		//	skapi.AddHandler(evidOnEnd3D,               handler);
			core.console("Waiting to login...");
		} else if (skapi.plig == pligInWorld) {
			core.showUI();
			core.registerContainerEvents();
		}
		
		LibDisappeared.emitter.on("Destroyed", onDisappeared);
		LibDisappeared.emitter.on("Disappeared", onDisappeared);
		
		/*
		core.inventory = new core.Cache({
			path      : _dir + "src/data/worlds/" + skapi.szWorld + "/characters/" + skapi.acoChar.szName + "/inventory.json"
			filePerKey: true
		}).load();
		*/
		
		core.itemStats = new core.Cache({
			path      : _dir + "src/data/worlds/" + skapi.szWorld + "/itemStats",
			filePerKey: true
		}).load();
	});
	
	core.on("onDisable", function onDisable() {
		LibDisappeared.emitter.removeListener("Destroyed", onDisappeared);
		LibDisappeared.emitter.removeListener("Disappeared", onDisappeared);
	});

	//////////////////////////////
	// Support aco functions.   //
	//////////////////////////////
	
	core.isDinnerware = function isDinnerware(aco) {
		// We don't want cups, either in our pack or in our stat calc.
		if (aco.eqm & eqmRangedWeapon) {
			// oty will equal 18 for bows and bowls.
			// ocm will equal 128 for bows and bowls.
			// vLaunch & distRange will equal 0 for bowls and cups but not bows. 
			var vLaunch = core.getObjectValue(aco, "oai.iwi.vLaunch");
			if (vLaunch == 0) {
				return true;
			}
		}
		return false;
	};
	
	core.getFullName = (function factory() {
		var materials = {};
		materials[materialAgate]            = "Agate";
		materials[materialAlabaster]        = "Alabaster";
		materials[materialAmber]            = "Amber";
		materials[materialAmethyst]         = "Amethyst";
		materials[materialAquamarine]       = "Aquamarine";
		materials[materialAzurite]          = "Azurite";
		materials[materialBlackGarnet]      = "Black Garnet";
		materials[materialBlackOpal]        = "Black Opal";
		materials[materialBloodstone]       = "Bloodstone";
		materials[materialBrass]            = "Brass";
		materials[materialBronze]           = "Bronze";
		materials[materialCarnelian]        = "Carnelian";
		materials[materialCeramic]          = "Ceramic";
		materials[materialCitrine]          = "Citrine";
		materials[materialCloth]            = "Cloth";
		materials[materialCopper]           = "Copper";
		materials[materialDiamond]          = "Diamond";
		materials[materialDilloHide]        = "Dillo Hide";
		materials[materialEbony]            = "Ebony";
		materials[materialEmerald]          = "Emerald";
		materials[materialFireOpal]         = "Fire Opal";
		materials[materialGem]              = "Gem";
		materials[materialGold]             = "Gold";
		materials[materialGranite]          = "Granite";
		materials[materialGreenGarnet]      = "Green Garnet";
		materials[materialGreenJade]        = "Green Jade";
		materials[materialGromnieHide]      = "Gromnie Hide";
		materials[materialHematite]         = "Hematite";
		materials[materialImperialTopaz]    = "Imperial Topaz";
		materials[materialIron]             = "Iron";
		materials[materialIvory]            = "Ivory";
		materials[materialJet]              = "Jet";
		materials[materialLapisLazuli]      = "Lapis Lazuli";
		materials[materialLavenderJade]     = "Lavender Jade";
		materials[materialLeather]          = "Leather";
		materials[materialLinen]            = "Linen";
		materials[materialMahogany]         = "Mahogany";
		materials[materialMalachite]        = "Malachite";
		materials[materialMarble]           = "Marble";
		materials[materialMetal]            = "Metal";
		materials[materialMoonstone]        = "Moonstone";
		materials[materialOak]              = "Oak";
		materials[materialObsidian]         = "Obsidian";
		materials[materialOnyx]             = "Onyx";
		materials[materialOpal]             = "Opal";
		materials[materialPeridot]          = "Peridot";
		materials[materialPine]             = "Pine";
		materials[materialPorcelain]        = "Porcelain";
		materials[materialPyreal]           = "Pyreal";
		materials[materialRedGarnet]        = "Red Garnet";
		materials[materialRedJade]          = "Red Jade";
		materials[materialReedsharkHide]    = "Reedshark Hide";
		materials[materialRoseQuartz]       = "Rose Quartz";
		materials[materialRuby]             = "Ruby";
		materials[materialSandstone]        = "Sandstone";
		materials[materialSapphire]         = "Sapphire";
		materials[materialSatin]            = "Satin";
		materials[materialSerpentine]       = "Serpentine";
		materials[materialSilk]             = "Silk";
		materials[materialSilver]           = "Silver";
		materials[materialSmokeyQuartz]     = "Smoky Quartz";
		materials[materialSteel]            = "Steel";
		materials[materialStone]            = "Stone";
		materials[materialSunstone]         = "Sunstone";
		materials[materialTeak]             = "Teak";
		materials[materialTigerEye]         = "Tiger Eye";
		materials[materialTourmaline]       = "Tourmaline";
		materials[materialTurquoise]        = "Turquoise";
		materials[materialVelvet]           = "Velvet";
		materials[materialWhiteJade]        = "White Jade";
		materials[materialWhiteQuartz]      = "White Quartz";
		materials[materialWhiteSapphire]    = "White Sapphire";
		materials[materialWood]             = "Wood";
		materials[materialWool]             = "Wool";
		materials[materialYellowGarnet]     = "Yellow Garnet";
		materials[materialYellowTopaz]      = "Yellow Topaz";
		materials[materialZircon]           = "Zircon";

		return function getFullName(aco) {
			var szName = aco && aco.szName;
			if (aco && aco.material > 0 && materials[aco.material]) {
				
				// Emulator has material name in some item names but not all.
				if (!szName.match(materials[aco.material])) {
					szName = materials[aco.material] + " " + aco.szName;
				}
			}
			return szName;
		};
	})();
	
	//var spellNames = {};
	core.getSpellName = function getSpellName(spellId) {
		core.debug("<getSpellName> " + spellId);

		//if (!spellNames[spellId]) {
		//	spellNames[spellId] = skapi.SpellInfoFromSpellid(spellId).szName;
		//}
		//return spellNames[spellId];
		var spell = skapi.SpellInfoFromSpellid(spellId);
		return spell.szName;
	};
	
	core.getCantripMaxDamage = function getCantripMaxDamage(aco) {
		var maxdamage = aco.oai.iwi.dhealth;
		
		var spells = core.getAcoSpells({aco: aco});
		var spellid, szName;
		for (var i = 0; i < spells.length; i++) {
			spellid = spells[i];

			//spell = skapi.SpellInfoFromSpellid(aco.oai.iei.rgspellid(i));
			szName = getSpellName(spellid);
			if (szName.match("Legendary Blood Thirst")) {
				maxdamage += 10;	// I'm guessing it's 10, it's not on the wiki.
				break;
			} else if (szName.match("Epic Blood Thirst")) {
				maxdamage += 7;
				break;
			} else if (szName.match("Major Blood Thirst")) {
				maxdamage += 4;
				break;
			} else if (szName.match("Minor Blood Thirst")) {
				maxdamage += 2;
				break;
			}
		}

		return maxdamage;
	};
	
	core.getCantripDamageBonus = function getCantripDamageBonus(aco) {
		var bonus = 0;
		
		var spells = core.getAcoSpells({aco: aco});
		var spellid, szName;
		for (var i = 0; i < spells.length; i++) {
			spellid = spells[i];
			szName = getSpellName(spellid);
			if (szName.match("Legendary Blood Thirst")) {
				bonus += 10;	// I'm guessing it's 10, it's not on the wiki.
				break;
			} else if (szName.match("Epic Blood Thirst")) {
				bonus += 7;
				break;
			} else if (szName.match("Major Blood Thirst")) {
				bonus += 4;
				break;
			} else if (szName.match("Minor Blood Thirst")) {
				bonus += 2;
				break;
			}
		}

		return bonus;
	};
	
	core.getBuffedMaxDamage = function getBuffedMaxDamage(aco) {
		var bloodDrinker = 22;// Assume we're going to be casting lvl 7 blood drinker (+22 dmg) 
		var lvlWieldReq = 0;//
		if (aco.oai && aco.oai.ibi && aco.oai.ibi.lvlWieldReq) {
			lvlWieldReq = aco.oai.ibi.lvlWieldReq;
		}

		var maxdamage = getCantripMaxDamage(aco) + bloodDrinker + (lvlWieldReq / 20);//aco.oai.iwi.dhealth + bloodDrinker + (lvlWieldReq/20);
		var variance = aco.oai.iwi.scaleDamageRange;
		return maxdamage;
	};

	core.getMinDamage = function getMinDamage(maxDamage, variance) {
		var minDamage = maxDamage - (variance * maxDamage);
		return round(minDamage, 2);
	};
	
	core.getAvgDamage = function getAvgDamage(maxDamage, variance) {
		var minDamage = getMinDamage(maxDamage, variance);
		var avgDamage = (maxDamage + minDamage) / 2;

		//Inform("maxDamage: " + maxDamage + ", variance: " + variance);
		//Inform("minDamage: " + minDamage + ", avgDamage: " + avgDamage);
		return round(avgDamage, 1);
	};
	
	core.getBuffedAvgDamage = function getBuffedAvgDamage(aco) {
		core.debug("<getBuffedAvgDamage> " + aco);
		var maxdamage = getBuffedMaxDamage(aco);
		core.debug("maxdamage: " + maxdamage);
		var variance = aco.oai.iwi.scaleDamageRange;
		core.debug("variance: " + variance);
		return getAvgDamage(maxdamage, variance);
	};
	
	var ammoDamage = {};
	ammoDamage[8] = {};// Arrows
	ammoDamage[8][0] = 12.25;	//Greater Arrow 12.25
	ammoDamage[8][230] = 22.1;	//Deadly Arrow 22.1
	ammoDamage[8][270] = 22.1;	//Greater Deadly Arrow 34
	ammoDamage[9] = {};// Quarrels
	ammoDamage[9][0] = 15.75;	// Greater Quarrel 15.75
	ammoDamage[9][230] = 24.08;		//Deadly Quarrel 24.08
	ammoDamage[9][270] = 45.05;	//Greater Deadly Armor Piercing Quarrel 45.05
	ammoDamage[10] = {};// Darts
	ammoDamage[10][0] = 16.625; // Greater Atlatl Dart 16.625
	ammoDamage[10][230] = 23.8;	// Deadly Atlatl Dart 23.8
	ammoDamage[10][270] = 35.7;	// Greater Deadly Atlatl Dart 35.7


	function fGetMissileDamage(aco) {
		var weaponType = aco.oai.ibi.raw.Item(65889);
		if (weaponType) {
		//	core.info("weaponType: " + weaponType);
			if (ammoDamage[weaponType]) {
				var lvlWieldReq = aco.oai.ibi.lvlWieldReq;
				if (!lvlWieldReq) {
					lvlWieldReq = 0;
				}

				//	core.info("lvlWieldReq: " + lvlWieldReq);
				
				var highestDPS = 0;
				
				for (var wield in ammoDamage[weaponType])  {
					if (!ammoDamage[weaponType].hasOwnProperty(wield)) continue;
					if (lvlWieldReq >= wield) {
						if (ammoDamage[weaponType][wield] > highestDPS) {
							highestDPS = ammoDamage[weaponType][wield];
						}
					}
				}

				//	core.info("highestDPS: " + highestDPS);
				return highestDPS;
			}
		}
		return 0;
	}

	core.getAcoAvgDamage = function getAcoAvgDamage(aco, cantrip) {
		var avgDamage = 0;
		if (getWieldSkill(aco) == skidMissileWeapons) {
			avgDamage = fGetMissileDamage(aco);
			var scaleDamageBonus = core.getObjectValue(aco, "oai.iwi.scaleDamageBonus", 0);
			avgDamage = avgDamage * (scaleDamageBonus - 1);
		} else {
			var maxDmg = getDamageMax(aco);
			var scaleDamageRange = core.getObjectValue(aco, "oai.iwi.scaleDamageRange", 0);
			avgDamage = getAvgDamage(maxDmg, scaleDamageRange);
		}
		return avgDamage;
	};
	
	function compare_spells(a, b) {
		if (a.diff == b.diff) {
			if (a.szName < b.szName) return 1;
			if (a.szName > b.szName) return -1;
		} else {
			if (a.diff < b.diff) return 1;
			if (a.diff > b.diff) return -1;
		}
		return 0;
	}
	
	/**
	 * getItemString() Get a items stats in human readable string format.
	 *
	 * @param {object} params: Collection of arguments.
	 * @param {object} params.aco: Item
	 * @return {string} Item stats.
	 */
	core.getItemString = function getItemString(params, callback) {
		var aco = params.aco;
		var thisArg = params.thisArg;
		
		var s = core.getFullName(aco);

		//core.info("s: " + s);
		
		if (aco.citemStack > 1) {
			s += " (" + aco.citemStack + ")";
		}
		
		if (aco.oai || aco._cloned) {
			return onWorkmanship();
		} else {
			return core.queueAssessAco({aco: aco}, onWorkmanship);
		}

		function onWorkmanship(err, workmanship) {
			if (err) return callback.call(thisArg, undefined, s);

			s += " :";
			
			var workmanship = core.getWorkmanshipSync({aco: aco});
			
			if (workmanship > 0) {
				s += " w" + round(workmanship, 2);
			}

			var al = getObjectValue(aco, "oai.iai.al", 0);

			if (al) {
				s += " AL" + al;
				var protAL = 0;
				protAL += al * (getObjectValue(aco, "oai.iai.protSlashing", 0));
				protAL += al * (getObjectValue(aco, "oai.iai.protPiercing", 0));
				protAL += al * (getObjectValue(aco, "oai.iai.protBludgeoning", 0));
				protAL += al * (getObjectValue(aco, "oai.iai.protFire", 0));
				protAL += al * (getObjectValue(aco, "oai.iai.protCold", 0));
				protAL += al * (getObjectValue(aco, "oai.iai.protAcid", 0));
				protAL += al * (getObjectValue(aco, "oai.iai.protElectrical", 0));
				protAL = protAL / 7;
				s += " [" + round(protAL) + "]";
			}

			var skill = getWieldSkill(aco);

			if (skill) {
				if (skill == skidMissileWeapons) {
					var scaleDamageBonus = getObjectValue(aco, "oai.iwi.scaleDamageBonus", 0);
					v = round((scaleDamageBonus - 1) * 100, 4);
					s += " M:" + v + "%";
				} else {
					var maxDamage = getDamageMax(aco);
					var minDamage = maxDamage - (getObjectValue(aco, "oai.iwi.scaleDamageRange") * maxDamage);
					var avgDamage = (maxDamage + minDamage) / 2;
					if (round(avgDamage, 2) > 0) {
						s += " D:" + round(avgDamage, 0) + " " + getDamageType(getObjectValue(aco, "oai.iwi.dmty", 0));
					}
				}
			
				var scaleAttackBonus = getObjectValue(aco, "oai.iwi.scaleAttackBonus", 0);
				if (round(scaleAttackBonus, 4) > 1) {
					v = round((scaleAttackBonus - 1) * 100, 4);
					s += " A+" + v + "%";
				}
				
				var scalePvMElemBonus = getObjectValue(aco, "oai.iwi.scalePvMElemBonus", 0);
				if (round(scalePvMElemBonus, 4) > 1) {
					v = round((scalePvMElemBonus - 1) * 100, 4);
					var dmty = getObjectValue(aco, "oai.iwi.dmty", 0);
					s += " " + getDamageType(dmty) + "+" + v + "%";
				}

				var scaleDefenseBonus = getObjectValue(aco, "oai.iwi.scaleDefenseBonus", 0);
				if (round(scaleDefenseBonus, 4) > 1) {
					v = round((scaleDefenseBonus - 1) * 100, 4);
					s += " MeD+" + v + "%";
				}

				var scaleMissileDBonus = getObjectValue(aco, "oai.iwi.scaleMissileDBonus", 0);
				if (round(scaleMissileDBonus, 4) > 1) {
					v = round((scaleMissileDBonus - 1) * 100, 4);
					s += " MiD+" + v + "%";
				}
			}
			
			var fractManaConvMod = getObjectValue(aco, "oai.ibi.fractManaConvMod");
			if (fractManaConvMod) {
				if (round(fractManaConvMod, 4) > 0) {
					v = round(fractManaConvMod * 100, 4);
					s += " MaC+" + v + "%";
				}
			}

			if (aco.cpyValue > 0) {
				s += " V:" + aco.cpyValue;
			}
				
			var fAttuned = getObjectValue(aco, "oai.ibi.fAttuned");
			var fBonded = getObjectValue(aco, "oai.ibi.fBonded");

			var lvlWieldReq = getObjectValue(aco, "oai.ibi.lvlWieldReq");
			var skidWieldReq = getObjectValue(aco, "oai.ibi.skidWieldReq", 0);
			
			if (lvlWieldReq > 0) {
				if (skidWieldReq) {
					s += " W:" + skidToName(skidWieldReq) + lvlWieldReq;
				} else {
					s += " LW:" + lvlWieldReq; // Level Wield
				}
			}
			
			var difficulty = getObjectValue(aco, "oai.iei.difficulty");
			if (difficulty) {
				s += " Lo:" + difficulty;
			}
			
			
			var spells = core.getAcoSpells({aco: aco});
			var listSpells = [];
			var spellid, spell;
			for (var i = 0; i < spells.length; i++) {
				spellid = spells[i];
				spell = skapi.SpellInfoFromSpellid(spellid);
				if (spell.mana == 10) {
					listSpells.push(spell);
				} else if (spell.diff == 1) {
					listSpells.push(spell);
				}
			}
			
			listSpells.sort(compare_spells);
			
			if (listSpells.length > 0) {
				var spell = listSpells[0];
				
				s += " [" + spell.szName;
				
				if (listSpells.length > 1) {
					for (var i = 1; i < (listSpells.length - 1); i++) {
						spell = listSpells[i];
						s += ", " + spell.szName;
					}
					spell = listSpells[listSpells.length - 1];
					s += " & " + spell.szName;
				}
				s += "]";
			}

			return callback.call(thisArg, undefined, s);
		}
	};

	core.getItemStringSync = function getItemStringSync(params) {
		var aco = params.aco;
		var s = core.getFullName(aco);

		//core.info("s: " + s);
		
		if (aco.citemStack > 1) {
			s += " (" + aco.citemStack + ")";
		}

		s += " :";
		
		var workmanship = core.getWorkmanshipSync({aco: aco});
		if (workmanship > 0) {
			s += " w" + round(workmanship, 2);
		}

		var al = getObjectValue(aco, "oai.iai.al", 0);

		if (al) {
			s += " AL" + al;
			var protAL = 0;
			protAL += al * (getObjectValue(aco, "oai.iai.protSlashing", 0));
			protAL += al * (getObjectValue(aco, "oai.iai.protPiercing", 0));
			protAL += al * (getObjectValue(aco, "oai.iai.protBludgeoning", 0));
			protAL += al * (getObjectValue(aco, "oai.iai.protFire", 0));
			protAL += al * (getObjectValue(aco, "oai.iai.protCold", 0));
			protAL += al * (getObjectValue(aco, "oai.iai.protAcid", 0));
			protAL += al * (getObjectValue(aco, "oai.iai.protElectrical", 0));
			protAL = protAL / 7;
			s += " [" + round(protAL) + "]";
		}

		var skill = getWieldSkill(aco);

		if (skill) {
			if (skill == skidMissileWeapons) {
				var scaleDamageBonus = getObjectValue(aco, "oai.iwi.scaleDamageBonus", 0);
				v = round((scaleDamageBonus - 1) * 100, 4);
				s += " M:" + v + "%";
			} else {
				var maxDamage = getDamageMax(aco);
				var minDamage = maxDamage - (getObjectValue(aco, "oai.iwi.scaleDamageRange") * maxDamage);
				var avgDamage = (maxDamage + minDamage) / 2;
				if (round(avgDamage, 2) > 0) {
					s += " D:" + round(avgDamage, 0) + " " + getDamageType(getObjectValue(aco, "oai.iwi.dmty", 0));
				}
			}
		
			var scaleAttackBonus = getObjectValue(aco, "oai.iwi.scaleAttackBonus", 0);
			if (round(scaleAttackBonus, 4) > 1) {
				v = round((scaleAttackBonus - 1) * 100, 4);
				s += " A+" + v + "%";
			}
			
			var scalePvMElemBonus = getObjectValue(aco, "oai.iwi.scalePvMElemBonus", 0);
			if (round(scalePvMElemBonus, 4) > 1) {
				v = round((scalePvMElemBonus - 1) * 100, 4);
				var dmty = getObjectValue(aco, "oai.iwi.dmty", 0);
				s += " " + getDamageType(dmty) + "+" + v + "%";
			}

			var scaleDefenseBonus = getObjectValue(aco, "oai.iwi.scaleDefenseBonus", 0);
			if (round(scaleDefenseBonus, 4) > 1) {
				v = round((scaleDefenseBonus - 1) * 100, 4);
				s += " MeD+" + v + "%";
			}

			var scaleMissileDBonus = getObjectValue(aco, "oai.iwi.scaleMissileDBonus", 0);
			if (round(scaleMissileDBonus, 4) > 1) {
				v = round((scaleMissileDBonus - 1) * 100, 4);
				s += " MiD+" + v + "%";
			}
		}
		
		var fractManaConvMod = getObjectValue(aco, "oai.ibi.fractManaConvMod");
		if (fractManaConvMod) {
			if (round(fractManaConvMod, 4) > 0) {
				v = round(fractManaConvMod * 100, 4);
				s += " MaC+" + v + "%";
			}
		}

		if (aco.cpyValue > 0) {
			s += " V:" + aco.cpyValue;
		}
			
		var fAttuned = getObjectValue(aco, "oai.ibi.fAttuned");
		var fBonded = getObjectValue(aco, "oai.ibi.fBonded");

		var lvlWieldReq = getObjectValue(aco, "oai.ibi.lvlWieldReq");
		var skidWieldReq = getObjectValue(aco, "oai.ibi.skidWieldReq", 0);
		
		if (lvlWieldReq > 0) {
			if (skidWieldReq) {
				s += " W:" + skidToName(skidWieldReq) + lvlWieldReq;
			} else {
				s += " LW:" + lvlWieldReq; // Level Wield
			}
		}
		
		var difficulty = getObjectValue(aco, "oai.iei.difficulty");
		if (difficulty) {
			s += " Lo:" + difficulty;
		}
		
		
		var spells = core.getAcoSpells({aco: aco});
		var listSpells = [];
		var spellid, spell;
		for (var i = 0; i < spells.length; i++) {
			spellid = spells[i];
			spell = skapi.SpellInfoFromSpellid(spellid);
			if (spell.mana == 10) {
				listSpells.push(spell);
			} else if (spell.diff == 1) {
				listSpells.push(spell);
			}
		}
		
		listSpells.sort(compare_spells);
		
		if (listSpells.length > 0) {
			var spell = listSpells[0];
			
			s += " [" + spell.szName;
			
			if (listSpells.length > 1) {
				for (var i = 1; i < (listSpells.length - 1); i++) {
					spell = listSpells[i];
					s += ", " + spell.szName;
				}
				spell = listSpells[listSpells.length - 1];
				s += " & " + spell.szName;
			}
			s += "]";
		}
			
		return s;
		
		
	};

	function skidToName(skid) {
		/******************************************************************************\
			skidToName: Return a human readable name for a given skid.
		\******************************************************************************/
		
		switch(skid) { // Handle the types we define
		case skidLightWeapons:
			return "Light";
		case skidHeavyWeapons:
			return "Heavy";
		case skidFinesseWeapons:
			return "Finesse";
		case skidTwoHanded:
			return "2H";
		case skidMissileWeapons:
			return "MissileW";
		case skidMissileDefense:
			return "MissileD";
		case skidMeleeDefense:
			return "MeleeD";
		case skidWarMagic:
			return "War";
		case skidVoidMagic:
			return "Void";
		}
		return "SkillId:" + skid + " ";
	}
	
	function getDamageType(dmty) {
		/******************************************************************************\
			getDamageType: Return a human readable name for a given damage type mask.
		\******************************************************************************/
		if (dmty & dmtySlashing && dmty & dmtyPiercing) return "Slash/Pierce";
		if (dmty & dmtySlashing) return "Slash";
		if (dmty & dmtyPiercing) return "Pierce";
		if (dmty & dmtyBludgeoning) return "Bludge";
		if (dmty & dmtyCold) return "Cold";
		if (dmty & dmtyFire) return "Fire";
		if (dmty & dmtyAcid) return "Acid";
		if (dmty & dmtyElectrical) return "Electrical";
		if (dmty & dmtyNether) return "Nether";
		return dmty;
	}

	function SimpleUUID() {
		return Math.floor((1 + Math.random()) * 0x10000).toString(16)
			.substring(1);
	}
	
	function escapeXml(unsafe) {
		return unsafe.replace(/[<>&'"]/g, function (c) {
			switch (c) {
			case '<': return '&lt;';
			case '>': return '&gt;';
			case '&': return '&amp;';
			case '\'': return '&apos;';
			case '"': return '&quot;';
			}
		});
	}
	
	function fShowProfileEditor(profile) {
		var title = "Editing " + profile.getName();
		
		function fRemoveEditor() {
			//skapi.RemoveControls(title);
			view.removeControls();
		}
		
		function fExitEditor() {
			//view.unregisterPanelEvents();
			//fRemoveEditor();
			view.removeControls();
		}

		function fShowEditor() {
			//var xml = view.toXML();
			//skapi.ShowControls(xml, true);
			//fPopulateRulesList();

			view.showControls(true);
			fPopulateRulesList();
		}

		function fPopulateRuleNumMenu() {
			choRuleNum.clear();
			choRuleNum.addOption({text: "Select..."});
			
			var ruleCount = profile.data.rules.length;
			var r;
			for (var i = 0; i < ruleCount; i++) {
				r = profile.getRule(i);
				core.debug("Adding " + r.toString() + " to rule num list...");
				choRuleNum.addOption({text: escapeXml(r.toString()), data: i});
			}
		}
		
		function fFillModifyTab(rule) {
			core.debug("<fFillModifyTab>");
			var pID = rule.getPropertyID();
			core.debug("pID: " + pID + ", properties: " + properties.length);
			
			// Update property menu.
			var pIndex = 0;
			
			var v;
			for (var i = 0; i < properties.length; i++) {
				v = properties[i];

				//core.info(i, v.data);
				if (v.data == pID) {
					pIndex = i;
					break;
				}
			}

			choProperty2.setSelected(pIndex + 1);//+1 due to 'Select...' index.

			var operator = rule.getOperator();
			core.debug("operator: " + operator);

			var oIndex = 0;
			for (var i = 0; i < _operators.length; i++) {
				v = _operators[i];
				
				if (v == operator) {
					oIndex = i;
				}
				
			}
			
			core.debug("oIndex: " + oIndex);
			
			choOperator2.setSelected(oIndex + 1);
			
			// Update value box.
			var value = rule.getValue();

			//core.info("AC edValue2.setText(" + value + ")");
			edValue2.setText(value);
			
			core.debug("</fFillModifyTab>");
		}
		
		function onEditorControlEvent(control, szPanel, szControl, value) {
			if (szControl == "edProfileName") {
				profile.setName(control.getValue());
				core.info("Name updated to '" + profile.getName() + "'.");
				
				fRemoveEditor();
				title = "Editing " + profile.getName();
				view.setTitle(title);
				edProfileName.setText(profile.getName());
				
				fShowEditor();
				
				return;
			} else if (szControl == "btnExit") {
				fExitEditor();
				return;
			} else if (szControl == "btnUpdateIcon") {
				if (!skapi.acoSelected) {
					core.info("You must select something first.");
					return;
				}
				
				profile.setIcon(skapi.acoSelected.icon); //0x06000000 + 
				core.info("Icon set to " + skapi.acoSelected.icon + ", " + profile.getIcon());
				view.setIcon(profile.getIcon());
				core.info("viewIcon: " + view.getIcon());
				fRemoveEditor();
				fShowEditor();
				return;
			} else if (szControl == "btnAdd") {
				var propertyId = choProperty.getValue();
				core.debug("propertyId: " + propertyId);
				
				if (typeof propertyId === "undefined" || typeof _properties[propertyId] === "undefined") {
					core.info("Select a property from the drop down menu.");
					return;
				}
				
				
				var operator = choOperator.getValue();
				core.debug("operator: " + operator);
				
				if (typeof operator === "undefined" || typeof _operators[operator] === "undefined") {
					core.info("Select a operator from the drop down menu.");
					return;
				}
				
				var value = edValue.getValue();
				core.debug("value: " + value);
				if (typeof value === "undefined") {
					core.info("Input a value into the edit box.");
					return;
				}
				
				
				var rule = new Rule();
				rule.setPropertyID(propertyId);
				rule.setOperator(_operators[operator]);
				rule.setValue(value);
				rule.setEnabled(true);
				
				profile.addRule(rule);
				
				core.info("Added [" + rule.toString() + "] to " + profile.getName() + ".");
				
				tabs.setValue(0);
				fPopulateRulesList();
				
				fPopulateRuleNumMenu();
				return;
			} else if (szControl == "lstRules") {
				var intCol = parseInt(value.split(",")[0]);
				var intRow = parseInt(value.split(",")[1]);
				
				core.debug("intCol: " + intCol + ", intRow: " + intRow);

				//var rules = profile.getRules();
				var ruleCount = profile.data.rules.length;
				var r = profile.getRule(intRow);
				
				
				//var oid = control.getListProperty(0, intRow);
				if (intCol == 0) { // enable checkbpx
					if (intRow < ruleCount) {
						var toggle = !r.getEnabled();
						r.setEnabled(toggle);
						core.info("[" + r.toString() + "] enabled: " + r.getEnabled());
						return;
					}
				} else if (intCol == 1) { // Name
					//var rule = profile.getRule(ruleNum);
					core.info(r.toString());
					
					//choRuleNum.removeListener( "onControlEvent", onEditorControlEvent );
					choRuleNum.setSelected(intRow + 1);

					//choRuleNum.on("onControlEvent", onEditorControlEvent)
					fFillModifyTab(r);
					tabs.setValue(2);
				} else if (intCol == 2) { // remove icon
					core.info("Removing [" + r.toString() + "]...");
					profile.removeRule(intRow);
					fPopulateRulesList();
					return;
				}
				
				return;
			} else if (szControl == "btnSave") {
			//	profile.sortRules(); // Sort rules first.
				profile.saveFile();
				core.debug("Saved...");

				//	fPopulateRulesList(); // Repopulate rules list since sort might have changed the order.
				core.loadProfiles();
				fPopulateProfilesList();

				return;
			} else if (szControl == "btnSaveExit") {
			//	profile.sortRules(); // Sort rules first.
				profile.saveFile();

				//	fPopulateRulesList();// Repopulate rules list since sort might have changed the order.
				core.loadProfiles();
				fPopulateProfilesList();
				fExitEditor();
				return;
			} else if (szControl == "choRuleNum") {
				var ruleNum = control.getValue();
				core.debug("ruleNum: " + ruleNum);
				if (typeof ruleNum === "undefined" || typeof profile.data.rules[ruleNum] === "undefined") {
					core.info("Select a rule from the rule # menu.");
					return;
				}

				var rule = profile.getRule(ruleNum);
				fFillModifyTab(rule);

				return;
			} else if (szControl == "btnModify") {
				var propertyId = choProperty2.getValue();
				core.debug("propertyId: " + propertyId);
				
				if (typeof propertyId === "undefined" || typeof _properties[propertyId] === "undefined") {
					core.info("Select a property from the drop down menu.");
					return;
				}

				var operator = choOperator2.getValue();
				core.debug("operator: " + operator);
				
				if (typeof operator === "undefined" || typeof _operators[operator] === "undefined") {
					core.info("Select a operator from the drop down menu.");
					return;
				}
				
				var value = edValue2.getValue();
				core.debug("value: " + value);
				if (typeof value === "undefined") {
					core.info("Input a value into the edit box.");
					return;
				}
				
				var ruleNum = choRuleNum.getValue();
				core.debug("ruleNum: " + ruleNum);
				var rule = profile.getRule(ruleNum);
				
				if (rule.getValue() == value) {
					core.warn("Value control returned our old value (" + value + "), if you've changed it click the value box again and hit [Enter] on your keyboard and click [Modify] again.");
				}
				
				rule.setPropertyID(propertyId);
				rule.setOperator(_operators[operator]);
				rule.setValue(value);
				
				core.info("Modified " + profile.getName() + "'s [" + rule.toString() + "] rule.");
				
				tabs.setValue(0);
				fPopulateRulesList();

				return;
			} else if (szControl == "tabs") {
				if (value == 1 || value == 2) {
					fReminder("valueEnter", "When changing the value box, hit [enter] on keyboard after inputting a value.");
				}
				return;
			} else if (szControl == "btnTest") {
				return;
			} else if (szControl == "choProperty") {
				fUpdateAddSuggestions();
				return;
			} else if (szControl == "choSuggestion") {
				var v = control.getValue();
				if (v && v != "" && v != "Select...") {
					edValue.setText(v);
				}
				return;
			} else if (szControl == "choProperty2") {
				fUpdateModifySuggestions();
				return;
			} else if (szControl == "choSuggestion2") {
				var v = control.getValue();
				if (v && v != "" && v != "Select...") {
					//core.info("AA edValue2.setText(" + v + ")");
					edValue2.setText(v);
				}
				return;
			} else if (szControl == "btnEval") {
				if (!skapi.acoSelected) {
					core.info("Select a item.");
					return;
				}
				var aco = skapi.acoSelected;

				/*
				var match = profile.evaluateRules({aco:aco, inform:true});
				core.debug("match: " + match);
				core.info("'"+getFullName(aco) + "' matches all rules: " + ((match & profile.flags.match) == 1));
				*/
					
				//var cloned = core.cloneAco({aco:aco});
					
				profile.evaluateRulesAsync({
					aco   : aco,
					inform: true
				}, function onEvaleRules(err, results) {
					//core.console("<btnEval|onEvaleRules>", err, JSON.stringify(results));
					core.info("'" + core.getFullName(aco) + "' matches all rules: " + results.match);
				});
					
				return;
			} else if (szControl == "sldMaxTinks") {
				stMaxTinksValue.setText(control.getValue());
				profile.setMaxTinks(Number(control.getValue()));
				return;
			} else if (szControl == "btnSelectedValue") {
				var propertyId = choProperty.getValue();
				core.debug("propertyId: " + propertyId);
				
				if (typeof propertyId === "undefined" || typeof _properties[propertyId] === "undefined") {
					core.info("Select a property from the drop down menu.");
					return;
				}
				if (!skapi.acoSelected) {
					core.info("Select a item.");
					return;
				}
				var aco = skapi.acoSelected;
				
				var property = _properties[propertyId];
				var value = property.getValue(skapi.acoSelected);
				core.debug("value: " + value);
				if (typeof value !== "undefined") {
					edValue.setText(value);
				} else {
					core.info("Could not find " + property.getName() + " property for " + core.getFullName(aco) + ".");
				}
				return;
			} else if (szControl == "btnSelectedValue2") {
				var propertyId = choProperty2.getValue();
				core.debug("propertyId: " + propertyId);
				
				if (typeof propertyId === "undefined" || typeof _properties[propertyId] === "undefined") {
					core.info("Select a property from the drop down menu.");
					return;
				}
				if (!skapi.acoSelected) {
					core.info("Select a item.");
					return;
				}
				var aco = skapi.acoSelected;
				
				var property = _properties[propertyId];
				var value = property.getValue(skapi.acoSelected);
				core.debug("value: " + value);
				if (typeof value !== "undefined") {
					//core.info("AB edValue2.setText(" + value + ")");
					edValue2.setText(value);
				} else {
					core.info("Could not find " + property.getName() + " property for " + core.getFullName(aco) + ".");
				}
				return;
			} else if (szControl == "chkIncludeImbune") {
				//profile.setIncludeImbuneTink(control.getValue());
			//	core.info("value: " + control.getChecked());
				profile.setIncludeImbuneTink(control.getChecked());
				
				if (control.getChecked() == true && profile.getMaxTinks() == 10) {
					core.info("If you're including imbune, you must lower max tinks to <= 9 since imbune counts as a tink.", cmcCoral);
				}
				return;
			}

			core.debug("<onEditorControlEvent> '" + szControl + "' '" + control.getValue() + "' '" + value + "'");
		}

		function fPopulateRulesList() {
			//var rules = profile.getRules();
			
			var scrollPosition = lstRules.getScrollPosition();
			
			var ruleCount = profile.data.rules.length;
			
			lstRules.clear();
			
			//var raw;
			var r;
			for (var i = 0; i < ruleCount; i++) {
				//raw = rules[i];
				//r = rules[i];
				
				r = profile.getRule(i);
				
				core.debug("i: " + i);
				core.debug("getEnabled: " + r.getEnabled());
				core.debug("toString: " + r.toString());

				lstRules.addRow(r.getEnabled(), r.toString(), (0x06000000 + 4600));
			}

			lstRules.jumpToPosition(scrollPosition);
		}

		function compare_suggestions(a, b) {
			if (a.name < b.name) return -1;
			if (a.name > b.name) return 1;
			return 0;
		}
		
		function fUpdateAddSuggestions() {
			var propertyId = choProperty.getValue();
			core.debug("<fUpdateAddSuggestions> propertyId: " + propertyId);
			choSuggestion.clear();
			choSuggestion.addOption("Select...");
			choSuggestion.setSelected(0);
			if (!_properties[propertyId]) {
				return;
			}
			var property = _properties[propertyId];
			var suggestions = property.getSuggestions();
			if (!suggestions) {
				return;
			}
			var sorted = [];
			var v;
			for (var szName in suggestions)  {
				if (!suggestions.hasOwnProperty(szName)) continue;
				v = suggestions[szName];
				sorted.push({value: v, name: szName});
			}

			sorted.sort(compare_suggestions);
			for (var i = 0; i < sorted.length; i++) {
				v = sorted[i];
				choSuggestion.addOption({text: v.name, data: v.value});//
			}
		}
		
		function compare_suggestions2(a, b) {
			if (a.name < b.name) return -1;
			if (a.name > b.name) return 1;
			return 0;
		}
		
		function fUpdateModifySuggestions() {
			var propertyId = choProperty2.getValue();
			core.debug("<fUpdateModifySuggestions> propertyId: " + propertyId);
			choSuggestion2.clear();
			choSuggestion2.addOption({text: "Select..."});
			choSuggestion2.setSelected(0);
			if (!_properties[propertyId]) {
				return;
			}
			var property = _properties[propertyId];
			var suggestions = property.getSuggestions();
			if (!suggestions) {
				return;
			}
			var selectedIndex = 0;
			var sorted = [];
			var v;
			for (var szName in suggestions)  {
				if (!suggestions.hasOwnProperty(szName)) continue;
				v = suggestions[szName];
				sorted.push({data: v, text: szName});
			}

			sorted.sort(compare_suggestions2);
			for (var i = 0; i < sorted.length; i++) {
				v = sorted[i];
				choSuggestion2.addOption(v);//
				core.info(" suggestion2 " + JSON.stringify(v));
				if (v.value == edValue2.getValue()) {
					selectedIndex = i + 1;
				}
			}
			
			core.debug("selectedIndex: " + selectedIndex);
			choSuggestion2.setSelected(selectedIndex);
		}

		var view = core.createView({
			title : title, 
			width : editorWidth, 
			height: editorHeight,
			icon  : (profile.getIcon() || 0)
		});
		
		
		var fixedLayout = new SkunkSchema.FixedLayout({parent: view});
		
		
		var edProfileName = new SkunkSchema.Edit({parent: fixedLayout, name: "edProfileName", text: profile.getName()})
			.setWidth(130)
			.on("onControlEvent", onEditorControlEvent);

		var btnUpdateIcon = new SkunkSchema.PushButton({parent: fixedLayout, name: "btnUpdateIcon", text: "Update Icon"})
			.setAnchor(fixedLayout, "TOPRIGHT", "TOPRIGHT")
			.setWidth(70)
			.on("onControlEvent", onEditorControlEvent);

		var tabs = new SkunkSchema.Notebook({
			parent: fixedLayout, 
			name  : "tabs"
		})
			.setAnchor(edProfileName, "BOTTOMLEFT")
			.setWidth(fixedLayout.getWidth())
			.setHeight(fixedLayout.getHeight() - 40);

		var pageRules = new SkunkSchema.Page({
			parent: tabs, 
			label : "Rules"
		});

		var lstRules = new SkunkSchema.List({
			parent: pageRules, 
			name  : "lstRules"
		})
			.addColumn({progid: "DecalControls.CheckColumn"}) // enabled
			.addColumn({progid: "DecalControls.TextColumn", fixedwidth: pageRules.getWidth() - 60}) // name
			.addColumn({progid: "DecalControls.IconColumn"}) // delete
			.on("onControlEvent", onEditorControlEvent);
	
	
		// Tab page for adding a new rule.
		var flAdd = new SkunkSchema.FixedLayout({
			parent: new SkunkSchema.Page({
				parent: tabs, 
				label : "Add"
			})
		});
	
		// Static text.
		var stProperty = new SkunkSchema.StaticText({
			parent: flAdd, 
			name  : "stProperty", 
			text  : "Property:"
		})
			.setWidth(50);
		
		
		// Drop down menu for available properties.
		var choProperty = new SkunkSchema.Choice({
			parent: flAdd, 
			name  : "choProperty"
		})
			.setAnchor(stProperty, "TOPRIGHT")
			.setWidth(flAdd.getWidth() - 50)
			.on("onControlEvent", onEditorControlEvent)
			.addOption({text: "Select..."});		
		
		
		// Populate the drop down menu.
		var properties = [];
		var p;
		for (var id in _properties)  {
			if (!_properties.hasOwnProperty(id)) continue;
			p = _properties[id];

			//p = new _properties[id]();
			//debug("name: " + id + ", name: " + p.getName());
			properties.push({data: id, text: p.getName()});
		}
		function compare(a, b) {
			if (a.text < b.text) return -1;
			if (a.text > b.text) return 1;
			return 0;
		}
		properties.sort(compare);
		for (var i = 0; i < properties.length; i++) {
			p = properties[i];
			choProperty.addOption(p);
		}
		
		// Static text for operator.
		var stOperator = new SkunkSchema.StaticText({parent: flAdd, name: "stOperator", text: "Operator: "})
			.setAnchor(stProperty, "BOTTOMLEFT")
			.setWidth(50);


		// Drop down menu for available properties.
		var choOperator = new SkunkSchema.Choice({
			parent: flAdd, 
			name  : "choOperator"
		})
			.setAnchor(stOperator, "TOPRIGHT")
			.setWidth(flAdd.getWidth() - 50)
			.on("onControlEvent", onEditorControlEvent)
			.addOption({text: "Select..."});	

		// Populate the available operators.
		var v;
		for (var i = 0; i < _operators.length; i++) {
			v = _operators[i];
			choOperator.addOption({
				text: escapeXml(v),
				data: i
			});
		}
		
		// Static text for value field.
		var stValue = new SkunkSchema.StaticText({parent: flAdd, name: "stValue", text: "Value: "})
			.setAnchor(stOperator, "BOTTOMLEFT")
			.setWidth(50);

		var edValue = new SkunkSchema.Edit({parent: flAdd, name: "edValue", text: ""})
			.setAnchor(stValue, "TOPRIGHT")
			.setWidth(flAdd.getWidth() - 50 - 40);

		//.on("onControlEvent", onControlEvent);

		var btnSelectedValue = new SkunkSchema.PushButton({parent: flAdd, name: "btnSelectedValue", text: "Selected"})
			.setAnchor(edValue, "TOPRIGHT")
			.setWidth(40)
			.on("onControlEvent", onEditorControlEvent);
			

		// Suggested values
		var stSuggestion = new SkunkSchema.StaticText({parent: flAdd, name: "stSuggestion", text: "Suggestion"})
			.setAnchor(stValue, "BOTTOMLEFT")
			.setWidth(50);

		var choSuggestion = new SkunkSchema.Choice({
			parent: flAdd, 
			name  : "choSuggestion"
		})
			.setAnchor(stSuggestion, "TOPRIGHT")
			.setWidth(flAdd.getWidth() - 50)
			.on("onControlEvent", onEditorControlEvent)
			.addOption({text: "Select..."});	

		var btnAdd = new SkunkSchema.PushButton({parent: flAdd, name: "btnAdd", text: "Add"})
			.setAnchor(choSuggestion, "BOTTOMLEFT")
			.setWidth(40)
			.on("onControlEvent", onEditorControlEvent);

		
		////////// Modify Page.
		var flModify = new SkunkSchema.FixedLayout({
			parent: new SkunkSchema.Page({
				parent: tabs, 
				label : "Modify"
			})
		});
	
		var stRuleNum = new SkunkSchema.StaticText({parent: flModify, name: "stRuleNum", text: "Rule #: "})
			.setWidth(50);

		var choRuleNum = new SkunkSchema.Choice({
			parent: flModify, 
			name  : "choRuleNum"
		})
			.setAnchor(stRuleNum, "TOPRIGHT")
			.setWidth(flModify.getWidth() - 50)
			.on("onControlEvent", onEditorControlEvent)
			.addOption({text: "Select..."});	

		fPopulateRuleNumMenu();
	
		// Static text.
		var stProperty2 = new SkunkSchema.StaticText({parent: flModify, name: "stProperty2", text: "Property:"})
			.setWidth(50)
			.setAnchor(stRuleNum, "BOTTOMLEFT");
		
		// Drop down menu for available properties.
		var choProperty2 = new SkunkSchema.Choice({
			parent: flModify, 
			name  : "choProperty2"
		})
			.setAnchor(stProperty2, "TOPRIGHT")
			.setWidth(flModify.getWidth() - 50)
			.on("onControlEvent", onEditorControlEvent)
			.addOption({text: "Select..."});

		for (var i = 0; i < properties.length; i++) {
			p = properties[i];
			choProperty2.addOption(p);
		}
		
		// Static text for operator.
		var stOperator2 = new SkunkSchema.StaticText({parent: flModify, name: "stOperator2", text: "Operator:"})
			.setWidth(50)
			.setAnchor(stProperty2, "BOTTOMLEFT");
		
		// Drop down menu for available properties.
		var choOperator2 = new SkunkSchema.Choice({
			parent: flModify, 
			name  : "choOperator2"
		})
			.setAnchor(stOperator2, "TOPRIGHT")
			.setWidth(flModify.getWidth() - 50)
			.addOption({text: "Select..."});

		for (var i = 0; i < _operators.length; i++) {
			v = _operators[i];

			//	core.debug("v: " + v.xml);
			choOperator2.addOption({
				text: escapeXml(v), 
				data: i
			});
		}
		
		// Value box.
		// Static text for value field.
		var stValue2 = new SkunkSchema.StaticText({parent: flModify, name: "stValue2", text: "Value:"})
			.setAnchor(stOperator2, "BOTTOMLEFT")
			.setWidth(50);

		var edValue2 = new SkunkSchema.Edit({parent: flModify, name: "edValue2", text: ""})
			.setAnchor(stValue2, "TOPRIGHT")
			.setWidth(flModify.getWidth() - 50 - 40);

		var btnSelectedValue2 = new SkunkSchema.PushButton({parent: flModify, name: "btnSelectedValue2", text: "Selected"})
			.setAnchor(edValue2, "TOPRIGHT")
			.setWidth(40)
			.on("onControlEvent", onEditorControlEvent);

		var stSuggestion2 = new SkunkSchema.StaticText({parent: flModify, name: "stSuggestion2", text: "Suggestion"})
			.setAnchor(stValue2, "BOTTOMLEFT")
			.setWidth(50);

		var choSuggestion2 = new SkunkSchema.Choice({
			parent: flModify, 
			name  : "choSuggestion2"
		})
			.setAnchor(stSuggestion2, "TOPRIGHT")
			.setWidth(flModify.getWidth() - 50)
			.addOption({text: "Select..."})
			.on("onControlEvent", onEditorControlEvent);

		var btnModify = new SkunkSchema.PushButton({parent: flModify, name: "btnModify", text: "Modify"})
			.setAnchor(choSuggestion2, "BOTTOMLEFT")
			.setWidth(40)
			.on("onControlEvent", onEditorControlEvent);
		
		// Config page.
		var flConfig = new SkunkSchema.FixedLayout({
			parent: new SkunkSchema.Page({
				parent: tabs, 
				label : "Config"
			})
		});
	
	
		var stMaxTinks = new SkunkSchema.StaticText({parent: flConfig, name: "stMaxTinks", text: "Max Tinks"});

		var sldMaxTinks = new SkunkSchema.Slider({
			parent: flConfig, 
			name  : "sldMaxTinks"
		})
			.setAnchor(stMaxTinks, "TOPRIGHT")
			.on("onControlEvent", onEditorControlEvent)
			.setWidth(80)
			.setMaximum(10)
			.setValue(profile.getMaxTinks());

		var stMaxTinksValue = new SkunkSchema.StaticText({parent: flConfig, name: "stMaxTinksValue", text: profile.getMaxTinks()})
			.setAnchor(sldMaxTinks, "TOPRIGHT");

		var chkIncludeImbune = new SkunkSchema.Checkbox({
			parent : flConfig, 
			name   : "chkIncludeImbune", 
			text   : "Include imbune",
			checked: profile.getIncludeImbuneTink()
		})
			.setAnchor(stMaxTinks, "BOTTOMLEFT")
			.on("onControlEvent", onEditorControlEvent);

		var stImbuneNote = new SkunkSchema.StaticText({parent: flConfig, name: "stImbuneNote", text: "(Start tink calc at +1)"})
			.setAnchor(chkIncludeImbune, "TOPRIGHT");

		// Footer.
		var btnExit = new SkunkSchema.PushButton({
			parent: fixedLayout, 
			name  : "btnExit", 
			text  : "Exit"
		})
			.setAnchor(fixedLayout, "BOTTOMRIGHT", "BOTTOMRIGHT")
			.setWidth(30)
			.on("onControlEvent", onEditorControlEvent);

		var btnSaveExit = new SkunkSchema.PushButton({
			parent: fixedLayout, 
			name  : "btnSaveExit", 
			text  : "Save &amp; Exit"
		})
			.setAnchor(btnExit, "TOPLEFT", "TOPRIGHT")
			.setWidth(60)
			.on("onControlEvent", onEditorControlEvent);

		var btnSave = new SkunkSchema.PushButton({
			parent: fixedLayout, 
			name  : "btnSave", 
			text  : "Save"
		})
			.setAnchor(btnSaveExit, "TOPLEFT", "TOPRIGHT")
			.setWidth(30)
			.on("onControlEvent", onEditorControlEvent);

		var btnEval = new SkunkSchema.PushButton({
			parent: fixedLayout, 
			name  : "btnEval", 
			text  : "Eval"
		})
			.setAnchor(fixedLayout, "BOTTOMLEFT", "BOTTOMLEFT")
			.setWidth(40)
			.on("onControlEvent", onEditorControlEvent);
	
		/**/
			
		fShowEditor();
	}
	
	core.matchingProfiles = {};

	core.getMatchingProfiles = (function factory() {
		function matched(e) {
			return e && e.match;
		}
		return function getMatchingProfiles(params, callback) {
			core.debug("<getMatchingProfiles>", params.aco);
			core.debug("_loadedProfiles: " + _loadedProfiles.length);
			
			var aco = params.aco;
			var cancellation = params.cancellation || new core.Cancellation();
			var thisArg = params.thisArg;
			
			var cache = false; // Cache the result for quicker look ups. Off by default to prevent memory leak.
			if (typeof params.cache !== "undefined") {
				cache = params.cache;
			}

			if (core.matchingProfiles[aco.oid]) {
				// Return cache async
				return core.setImmediate(core.applyMethod, thisArg, callback, [undefined, core.matchingProfiles[aco.oid]]);
			}

			async_js.concatSeries(_loadedProfiles, function checkProfile(p, callback) {
				core.debug("<getMatchingProfiles|checkProfile>", p.getName());
				if (cancellation && cancellation.canceled) return callback(new Error("CANCELLATION"));
				if (!p.getEnabled()) return callback(); 

				p.evaluateRulesAsync({
					aco         : aco,
					cancellation: cancellation
				}, function onEvaleRules(err, results) {
					//core.console("<getMatchingProfiles|checkProfile|onEvaleRules>", err, JSON.stringify(results));
					if (err) return callback(err);
					if (cancellation && cancellation.canceled) return callback(new Error("CANCELLATION"));

					var size = core.getCallStackSize();

					//core.info("size: " + size);
					
					if (results && results.match) {
						if (size < maxCallSize) return callback(undefined, results);
						return core.setImmediate(callback, undefined, results);
					}

					if (size < maxCallSize) return callback(undefined);
					return core.setImmediate(callback);
					
				});
				
			}, function finishedProfiles(err, results) {
				core.debug("<getMatchingProfiles|finishedProfiles>", err, results);
				if (err) return callback.call(thisArg, err);
				if (cancellation && cancellation.canceled) return callback.call(thisArg, new Error("CANCELLATION")); 
				
				//core.console("finishedProfiles", err, JSON.stringify(results, null, "\t"));
				
				//results = results.filter(matched);  // Remove nulls from array.

				if (cache == true) {
					core.matchingProfiles[aco.oid] = results;
				}
				callback.call(thisArg, undefined, results);
			});
		};
	})();

	core.getMatchingProfilesSync = (function factory() {
		function _evalRules(profile) {
			if (!profile.getEnabled()) return false;
			return profile.evaluateRulesSync({aco: this.aco, verbose: this.verbose});
		}
		function _justName(profile) {
			return profile.data.name;
		}
		return function getMatchingProfilesSync(params) {
			var aco = params.aco;
			var verbose = (!params || params.verbose === undefined ? core.debug : params.verbose);
			var mode = (!params || params.mode === undefined ? "any" : params.mode);
			if (mode == "any") {
				var profile = _loadedProfiles.find(_evalRules, {aco: aco, verbose: verbose});//.map(_justName);
				return profile && [profile.data.name] || [];
			} else if (mode == "all") {
				return _loadedProfiles.filter(_evalRules, {aco: aco, verbose: verbose}).map(_justName);
			}
		};
	})();

	function compare_inv(a, b) {
		if (a.mcm != b.mcm) {
			if (a.mcm < b.mcm) return -1;
			if (a.mcm > b.mcm) return 1;
		}
		
		if (a.eqm != b.eqm) {
			if (a.eqm < b.eqm) return -1;
			if (a.eqm > b.eqm) return 1;
		}

		if (a.mcm == mcmSalvageBag && b.mcm == mcmSalvageBag) {
			if (core.getFullName(a) == core.getFullName(b)) {
				if (a.workmanship < b.workmanship) return -1;
				if (a.workmanship > b.workmanship) return 1;
			}
			if (core.getFullName(a) < core.getFullName(b)) return -1;
			if (core.getFullName(a) > core.getFullName(b)) return 1;
		}
			
		if (a.szName < b.szName) return -1;
		if (a.szName > b.szName) return 1;
		
		return 0;
	}
	
	function fSortInventory() {
		// Get items in inventory and stick it into a array that we can sort.
		var inventory = [];
		var acf = skapi.AcfNew();
		acf.olc = olcInventory;
		var coaco = acf.CoacoGet();
		var aco;
		for (var i = 0; i < coaco.Count; i++) {
			aco = coaco.Item(i);
			inventory.push(aco);
		}
		inventory.sort(compare_inv);

		core.debug("inventory: " + inventory.length);

		// Get number of real packs, not foci
		var pack = skapi.cpack;
		var cpack = 0;
		while (--pack) {
			if (!aco || !aco.fExists || !aco.fInInventory) {
			//	core.debug("   Item no longer exists");
				return false;
			}
		
			acoPack = skapi.AcoFromIpackIitem(pack, iitemNil);
			if (acoPack && acoPack.citemMax > 0) {
				cpack++;
			}
		}
		core.debug("cpack: " + cpack);
		
		// Split inventory into pack size slots. Doing this method so I can loop through slots 1-2-3 and maintain the sorting order from pack to pack.
		var packs = [];
		do {
			if (packs.length >= cpack) {
				break;
			}
			
			packs.push(inventory.splice(Math.max(inventory.length - 24, 0), 24));
			core.debug("packs: " + packs.length + "(" + packs[packs.length - 1].length + "), inventory: " + inventory.length);
		} while (inventory.length > 0);
		core.debug("inventory for main pack: " + inventory.length);
		
		// Start sorting items in side packs.
		var acoPack;
		var aco;
		var packSlot;
		var skipPacks = 0;
		for (var pi = 0; pi < packs.length; pi++) {
			core.debug("pi: " + pi);
			do {
				packSlot = skapi.cpack - (pi + 1 + skipPacks);
				acoPack = skapi.AcoFromIpackIitem(packSlot, iitemNil);
				core.debug("packSlot: " + packSlot + ", skipPacks: " + skipPacks + ", acoPack: " + acoPack + ", citemMax: " + (acoPack && acoPack.citemMax));
				if (acoPack && acoPack.citemMax == 0) {
					skipPacks++;
					continue;
				}
				break;//don't loop
			} while (true);

			
			core.debug("pi: " + pi + ", acoPack: " + acoPack + ", packSlot: " + packSlot);
			skapi.SelectAco(acoPack);
			acoPack.Use();
			skapi.WaitEvent(100, wemFullTimeout);

			for (var i = 0; i < packs[pi].length; i++) {
				aco = packs[pi][i];
				
				acoPrev = skapi.AcoFromIpackIitem(packSlot, i);
				
				if (acoPrev && acoPrev.oid == aco.oid) continue;
				
				if (acoPrev && acoPack.citemContents >= acoPack.citemMax) {
					if (acoPrev.oid == aco.oid) continue;

					//	core.debug("Moving " + core.getFullName(acoPrev) + " out of the way.");
					acoPrev.MoveToPack(0, 0, true); // move previous item to main pack.
					skapi.WaitEvent(1000, wemSpecific, evidOnAddToInventory);

				//	skapi.WaitEvent(100, wemFullTimeout);
				}
				
				core.debug("pi: " + pi + " (" + packSlot + "), i: " + i + ", aco: " + core.getFullName(aco));
				aco.MoveToPack(packSlot, i);
				skapi.WaitEvent(1000, wemSpecific, evidOnAddToInventory);
			}
		}
		
		
		// Sort the main pack.
		acoPack = skapi.AcoFromIpackIitem(0, iitemNil);
		skapi.SelectAco(acoPack);
		acoPack.Use();
		
		// Do main pack;
		for (var i = 0; i < inventory.length; i++) {
			aco = inventory[i];
			acoPrev = skapi.AcoFromIpackIitem(0, i);
			if (acoPrev && acoPrev.oid == aco.oid) continue;
			
			core.debug("p: " + 0 + ", i: " + i + ", aco: " + core.getFullName(aco));
			aco.MoveToPack(0, i);
			skapi.WaitEvent(1000, wemSpecific, evidOnAddToInventory);
		}
		

		core.info("Done");
	}
	
	function fControlEvent(control, szPanel, szControl, value) {
		
		
		if (szControl == "sldChanceThreshold") {
			//var min = 
			var v = round(control.getValue() / 2 + 50);

			//debug("v: " + v);
			prgChanceThreshold.setValue(v);
			_settings.tinkChanceThreshold = v;
			
			fSaveSettings();
			return;
		} else if (szControl == "edArmorTinkering") {
			_settings.skills[skidArmorTinkering] = control.getValue();
			fSaveSettings();
			return;
		} else if (szControl == "edItemTinkering") {
			_settings.skills[skidItemTinkering] = control.getValue();
			fSaveSettings();
			return;
		} else if (szControl == "edMagicItemTinkering") {
			_settings.skills[skidMagicItemTinkering] = control.getValue();
			fSaveSettings();
			return;
		} else if (szControl == "edWeaponTinkering") {
			_settings.skills[skidWeaponTinkering] = control.getValue();
			fSaveSettings();
			return;
		} else if (szControl == "edSalvageWorkmanship") {
			_settings.salvageWorkmanship = control.getValue();
			core.debug("salvageWorkmanship: " + _settings.salvageWorkmanship);
			fSaveSettings();
			return;
		} else if (szControl == "btnNew") {
			var profile = new Profile();
			profile.setName(SimpleUUID());
			profile.setEnabled(true);
			fShowProfileEditor(profile);
			return;
		} else if (szControl == "lstProfiles") {
			var intCol = parseInt(value.split(",")[0]);
			var intRow = parseInt(value.split(",")[1]);
				
			core.matchingProfiles = {};
			
			var p = _loadedProfiles[intRow];	
			if (intCol == 0) { // global enable checkbox
				if (intRow < _loadedProfiles.length) {
					var toggle = !p.getEnabled();
					p.setEnabled(toggle);
					p.saveFile();
					core.info("[" + p.getName() + "] global enabled: " + p.getEnabled());
					return;
				}
			} else if (intCol >= 1 && intCol <= 2) { // Icon and name
				fShowProfileEditor(p);
				return;
			} else if (intCol == 3) {
				core.info("Deleting '" + p.getName() + "'...");
				p.deleteFile();
				core.loadProfiles();
				fPopulateProfilesList();
				return;
			}

			core.debug("intCol: " + intCol + ", intRow: " + intRow);
			return;
		} else if (szControl == "lstDetected") {
			var intCol = parseInt(value.split(",")[0]);
			var intRow = parseInt(value.split(",")[1]);
			
			core.debug("<lstDetected> " + intCol + ", " + intRow);

			var row = control.rows[intRow];
			var oid = row[0];
			var aco = skapi.AcoFromOid(oid);
			if (!aco || !aco.fExists) {
				core.warn("Item isn't in client memory anymore.");
				return;
			}
			
			skapi.SelectAco(aco);
			
			/*
			var itemString = getItemString({aco:aco});
			core.info(itemString);
			
			
			core.getMatchingProfiles({
				aco:aco,
				cache: true
			}, function onSelectedMatches(err, results) {

				core.info(getFullName(aco) + " matches " + results.length + " profiles.");
				
				var p;
				var tinks;
				for ( var i=0; i < results.length; i++ ) {
					p = results[i].profile;
					tinks = results[i].neededTinks; //p.getNeededTinks();
					if (tinks && tinks > 0) {
						core.info("  " + p.getName() + " (" + tinks + " tinks)" );
					} else {
						core.info("  " + p.getName() );
					}
				}
			});
			*/

		} else if (szControl == "btnSelected") {
			if (!skapi.acoSelected) {
				core.info("Select a item.");
				return;
			}
			
			var aco = skapi.acoSelected; //core.cloneAco({aco:skapi.acoSelected});
			//core.console("aco2", JSON.stringify(aco2, null, "\t"));

			core.sayMatchingProfiles({aco: aco});

			/*
			Sync method
			var matches = findMatchingProfiles(aco);
			
			if (matches.length == 0) {
				core.info(getFullName(skapi.acoSelected) + " doesn't match any profiles.");
				return;
			}
			
			core.info(getFullName(skapi.acoSelected) + " matches the following profiles.");
			
			var p;
			var tinks;
			for ( var i=0; i < matches.length; i++ ) {
				p = matches[i];
				tinks = p.getNeededTinks();
				if (tinks && tinks>0) {
					core.info("  " + p.getName() + " (" + tinks + " tinks)" );
				} else {
					core.info("  " + p.getName() );
				}
			}
			*/
			
			return;
		} else if (szControl == "btnQuit") {
			//end();
			core.disable();
			return;
		} else if (szControl == "btnTest") {
			

			return;
		} else if (szControl == "btnClearConsole") {
			console.Clear();
			core.info("Cleared Console.");
			return;
		} else if (szControl == "btnSortProfileRules") {
			core.info("Sorting profile rules...");
			var startingTime = new Date();
			
			var p;
			for (var i = 0; i < _loadedProfiles.length; i++) {
				p = _loadedProfiles[i];
				p.sortRules();
				p.saveFile();
			}
			
			var elapsed = new Date() - startingTime;
			core.info("Sorted " + _loadedProfiles.length + " profiles in " + round(elapsed / 1000, 2) + " seconds.");
			
			return;

		/*
		} else if (szControl == "btnIncreaseMaxTinks") {
			var p;
			var increased = 0;
			for ( var i=0; i < _loadedProfiles.length; i++ ) {
				p = _loadedProfiles[i];
				if (p.getMaxTinks() == 0 || p.getMaxTinks() == 10) continue;
				//core.console(p.getName() + " " + p.getMaxTinks());
				if (p.getMaxTinks() == 9) {
					core.info("Increased " + p.getName() + "'s max tinks to 10.");
					p.setMaxTinks(10);
					p.saveFile();
					increased += 1;
				}
			}
			core.info("Increased " + increased + " profiles max tinks to 10.");*/
		} else if (szControl == "btnProperties") {
			if (!skapi.acoSelected) {
				core.info("Select a item.");
				return;
			}
			var aco = skapi.acoSelected;
			
			core.info(core.getFullName(aco) + " (" + aco.oid + ")");
			
			var properties = [];
			var p;
			for (var id in _properties)  {
				if (!_properties.hasOwnProperty(id)) continue;
				p = _properties[id];

				//p = new _properties[id]();
				//	core.debug("name: " + id + ", name: " + p.getName());
				properties.push({p: p, name: p.getName()});
			}
			
			function compare(a, b) {
				if (a.name < b.name) return -1;
				if (a.name > b.name) return 1;
				return 0;
			}
			properties.sort(compare);
			var v;
			var suggestions;
			var szSuggestion;
			for (var i = 0; i < properties.length; i++) {
				p = properties[i].p;
				
				//core.console(i, p.data.id, p.data.name)
				
				if (p.valueExists(aco)) {
					if (typeof p.getValue !== "undefined") {
						v = p.getValue(aco);
					} else if (typeof p.getValues !== "undefined") {
						v = p.getValues(aco);
					}

					//core.console("v: " + v)

					if (typeof v !== "undefined") {
						suggestions = p.getSuggestions();
						szSuggestion = undefined;
						if (suggestions) {
							for (var szName in suggestions)  {
								if (!suggestions.hasOwnProperty(szName)) continue;

								//v = suggestions[szName];
								//sorted.push({value:v, name:szName});
								
								if (v == suggestions[szName]) {
									szSuggestion = szName;
									break;
								}
							}
						}
						
						//v = p.getValue && p.getValue(aco) || p.getValues && p.getValues(aco);
						core.debug(p.getName() + ", valueExists: " + p.valueExists(aco) + ", v: " + v + ", szSuggestion: " + szSuggestion);

						//if (typeof v !== "undefined")
						//{
						
						if (szSuggestion) {
							core.info(p.getName() + ": " + v + " (" + szSuggestion + ")");
						} else {
							core.info(p.getName() + ": " + v);
						}
						
							
						//}
					}
				}
			}
		} else if (szControl == "btnRGBVariance") {
			if (!skapi.acoSelected) {
				core.info("Select a item.");
				return;
			}
			var aco = skapi.acoSelected;
			
			var aRGB = edRGB.getValue();
			core.debug("aRGB: " + aRGB);
			
			var diff;
			
			var acoValue = _properties["palette0RGB"].getValue(aco);
			if (acoValue) {
				diff = getRGBPeakVariance(acoValue, aRGB);
				core.info("palette0RGB variance: " + diff);
			}
			
			acoValue = _properties["palette1RGB"].getValue(aco);
			if (acoValue) {
				diff = getRGBPeakVariance(acoValue, aRGB);
				core.info("palette1RGB variance: " + diff);
			}
			
			acoValue = _properties["palette2RGB"].getValue(aco);
			if (acoValue) {
				diff = getRGBPeakVariance(acoValue, aRGB);
				core.info("palette2RGB variance: " + diff);
			}
			
			acoValue = _properties["palette3RGB"].getValue(aco);
			if (acoValue) {
				diff = getRGBPeakVariance(acoValue, aRGB);
				core.info("palette3RGB variance: " + diff);
			}
			
			return;
		} else if (szControl == "btnListPalettes") {
			if (!skapi.acoSelected) {
				core.info("Select a item.");
				return;
			}
			var aco = skapi.acoSelected;
			var palettes = data.palettes[aco.oid] || core.getPalettes(aco.oid);
			if (!palettes) { //data.palettes[aco.oid]
				core.info("No palettes cached for " + core.getFullName(aco) + ". You'll need the client to forget and see the item again.");
				return;
			}
			
			var palette0 = _properties["palette0"].getValue(aco);
			var palette1 = _properties["palette1"].getValue(aco);
			var palette2 = _properties["palette2"].getValue(aco);
			var palette3 = _properties["palette3"].getValue(aco);
			
			var palette0RGB = _properties["palette0RGB"].getValue(aco);
			var palette1RGB = _properties["palette1RGB"].getValue(aco);
			var palette2RGB = _properties["palette2RGB"].getValue(aco);
			var palette3RGB = _properties["palette3RGB"].getValue(aco);
			
			var palette0Heu = _properties["palette0Heu"].getValue(aco);
			var palette1Heu = _properties["palette1Heu"].getValue(aco);
			var palette2Heu = _properties["palette2Heu"].getValue(aco);
			var palette3Heu = _properties["palette3Heu"].getValue(aco);
			
			var palette0Sat = _properties["palette0Sat"].getValue(aco);
			var palette1Sat = _properties["palette1Sat"].getValue(aco);
			var palette2Sat = _properties["palette2Sat"].getValue(aco);
			var palette3Sat = _properties["palette3Sat"].getValue(aco);
			
			var palette0lgt = _properties["palette0lgt"].getValue(aco);
			var palette1lgt = _properties["palette1lgt"].getValue(aco);
			var palette2lgt = _properties["palette2lgt"].getValue(aco);
			var palette3lgt = _properties["palette3lgt"].getValue(aco);
			
			
			core.info("Palettes for " + core.getFullName(aco));
			
			if (palette0) {
				core.info("  palette0: " + palette0 + ", RGB: " + palette0RGB + ", Heu: " + round(palette0Heu, 2) + ", Sat: " + round(palette0Sat, 2) + ", Light: " + round(palette0lgt, 2));
			}
			if (palette1) {
				core.info("  palette1: " + palette1 + ", RGB: " + palette1RGB + ", Heu: " + round(palette1Heu, 2) + ", Sat: " + round(palette1Sat, 2) + ", Light: " + round(palette1lgt, 2));
			}
			if (palette2) {
				core.info("  palette2: " + palette2 + ", RGB: " + palette2RGB + ", Heu: " + round(palette2Heu, 2) + ", Sat: " + round(palette2Sat, 2) + ", Light: " + round(palette2lgt, 2));
			}
			if (palette3) {
				core.info("  palette3: " + palette3 + ", RGB: " + palette3RGB + ", Heu: " + round(palette3Heu, 2) + ", Sat: " + round(palette3Sat, 2) + ", Light: " + round(palette3lgt, 2));
			}
			
			return;
		} else if (szControl == "chkDebugging") {
			_settings.debugging = control.getChecked();
			core.info("Debugging: " + _settings.debugging);
			fSaveSettings();
			return;
		} else if (szControl == "chkAssessChests") {
			_settings.chkAssessChests = control.getChecked();
			fSaveSettings();
		} else if (szControl == "chkAssessTrade") {
			_settings.chkAssessTrade = control.getChecked();
			fSaveSettings();
		} else if (szControl == "chkLootChests") {
			_settings.chkLootChests = control.getChecked();
			fSaveSettings();
		} else if (szControl == "chkAssessCorpses") {
			_settings.chkAssessCorpses = control.getChecked();
			fSaveSettings();
		} else if (szControl == "chkLootCorpses") {
			_settings.chkLootCorpses = control.getChecked();
			fSaveSettings();

			/*
		} else if (szControl == "btnInvSearch") {
			var text = btnInvSearch.getText();
			btnInvSearch.setText("...");
			//btnInvSearch.setText(text);
			
			core.launchInventorySearchOld(null, function(err, results) {
				btnInvSearch.setText(text);
				if (err) return dumpException(err);
			});
			
			
			return;
			*/
		} else if (szControl == "btnJunkSearch") {
			var text = btnJunkSearch.getText();
			btnJunkSearch.setText("...");

			//fShowInvSearchPanel();
			core.fShowJunkSearchPanel();
			btnJunkSearch.setText(text);
			return;
		} else if (szControl == "btnSuitCreator") {
			var text = btnSuitCreator.getText();
			btnSuitCreator.setText("...");

			//fShowInvSearchPanel();
			//core.fShowJunkSearchPanel();
			core.fShowSuitCreatorPanel();
			btnSuitCreator.setText(text);
			return;
		} else if (szControl == "btnSortInventory") {
			var text = control.getText();
			control.setText("...");
			fSortInventory();
			control.setText(text);
			return;
		} else if (szControl == "btnCantripCleaner") {
			var text = control.getText();
			control.setText("...");
			core.fShowCantripCleanerPanel();
			control.setText(text);
			return;
		} else if (szControl == "btnAetheriaCleaner") {
			var text = control.getText();
			control.setText("...");
			core.fShowAetheriaCleanerPanel();
			control.setText(text);
			return;
		} else if (szControl == "btnSalvageCleaner") {
			var text = control.getText();
			control.setText("...");
			core.fShowSalvageCleanerPanel();
			control.setText(text);
			return;
		} else if (szControl == "btnPaletteMatcher") {
			var text = control.getText();
			control.setText("...");
			core.fShowPaletteMatcherPanel();
			control.setText(text);
			return;
		} else if (szControl == "btnSaveInv") {
			var text = btnSaveInv.getText();
			btnSaveInv.setText("...");

			core.saveInventory(null, function(err, results) {
				btnSaveInv.setText(text);
				if (err) return core.warn("Error while saving inventory", err);
			});
			
			return;
		} else if (szControl == "btnListProperties") {
			if (!skapi.acoSelected) {
				core.info("Select a item.");
				return;
			}

			return;
		} else if (szControl == "btnPaletteInfo") {
			var pID = edPID.getValue();
			core.debug("btnPaletteInfo! " + pID);

			if (!core.colourIds[pID]) {
				core.info("No palette info for " + pID + ".");
				return;
			}
			
			//var RGB = ACColours[pID].getRGB();
			var RGB = core.colourIds[pID].rgb;
			var HSL = getPaletteRSL(pID);
			var Heu = HSL[0];
			var Sat = HSL[1];
			var Lgt = HSL[2];
			core.info("  palette: " + pID + ", RGB: " + RGB + ", Heu: " + round(Heu, 2) + ", Sat: " + round(Sat, 2) + ", Light: " + round(Lgt, 2));
			return;

		} else if (szControl == "btnTest1") {
			if (!skapi.acoSelected) {
				core.info("Select a item.");
				return;
			}
			
			var aco = skapi.acoSelected;
			getItemString({aco: aco}, function(err, stats) {
				core.info("aco1", stats);
			});

			/*
			var aco2 = core.cloneAco({aco:aco});
			aco2.szName = "cloned";
			
			getItemString({aco:aco2}, function(err, stats) {
				core.info("aco2", stats);
			});
			
			core.sayMatchingProfiles({aco:aco});
			
			core.sayMatchingProfiles({aco:aco2});
			*/
			
			return;
		}

		core.debug("<fControlEvent> " + szControl);
	}

	core.sayMatchingProfiles = function sayMatchingProfiles(params) {
		var aco = params.aco;
		
		if (aco.oai || aco._cloned) {
			onAssess();
		} else {
			core.debug("Assessing aco first...");
			core.queueAssessAco({
				aco: aco
			}, onAssess);
		}
		
		var start;
		function onAssess(err) {
			core.getItemString({aco: aco}, function(err, itemString) {
				if (err) return dumpException(err);
				core.info(itemString);
			});
			
			start = new Date().getTime();
			core.getMatchingProfiles({
				aco: aco
			}, onSelectedMatches);
		}
		
		function onSelectedMatches(err, results) {
			if (err) {
				core.warn("error: " + err);
				return;
			}

			var elapsed = new Date().getTime() - start;
			core.console("get matches elapsed: " + elapsed);
			
			results = results.filter(function matched(e) {
				return e && e.match;
			});  // Remove nulls from array.
				
			//core.console("<onMatches>", err, JSON.stringify(results, null, "\t"));
			core.info(core.getFullName(aco) + " matches " + results.length + " profiles.");
			
			var p;
			var tinks;
			for (var i = 0; i < results.length; i++) {
				p = results[i].profile;
				tinks = results[i].neededTinks; //p.getNeededTinks();
				if (tinks && tinks > 0) {
					core.info("  " + p.getName() + " (" + tinks + " tinks)");
				} else {
					core.info("  " + p.getName());
				}
			}
		}
	};

	core.AcoBelongsToMe = function AcoBelongsToMe(acoItem) {
		if (acoItem.acoWearer && acoItem.acoWearer.oid != skapi.acoChar.oid) {
			return false;
		}
		
		if (acoItem.acoContainer) {
			if (acoItem.acoContainer.oty & otyContainer) {
				if (!acoItem.acoContainer.acoContainer || acoItem.acoContainer.acoContainer.oid != skapi.acoChar.oid) {
					return false;
				}
			}
		}
		
		return true;
	};
	

	function GetSlotName(eqm) {
		if (eqm == eqmLBracelet || eqm == eqmRBracelet || eqm == eqmBracelets) return "Bracelets";
		if (eqm == eqmLRing || eqm == eqmRRing || eqm == eqmRings) return "Rings";
		
		for (var szSlot in suitSlots)  {
			if (suitSlots[szSlot] == eqm) {
				return szSlot;
			}
		}
		return eqm;
	}
	
	function compareByName(a, b) {
		if (a.name < b.name) return -1;
		if (a.name > b.name) return 1;
		return 0;
	}
	
	
	var _settings;
	function fLoadSettings() {
		_settings = core.loadJsonFile({path: _dir + "src\\settings.json"}) || {};	
		
		_settings.skills = _settings.skills || {};
		
		_settings.skills[skidArmorTinkering] = _settings.skills[skidArmorTinkering] || 615;
		
		_settings.skills[skidItemTinkering] = _settings.skills[skidItemTinkering] || 571;
		
		_settings.skills[skidMagicItemTinkering] = _settings.skills[skidMagicItemTinkering] || 611;
		
		_settings.skills[skidWeaponTinkering] = _settings.skills[skidWeaponTinkering] || 611;
		
		_settings.tinkChanceThreshold = _settings.tinkChanceThreshold || 100;
		_settings.salvageWorkmanship = _settings.salvageWorkmanship || 10;
		
		if (typeof _settings.chkAssessChests === "undefined") {
			_settings.chkAssessChests = true;
		}
		
		if (typeof _settings.chkLootChests === "undefined") {
			_settings.chkLootChests = false;
		}
		
		if (typeof _settings.chkAssessCorpses === "undefined") {
			_settings.chkAssessCorpses = true;
		}
		
		if (typeof _settings.chkLootCorpses === "undefined") {
			_settings.chkLootChests = true;
		}
		if (typeof _settings.chkAssessTrade === "undefined") {
			_settings.chkAssessTrade = true;
		}
		
		if (typeof _settings.debugging === "undefined") {
			_settings.debugging = false;
		}
	}
	
	
	function fSaveSettings() {
		core.saveJsonFile({
			path: "src\\settings.json",
			data: _settings
		});
		
		core.debug("Saved settings.");
	}

	core.getProfileByName = function getProfileByName(szName) {
		var p;
		var data;
		for (var i = 0; i < _loadedProfiles.length; i++) {
			p = _loadedProfiles[i];
			if (p.getName() == szName) {
				return p;
			}
		}
	};
	
	var _loadedProfiles = [];
	core.loadProfiles = function loadProfiles() {
		/******************************************************************************\
			loadProfiles: Load our profile files into memory.
		\******************************************************************************/
		
		_loadedProfiles = [];
		var path = getProfileDirectory();
		core.debug("path: " + path, core.existsSync(path));
		
		if (core.existsSync(path)) {
			var files = core.readdirSync(getProfileDirectory());
			core.debug("files", files.length);

			var filePath;
			var data;
			var p;
			for (var i = 0; i < files.length; i++) {
				filePath = files[i];
				data = core.loadJsonFile({path: filePath}) || {};
				
				p = new Profile(data, filePath);

				//debug("data: " + data);
				_loadedProfiles.push(p);
				
			}
		}
		
		core.matchingProfiles = {};

		/*
		var p;
		for ( var i=0; i < files.length; i++ ) {
			p = new Profile(files[i])
			_loadedProfiles.push( p );
		}
		
		// Sort the list.
		function compare(a,b) {
			if (a.getName() < b.getName())
				return -1;
			if (a.getName() > b.getName())
				return 1;
			return 0;
		}
		_loadedProfiles.sort(compare);

		core.debug("Profiles loaded...");
		*/
	};
	
	core.getLoadedProfiles = function getLoadedProfiles() {
		return _loadedProfiles;
	};
	
	function fPopulateProfilesList() {
		var scrollPosition = lstProfiles.getScrollPosition();
		lstProfiles.clear();
		
		//loadProfiles();

		var p;
		var data;
		for (var i = 0; i < _loadedProfiles.length; i++) {
			p = _loadedProfiles[i];

			lstProfiles.addRow(p.getEnabled(), (0x06000000 + (p.getIcon() || 0)), p.getName(), (0x06000000 + 4600));
		}

		if (typeof scrollPosition !== "undefined") {
			scrollPosition = Math.min(scrollPosition, _loadedProfiles.length);
			lstProfiles.jumpToPosition(scrollPosition);
		}
		
	}

	var prgChanceThreshold;
	var lstProfiles;
	var edRGB;
	var edPID;
	var btnInvSearch;
	var btnSaveInv;
	var lstDetected;
	var btnJunkSearch;
	var mainView;
	var btnSuitCreator;
	var btnSortInventory;
	var btnCantripCleaner;
	var btnAetheriaCleaner;
	var btnSalvageCleaner;
	var btnPaletteMatcher;
	core.controls = {};
	core.showUI = function showUI(visible) {
		if (typeof visible == "undefined") visible = true;
		
		if (mainView) {
			// Remove old controls and event handlers.
			mainView.removeControls();
		}
		
		mainView = core.controls.mainView = core.createView({
			title : _title, 
			width : viewWidth, 
			height: viewHeight,
			icon  : 7919
		});

		var layout = new SkunkSchema.FixedLayout({parent: mainView});

		var nbTabs = new SkunkSchema.Notebook({
			parent: layout, 
			name  : "nbTabs"
		})
			.setWidth(mainView.getWidth())
			.setHeight(mainView.getHeight() - 20);

		var pageProfiles = new SkunkSchema.Page({
			parent: nbTabs, 
			label : "Profiles"
		});
		
		lstProfiles = core.controls.lstProfiles = new SkunkSchema.List({
			parent: pageProfiles, 
			name  : "lstProfiles"
		})
			.setWidth(pageProfiles.getWidth())
			.setHeight(pageProfiles.getHeight())
			.addColumn({progid: "DecalControls.CheckColumn"})
			.addColumn({progid: "DecalControls.IconColumn"})
			.addColumn({progid: "DecalControls.TextColumn", fixedwidth: nbTabs.getWidth() - 80})
			.addColumn({progid: "DecalControls.IconColumn"})
			.on("onControlEvent", fControlEvent);

		
		// Detected tab
		var pageDetected = new SkunkSchema.Page({
			parent: nbTabs, 
			label : "Detected"
		});

		var flDetected = new SkunkSchema.FixedLayout({parent: pageDetected});

		prgAssessProgress = new SkunkSchema.Progress({
			parent: flDetected, 
			name  : "prgAssessProgress"
		})
			.setWidth(nbTabs.getWidth())
			.setHeight(20);

		lstDetected = core.controls.lstDetected = new SkunkSchema.List({
			parent: flDetected, 
			name  : "lstDetected"
		})
			.setAnchor(prgAssessProgress, "BOTTOMLEFT")
			.setWidth(flDetected.getWidth())
			.setHeight(flDetected.getHeight() - 20)
			.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 0}) // oid
			.addColumn({progid: "DecalControls.IconColumn"})// icon
			.addColumn({progid: "DecalControls.TextColumn", fixedwidth: nbTabs.getWidth() - 20})// name
			.on("onControlEvent", fControlEvent);
	
	
		// Tools tab.
		var pageTools = new SkunkSchema.Page({
			parent: nbTabs, 
			label : "Tools"
		});

		// Tabs within tools.
		var toolTabs = new SkunkSchema.Notebook({
			parent: pageTools, 
			name  : "toolTabs"
		});
		
		/*
		// Palette tab.
		var pagePalette = new SkunkSchema.Page({
			parent:toolTabs, 
			label:"Palette"
		});

		var flPalette = new SkunkSchema.FixedLayout({parent:pagePalette});

		var stListPalettes = new SkunkSchema.StaticText({parent:flPalette, name:"stListPalettes", text:"List selected palettes"});

		var btnListPalettes = new SkunkSchema.PushButton({parent:flPalette, name:"btnListPalettes", text:"Selected"})
		.setAnchor(stListPalettes, "TOPRIGHT")
		.on("onControlEvent", fControlEvent);
		
		// Colour variance button
		var stRGB = new SkunkSchema.StaticText({parent:flPalette, name:"stRGB", text:"RGB Hex:"})
		.setAnchor(stListPalettes, "BOTTOMLEFT")
		.setWidth(50);

		edRGB = core.controls.edRGB = new SkunkSchema.Edit({parent:flPalette, name:"edRGB", text:"FFFFFF"})
		.setAnchor(stRGB, "TOPRIGHT")
		.setWidth(50);

		var btnRGBVariance = new SkunkSchema.PushButton({parent:flPalette, name:"btnRGBVariance", text:"Show variance"})
		.setAnchor(edRGB, "TOPRIGHT")
		.on("onControlEvent", fControlEvent);

		var stPaletteID = new SkunkSchema.StaticText({parent:flPalette, name:"stPaletteID", text:"Palette ID"})
		.setAnchor(stRGB, "BOTTOMLEFT")
		.setWidth(50);

		edPID = core.controls.edPID = new SkunkSchema.Edit({parent:flPalette, name:"edPID", text:"0"})
		.setAnchor(stPaletteID, "TOPRIGHT")
		.setWidth(50);

		var btnPaletteInfo = new SkunkSchema.PushButton({parent:flPalette, name:"btnPaletteInfo", text:"Palette Info"})
		.setAnchor(edPID, "TOPRIGHT")
		.on("onControlEvent", fControlEvent);

		var stPurgePalettes = new SkunkSchema.StaticText({parent:flPalette, name:"stPurgePalettes", text:"Purge unneeded oids"})
		.setAnchor(stPaletteID, "BOTTOMLEFT");
		
		var btnPurgePalettes = new SkunkSchema.PushButton({parent:flPalette, name:"btnPurgePalettes", text:"Purge"})
		.setAnchor(stPurgePalettes, "TOPRIGHT")
		.on("onControlEvent", fControlEvent);
	*/
		
		// Inv tab
		var flInv = new SkunkSchema.FixedLayout({
			parent: new SkunkSchema.Page({
				parent: toolTabs, 
				label : "Inv"
			})
		});
	
		btnSaveInv = core.controls.btnSaveInv = new SkunkSchema.PushButton({
			parent: flInv, 
			name  : "btnSaveInv", 
			text  : "Save inventory"
		})
			.on("onControlEvent", fControlEvent);

		btnInvSearch = core.controls.btnInvSearch = new SkunkSchema.PushButton({
			parent: flInv, 
			name  : "btnInvSearch", 
			text  : "Inventory Search"
		})
			.setAnchor(btnSaveInv, "BOTTOMLEFT")
			.on("onControlEvent", function() {
				btnInvSearch.setText("...");
				core.launchInventorySearch({logger: core.console}, function(err, results) {
					btnInvSearch.setText("Inventory Search");
					if (err) return dumpException(err);
				});
			});
	
		/*

		btnInvSearch = new LibSchema.PushButton(flInv, "btnInvSearch", "Profile search");
		btnInvSearch.setControlEvent(fControlEvent);
		btnInvSearch.setAnchor(btnSaveInv, "BOTTOMLEFT");

		btnJunkSearch = new LibSchema.PushButton(flInv, "btnJunkSearch", "Junk search");
		btnJunkSearch.setControlEvent(fControlEvent);
		btnJunkSearch.setAnchor(btnInvSearch, "BOTTOMLEFT");

		btnSuitCreator = new LibSchema.PushButton(flInv, "btnSuitCreator", "Suit Creator");
		btnSuitCreator.setControlEvent(fControlEvent);
		btnSuitCreator.setAnchor(btnJunkSearch, "BOTTOMLEFT");
		
		btnSortInventory = new LibSchema.PushButton(flInv, "btnSortInventory", "Sort Inventory");
		btnSortInventory.setControlEvent(fControlEvent);
		btnSortInventory.setAnchor(btnSuitCreator, "BOTTOMLEFT");
		
		btnCantripCleaner = new LibSchema.PushButton(flInv, "btnCantripCleaner", "Cantrip Cleaner");
		btnCantripCleaner.setControlEvent(fControlEvent);
		btnCantripCleaner.setAnchor(btnSortInventory, "BOTTOMLEFT");
		
		btnAetheriaCleaner = new LibSchema.PushButton(flInv, "btnAetheriaCleaner", "Aetheria Cleaner");
		btnAetheriaCleaner.setControlEvent(fControlEvent);
		btnAetheriaCleaner.setAnchor(btnCantripCleaner, "BOTTOMLEFT");
		
		btnSalvageCleaner = new LibSchema.PushButton(flInv, "btnSalvageCleaner", "Salvage Cleaner");
		btnSalvageCleaner.setControlEvent(fControlEvent);
		btnSalvageCleaner.setAnchor(btnAetheriaCleaner, "BOTTOMLEFT");
		
		btnPaletteMatcher = new LibSchema.PushButton(flInv, "btnPaletteMatcher", "Palette Matcher");
		btnPaletteMatcher.setControlEvent(fControlEvent);
		btnPaletteMatcher.setAnchor(btnSalvageCleaner, "BOTTOMLEFT");
		*/

		// Misc tools
		var flMT = new SkunkSchema.FixedLayout({
			parent: new SkunkSchema.Page({
				parent: toolTabs, 
				label : "Misc"
			})
		});

		var btnProperties = core.controls.btnProperties = new SkunkSchema.PushButton({
			parent: flMT, 
			name  : "btnProperties", 
			text  : "Properties"
		})
			.on("onControlEvent", fControlEvent);

		/*
		var btnTest = core.controls.btnTest = new SkunkSchema.PushButton({
			parent: flMT, 
			name: "btnTest", 
			text: "Test"
		})
		.setAnchor(btnProperties, "BOTTOMLEFT")
		.on("onControlEvent", fControlEvent);
	*/
		
		var btnClearConsole = core.controls.btnClearConsole = new SkunkSchema.PushButton({
			parent: flMT, 
			name  : "btnClearConsole", 
			text  : "Clear Console"
		})
			.setAnchor(btnProperties, "BOTTOMLEFT")
			.on("onControlEvent", fControlEvent);	

		// Config tab
		var pageConfig = new SkunkSchema.Page({
			parent: nbTabs, 
			label : "Config"
		});

		var configTabs = new SkunkSchema.Notebook({
			parent: pageConfig, 
			name  : "configTabs"
		});

		var pageConfigMain = new SkunkSchema.Page({
			parent: configTabs, 
			label : "Main"
		});

		var flConfigMain = new SkunkSchema.FixedLayout({parent: pageConfigMain});

		var chkAssessChests = core.controls.chkAssessChests = new SkunkSchema.Checkbox({
			parent : flConfigMain, 
			name   : "chkAssessChests", 
			text   : "Assess Chests",
			checked: _settings.chkAssessChests
		})
			.on("onControlEvent", fControlEvent);

		var chkLootChests = core.controls.chkLootChests = new SkunkSchema.Checkbox({
			parent : flConfigMain, 
			name   : "chkLootChests", 
			text   : "Loot Chests",
			checked: _settings.chkLootChests
		})
			.setAnchor(chkAssessChests, "BOTTOMLEFT", "TOPLEFT")
			.on("onControlEvent", fControlEvent);

		var chkAssessCorpses = core.controls.chkAssessCorpses = new SkunkSchema.Checkbox({
			parent : flConfigMain, 
			name   : "chkAssessCorpses", 
			text   : "Assess Corpses",
			checked: _settings.chkAssessCorpses
		})
			.setAnchor(chkLootChests, "BOTTOMLEFT", "TOPLEFT")
			.on("onControlEvent", fControlEvent);

		var chkLootCorpses = core.controls.chkLootCorpses = new SkunkSchema.Checkbox({
			parent : flConfigMain, 
			name   : "chkLootCorpses", 
			text   : "Loot Corpses",
			checked: _settings.chkLootCorpses
		})
			.setAnchor(chkAssessCorpses, "BOTTOMLEFT", "TOPLEFT")
			.on("onControlEvent", fControlEvent);

		var chkAssessTrade = core.controls.chkAssessTrade = new SkunkSchema.Checkbox({
			parent : flConfigMain, 
			name   : "chkAssessTrade", 
			text   : "Assess Trade Window",
			checked: _settings.chkAssessTrade
		})
			.setAnchor(chkLootCorpses, "BOTTOMLEFT", "TOPLEFT")
			.on("onControlEvent", fControlEvent);

		var chkDebugging = core.controls.chkDebugging = new SkunkSchema.Checkbox({
			parent : flConfigMain, 
			name   : "chkDebugging", 
			text   : "Debugging",
			checked: _settings.debugging
		})
			.setAnchor(chkAssessTrade, "BOTTOMLEFT", "TOPLEFT")
			.on("onControlEvent", fControlEvent);

		//Tink page.
		var flTinkering = new SkunkSchema.FixedLayout({
			parent: new SkunkSchema.Page({
				parent: configTabs, 
				label : "Tink"
			})
		});

		var stArmorTinkering = new SkunkSchema.StaticText({parent: flTinkering, name: "stArmorTinkering", text: "Armor Tinkering:"});

		var edArmorTinkering = core.controls.edArmorTinkering = new SkunkSchema.Edit({parent: flTinkering, name: "edArmorTinkering", text: _settings.skills[skidArmorTinkering]})
			.setAnchor(stArmorTinkering, "TOPRIGHT")
			.on("onControlEvent", fControlEvent);

		var stItemTinkering = new SkunkSchema.StaticText({parent: flTinkering, name: "stItemTinkering", text: "Item Tinkering:"})
			.setAnchor(stArmorTinkering, "BOTTOMLEFT");

		var edItemTinkering = core.controls.edItemTinkering = new SkunkSchema.Edit({parent: flTinkering, name: "edItemTinkering", text: _settings.skills[skidItemTinkering]})
			.setAnchor(stItemTinkering, "TOPRIGHT")
			.on("onControlEvent", fControlEvent);

		var stMagicItemTinkering = new SkunkSchema.StaticText({parent: flTinkering, name: "stMagicItemTinkering", text: "Magic Item Tinkering:"})
			.setAnchor(stItemTinkering, "BOTTOMLEFT");

		var edMagicItemTinkering = core.controls.edMagicItemTinkering = new SkunkSchema.Edit({parent: flTinkering, name: "edMagicItemTinkering", text: _settings.skills[skidMagicItemTinkering]})
			.setAnchor(stMagicItemTinkering, "TOPRIGHT")
			.on("onControlEvent", fControlEvent);

		var stWeaponTinkering = new SkunkSchema.StaticText({parent: flTinkering, name: "stWeaponTinkering", text: "Weapon Tinkering:"})
			.setAnchor(stMagicItemTinkering, "BOTTOMLEFT");

		var edWeaponTinkering = core.controls.edWeaponTinkering = new SkunkSchema.Edit({parent: flTinkering, name: "edWeaponTinkering", text: _settings.skills[skidWeaponTinkering]})
			.setAnchor(stWeaponTinkering, "TOPRIGHT")
			.on("onControlEvent", fControlEvent);

		var stChanceThreshold = new SkunkSchema.StaticText({parent: flTinkering, name: "stChanceThreshold", text: "Chance Threshold"})
			.setAnchor(stWeaponTinkering, "BOTTOMLEFT");

		prgChanceThreshold = core.controls.prgChanceThreshold = new SkunkSchema.Progress({
			parent: flTinkering, 
			name  : "prgChanceThreshold"
		})
			.setAnchor(stChanceThreshold, "TOPRIGHT")
			.setValue(_settings.tinkChanceThreshold);
	
		
		var sldChanceThreshold = core.controls.sldChanceThreshold = new SkunkSchema.Slider({
			parent: flTinkering, 
			name  : "sldChanceThreshold"
		})
			.setAnchor(stChanceThreshold, "TOPRIGHT")
			.setOffsetY(-5)
			.setValue(_settings.tinkChanceThreshold)
			.on("onControlEvent", fControlEvent);

		var stSalvageWorkmanship = new SkunkSchema.StaticText({parent: flTinkering, name: "stSalvageWorkmanship", text: "Salv Workmanship"})
			.setAnchor(stChanceThreshold, "BOTTOMLEFT");

		var edSalvageWorkmanship = core.controls.edSalvageWorkmanship = new SkunkSchema.Edit({parent: flTinkering, name: "edSalvageWorkmanship", text: _settings.salvageWorkmanship})
			.setAnchor(stSalvageWorkmanship, "TOPRIGHT")
			.on("onControlEvent", fControlEvent);

		var flDebug = new SkunkSchema.FixedLayout({
			parent: new SkunkSchema.Page({
				parent: configTabs, 
				label : "Debug"
			})
		});

		var btnTest1 = core.controls.btnTest1 = new SkunkSchema.PushButton({
			parent: flDebug, 
			name  : "btnTest1", 
			text  : "Test1"
		})
			.on("onControlEvent", function() {
				var matches = core.getMatchingProfilesSync({aco: skapi.acoSelected, verbose: core.info});
				core.console("matches: " + JSON.stringify(matches, undefined, "\t"));
			});	

		// Footer
		var btnNew = core.controls.btnNew = new SkunkSchema.PushButton({
			parent: layout, 
			name  : "btnNew", 
			text  : "New"
		})
			.setAnchor(layout, "BOTTOMLEFT", "BOTTOMLEFT")
			.setWidth(30)
			.on("onControlEvent", fControlEvent);

		var btnSelected = core.controls.btnSelected = new SkunkSchema.PushButton({
			parent: layout, 
			name  : "btnSelected", 
			text  : "Selected"
		})
			.setAnchor(btnNew, "TOPRIGHT")
			.setWidth(40)
			.on("onControlEvent", fControlEvent);

		var btnQuit = core.controls.btnQuit = new SkunkSchema.PushButton({
			parent: layout, 
			name  : "btnQuit", 
			text  : "Quit"
		})
			.setAnchor(layout, "BOTTOMRIGHT", "BOTTOMRIGHT")
			.setWidth(40)
			.on("onControlEvent", fControlEvent);

		mainView.showControls(visible);

		fPopulateProfilesList();
	};
	
	var openedContainer;

	core.openedContainer = null;
	core.OnOpenContainer = function OnOpenContainer(acoContainer) {
		core.debug("<OnOpenContainer>", acoContainer);
	};
	
	core.OnOpenContainerPanel = function OnOpenContainerPanel(acoContainer) {
		core.debug("<OnOpenContainerPanel>", acoContainer);
		core.openedContainer = acoContainer;

		if (acoContainer.oty & otyCorpse) {
			core.info("parent is otyCorpse");
			if (_settings.chkAssessCorpses == true) {
				core.scanContainer({
					acoContainer : acoContainer, 
					monitorClosed: true
				}, onScan);
			}
		} else if (acoContainer.oty & (otyContainer | otyDirectivesCache)) {
			if (_settings.chkAssessChests == true) {
				core.scanContainer({
					acoContainer : acoContainer, 
					monitorClosed: true
				}, onScan);
			}
		}

		function onScan(err, results) {
			if (err) {
				if (err.message == "CONTAINER_CLOSED") {
					//lstDetected.clear();
					prgAssessProgress.setValue(100); 
					core.info("Canceled scan due to container closed.");
				} else if (err.message == "CANCELLATION") {
					return;
				} else {
					dumpException(err);
				}
				return;
			}
			
			core.info("Done scanning " + acoContainer.szName + ".");

			if (acoContainer.oty & otyCorpse) {
				if (_settings.chkLootCorpses == true) {
					core.info("Looting " + results.length + " items...");
					core.lootItems({items: results}, onDoneLooting);
				}
			} else if (acoContainer.oty & (otyContainer | otyDirectivesCache)) {
				if (_settings.chkLootChests == true) {
					core.info("Looting " + results.length + " items...");
					core.lootItems({items: results}, onDoneLooting);
				}
			}
			
		}
		
		function onDoneLooting(err, results) {
			if (err) core.warn("Error looting", err);
			core.info("Done looting.");
		}
	};

	core.lootItems = function lootItems(params, callback) {
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		var items = params.items;
		
		return async_js.everySeries(items, function each(item, callback) {
			core.lootItem({aco: item.aco}, function onLoot(err, results) {
				if (err && err.message == "TOO_BUSY") return core.lootItem({aco: item.aco}, onLoot);
				callback(err, results);
			});
		}, callback);
	};

	core.OnCloseContainer = function OnCloseContainer(acoContainer) {
		core.debug("<OnCloseContainer> " + acoContainer + " (" + (typeof acoContainer) + ")");
		core.openedContainer = null;
		
		fRemoveDetectedAcoContainer(acoContainer);
	};

	/*
	core.OnObjectDestroy = function OnObjectDestroy(aco) {
		//inform("<OnObjectDestroy> " + aco );
		
		fRemoveDetectedOid(aco);
		
		if (data.palettes[aco.oid] !== "undefined") {
			delete data.palettes[aco.oid];
		}
	};
	*/
	
	//data.palettes[aco.oid]
	
	core.printDebugStats = function printDebugStats() {
		var numPalettes = 0;
		for (var oid in data.palettes)  {
			if (!data.palettes.hasOwnProperty(oid)) continue;
			numPalettes += 1;
		}
		core.console("We have " + numPalettes + " palettes in memory.");
	};
	
	var assessQueue = [];
	
	var otyDirectivesCache = 0x00000014;	// Doesn't show up as a regular Container chest.
	core.OnObjectCreate = function OnObjectCreate(aco) {
		var acoParentContainer = aco.acoContainer;
		if (acoParentContainer && acoParentContainer.acoContainer) {
			acoParentContainer = acoParentContainer.acoContainer;
		}

		
		// TODO ground items.
	};
	
	var lastTradeContainer;
	core.tradeWindow = {};
	var tradeAssessRemaining = [];
	core.OnTradeAdd = function OnTradeAdd(aco, side) {
		core.tradeWindow[side] = core.tradeWindow[side] || [];
		core.tradeWindow[side].push(aco);
		
		var parentContainer = getParentContainer(aco);
		
		
		if (side != 2) {
			// 2 is their side.
			return;
		}

		if (_settings.chkAssessTrade != true) return;
		
		//core.info("<OnTradeAdd> " + aco + ", " + side + ", parentContainer: " + parentContainer);
		var start = new Date().getTime();
		
		tradeAssessRemaining.push(aco);
		
		if (aco.oai) {
			onAssess();
		} else {
			//core.console("Assessing " + aco.szName + " first...");
			core.queueAssessAco({
				aco: aco
			}, onAssess);
		}

		function onAssess(err) {
			if (err) return err;

			//core.console("Getting " + aco.szName + "'s matching profiles...");
			core.getMatchingProfiles({
				aco  : aco,
				cache: true
			}, onMatches);
		}

		function onMatches(err, results) {
			if (err) return dumpException(err, "OnTradeAdd|onMatches");

			//var aco = results.aco;
			var elapsed = new Date().getTime() - start;
			core.debug("<OnTradeAdd|onMatches>", aco.szName, aco.oid, "profiles: " + results.length);

			if (!core.tradeWindow[side]) return; // window closed/reset.
			
			var index = tradeAssessRemaining.indexOf(aco);
			if (index >= 0) {
				tradeAssessRemaining.splice(index, 1);
			}
				
			if (results.length > 0) {

				var p;
				var tinks;
				for (var i = 0; i < results.length; i++) {
					p = results[i].profile;
					tinks = results[i].neededTinks; //p.getNeededTinks();
					if (tinks && tinks > 0) {
						core.info(" " + core.getFullName(aco) + " matches " + p.getName() + " (" + tinks + " tinks).");
					} else {
						core.info(" " + core.getFullName(aco) + " matches " + p.getName()  + ".");
					}
				}

				lstDetected.addRow(aco.oid, (0x06000000 + aco.icon), core.getFullName(aco));
			}
			
			var total = core.tradeWindow[side].length;
			var remaining = tradeAssessRemaining.length;
			
			var progress = round((remaining / total)  * 100);
			core.debug("elapsed: " + elapsed + ", remaining: " + remaining + ", total: " + total + ", progress: " + progress);
			
			prgAssessProgress.setValue(100 - progress); 
		}

	};

	core.OnTradeEnd = function OnTradeEnd(arc) {
		core.debug("<OnTradeEnd> " + lastTradeContainer);
		core.tradeWindow = {};

		//lstDetected.clear();
		prgAssessProgress.setValue(100); 
		tradeAssessRemaining = [];
	};
	
	core.OnTradeReset = function OnTradeReset(acoContainer) {
		core.info("<OnTradeReset> " + acoContainer + " " + (acoContainer && acoContainer.oid));
		core.tradeWindow = {};
		lstDetected.clear();
		prgAssessProgress.setValue(100); 
		tradeAssessRemaining = [];
	};
	
	core.OnEnd3D = function OnEnd3D() {
		core.console("<OnEnd3D>");

	//	end();
	};
	
	core.OnStart3D = function OnStart3D() {
		core.console("<OnStart3D>");
		core.addHandler(evidOnLogon); // Show UI on login
			
		//showUI();
	};
	
	core.OnLogon = function OnLogon(acoChar) {
		core.console("<OnLogon>");
		core.removeHandler(evidOnLogon);
		showUI();
		registerContainerEvents();
	};
	
	core.OnAddToInventory = function OnAddToInventory(aco) {
		core.debug("<OnAddToInventory> " + aco + ", AcoBelongsToMe: " + AcoBelongsToMe(aco));
		if (AcoBelongsToMe(aco) && typeof data.palettes[aco.oid] !== "undefined") {
			//core.setPalettes({oid:aco.oid, palettes:data.palettes[aco.oid]});
			// TODO: fix this
		}
	};
	
	var eventsRegistered = false;
	core.registerContainerEvents = function registerContainerEvents() {
		if (eventsRegistered == false) {
			core.addHandler(evidOnOpenContainer);
			core.addHandler(evidOnOpenContainerPanel);
			core.addHandler(evidOnCloseContainer);
			core.addHandler(evidOnObjectCreate);
			core.addHandler(evidOnTradeAdd);
			core.addHandler(evidOnTradeEnd);
			core.addHandler(evidOnTradeReset);
			eventsRegistered = true;
		}
	};

	//////////////////
	// Properties   //
	//////////////////
	/*
	var _operators = [
		{xml:"&lt;",o:"=="},
		"!=",
		">",
		">=",
		"<",
		"<=",
		"&",
		"!&"
	];
	*/
		
	// Available operators we can use.
	
	var _operators = [
		"==",
		"!=",
		">",
		">=",
		"<",
		"<=",
		"&",
		"!&",
		"match",
		"!match",
		"RGBvariance",
		"!Suggestion", // missing suggestion
		"Exists"
	];


	var _properties = {};

	var Property;
	(function() {
		Property = core.Property = function(data, assessment)  {
			this.data = data || {};
			this.assessment = (assessment === undefined ? false : assessment);

			//this.operatorMaterials = {};
		};
		
		Property.prototype.getID = function fGetID() {
			return this.data.id;
		};
		
		Property.prototype.getName = function fGetName() {
			return this.data.name;
		};

		Property.prototype.tinksToValue = function (aco, operator, value) {
			if (this.valueExists(aco) && this.operatorMaterials && this.operatorMaterials[operator]) {
				var material = this.operatorMaterials[operator];
				
				if (material == materialSteel) {
					if (aco.szName.match(/Covenant/)) {
						core.debug("We can't apply steel to " + aco.szName);
						return;
					} else if (aco.szName.match(/Olthoi/) && !(aco.szName.match(/Olthoi Alduressa/) || aco.szName.match(/Olthoi Amuli/) || aco.szName.match(/Olthoi Celdon/) || aco.szName.match(/Olthoi Koujia/))) {
						core.debug("We can't apply steel to " + aco.szName);
						return;
					}
				}

				
				var materialUtil = TinkeringUtil.getMaterial(material);
				if (materialUtil) {
					var acoValue = materialUtil.getAcoValue(aco);

					//	core.debug("acoValue: " + acoValue);
					var tinks = materialUtil.getNeededTinks(acoValue, value);

					//	core.debug("tinks: " + tinks);
					return tinks;
				}
			}
		};
		
		Property.prototype.getTinkeringSkid = function () {
			if (this.operatorMaterials) {
				var material;
				for (var op in this.operatorMaterials)  {
					if (!this.operatorMaterials.hasOwnProperty(op)) continue;
					material = this.operatorMaterials[op];
					break;
				}

				//	core.debug("<getTinkeringSkid> material:" + material);
				var materialUtil = TinkeringUtil.getMaterial(material);
				if (materialUtil) {
					return materialUtil.skid;
				}
			}
		};
		
		Property.prototype.tinksToValueAboveChance = function (aco, operator, value, chanceThreshold, startingAttempt) {
			// Return the number of tinks needed to hit a 'value' but only if they're above our chance threshold. If chance goes below we return nothing.
			
			if (this.valueExists(aco) && this.operatorMaterials && this.operatorMaterials[operator]) {
				var material = this.operatorMaterials[operator];
				
				if ((aco.szName.match(/Covenant/) || aco.szName.match(/Olthoi/)) && material == materialSteel) {
					core.debug("We can't apply steel to cov armor.");
					return;
				}
				
				var workmanship = core.getWorkmanshipSync({aco: aco});
				core.debug("workmanship: " + workmanship);
				if (!workmanship) return;
				
				var materialUtil = TinkeringUtil.getMaterial(material);
				if (materialUtil) {
					var acoValue = materialUtil.getAcoValue(aco);
					var tinks = materialUtil.getNeededTinks(acoValue, value);
					core.debug("materialUtil.getNeededTinks: " + tinks);
					
					if (tinks <= 0) {
						return {neededTinks: tinks, achievable: false};
					} else if (tinks > 10) {
						return {neededTinks: tinks, achievable: false};
					}
					var skillCur = _settings.skills[materialUtil.skid];
					
					//startingAttempt = startingAttempt || aco.oai.ibi.ctink;
					
					var attempt = startingAttempt + 1;
					var skillNeeded;
					
					for (var i = 0; i < tinks; i++) {
						skillNeeded = materialUtil.getThisSkillNeeded({
							itemWorkmanship   : workmanship,
							salvageWorkmanship: _settings.salvageWorkmanship,
							attempt           : attempt
						});
					
						core.debug("workmanship: " + workmanship, "salvageWorkmanship: " + _settings.salvageWorkmanship, "skillCur: " + skillCur, "attempt: " + attempt);

						//chance = round(materialUtil.getThisTinkChance(workmanship, _settings.salvageWorkmanship, skillCur, attempt)); // Client rounds chance, we should too.
						chance = round(materialUtil.getThisTinkChance({
							itemWorkmanship   : workmanship,
							salvageWorkmanship: _settings.salvageWorkmanship,
							skillCurrent      : skillCur,
							attempt           : attempt
						}));

						
						core.debug("i: " + i + ", attempt: " + attempt + ", skillNeeded: " + skillNeeded + ", chance: " + chance);
						
						if (chance < chanceThreshold) {
							core.debug("attempt: " + attempt + ", skillNeeded: " + skillNeeded + ", chance: " + chance + " (below threshold)");
							return {neededTinks: tinks, achievable: false};
						}
						
						attempt ++;
					}

					//return {count:tinks, attempt:attempt};
					return {neededTinks: tinks, attempt: attempt - 1, achievable: true};
				}
			}
		};
		
		Property.prototype.getSuggestions = function() {
			return this.data.suggestions;
		};
		
		Property.prototype.getSzSuggestion = function(value) {
			var suggestions = this.data.suggestions;
			for (var szName in suggestions) {
				if (!suggestions.hasOwnProperty(szName)) continue;
				if (value == suggestions[szName]) {
					return szName;
				}
			}
		};
		
		Property.prototype.valueExists = function(aco) {
			//return (typeof this.getValue === "function" && typeof );
			if (typeof this.getValue === "function") {
				return this.getValue(aco) !== "undefined";
			} else if (typeof this.getValues === "function") {
				return this.getValues(aco) !== "undefined";
			}
		};

		/* 
		// Placeholder getValueAsync
		Property.prototype.getValueAsync = function getValueAsync(aco, callback) {
			//var value = this.getValue(aco);
			//callback(null, value);
			if (typeof this.getValue === "function") {
				//return callback(null, this.getValue(aco));
				return core.setImmediate(callback, undefined, this.getValue(aco));
			} else if (typeof this.getValues === "function") {
				//return callback(null, this.getValues(aco));
				return core.setImmediate(callback, undefined, this.getValues(aco));
			}
		};
		

		// Placeholder valueExistsAsync
		Property.prototype.valueExistsAsync = function valueExistsAsync(aco, callback) {
			//var value = this.valueExists(aco);
			//callback(null, value);
			if (typeof this.getValueAsync === "function") {
				return this.getValueAsync(aco, function onValue(err, results) {
					//core.debug("onValue: " + results);
					callback(err, (typeof results !== "undefined"));
				});
			} else if (typeof this.getValue === "function") {
				var results = this.getValue(aco);
				return core.setImmediate(callback, undefined, (typeof results !== "undefined"));
			}
			return core.setImmediate(callback, undefined, false);
		};
		*/
			
	})();

	(function() {
		var id = "al";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Armor Level"
		});
		property.assessment = true;
	
		property.operatorMaterials = {};
		property.operatorMaterials[">="]    = materialSteel;
		property.operatorMaterials[">"]     = materialSteel;
		
		property.getValue = function getValue(aco) {
			if (typeof aco["oai.iai.al"] !== "undefined") { // Flat object.
				return aco["oai.iai.al"];
			} else if (aco.oai && aco.oai.iai) {
				return aco.oai.iai.al;
			}
		};
	})();

	(function() {
		// Armor level with minor/major/epic/legendary boosts.
		var id = "alCantrips";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Armor Level (Cantrips)"
		});
		property.assessment = true;
		
		property.operatorMaterials = {};
		property.operatorMaterials[">="]    = materialSteel;
		property.operatorMaterials[">"]     = materialSteel;

		property.getValue = function getValue(aco) {
			var al;
			if (typeof aco["oai.iai.al"] !== "undefined") {
				al = aco["oai.iai.al"];
			} else {
				al = aco.oai && aco.oai.iai && aco.oai.iai.al;
			}

			var spells = core.getAcoSpells({aco: aco});
			var spellid, szName;
			for (var i = 0; i < spells.length; i++) {
				spellid = spells[i];
				szName = getSpellName(spellid);
				if (szName.match(/Minor Impenetrability/)) {
					al += 20;
					continue;
				} else if (szName.match(/Major Impenetrability/)) {
					al += 40;
					continue;
				} else if (szName.match(/Epic Impenetrability/)) {
					al += 60;
					continue;
				} else if (szName.match(/Legendary Impenetrability/)) {
					al += 80;
					continue;
				}
			}

			return al;
		};
	})();

	function getBaneLevelModifer(szLevel) {
		if (szLevel == "I") {
			return 0.1;
		} else if (szLevel == "II") {
			return 0.25;
		} else if (szLevel == "III") {
			return 0.5;
		} else if (szLevel == "IV") {
			return 0.75;
		} else if (szLevel == "V") {
			return 1;
		} else if (szLevel == "VI") {
			return 1.5;
		} else if (szLevel == "Minor") {
			return 0.1;
		} else if (szLevel == "Major") {
			return 0.15;
		} else if (szLevel == "Epic") {
			return 0.2;
		} else if (szLevel == "Legendary") {
			return 0.25;
		}
	}
	
	core.getBuffedAL = function getBuffedAL(aco) {
		/******************************************************************************\
			getBuffedAL: Return the buffed AL factoring in inherent banes. Only really useful for covenant armor.
		\******************************************************************************/
		
		//if (!aco || !aco.oai || !aco.oai.iai) return;
		
		var al                  = aco["oai.iai.al"] || aco.oai && aco.oai.iai && aco.oai.iai.al;
		var protAcid            = aco["oai.iai.protAcid"] || aco.oai && aco.oai.iai && aco.oai.iai.protAcid;
		var protBludgeoning	    = aco["oai.iai.protBludgeoning"] || aco.oai && aco.oai.iai && aco.oai.iai.protBludgeoning;
		var protCold            = aco["oai.iai.protCold"] || aco.oai && aco.oai.iai && aco.oai.iai.protCold;
		var protElectrical      = aco["oai.iai.protElectrical"] || aco.oai && aco.oai.iai && aco.oai.iai.protElectrical;
		var protFire            = aco["oai.iai.protFire"] || aco.oai && aco.oai.iai && aco.oai.iai.protFire;
		var protPiercing        = aco["oai.iai.protPiercing"] || aco.oai && aco.oai.iai && aco.oai.iai.protPiercing;
		var protSlashing        = aco["oai.iai.protSlashing"] || aco.oai && aco.oai.iai && aco.oai.iai.protSlashing;	

		var spells = core.getAcoSpells({aco: aco});
		var spellid, spell, szElement, szLevel, mod;
		for (var i = 0; i < spells.length; i++) {
			spellid = spells[i];
			spell = skapi.SpellInfoFromSpellid(spellid);
			if (!spell || spell.skid != skidItemEnchantment) continue;
			
			// Check for 1-7 banes.
			if (spell.szName.match(/(.*) Bane (.*)/)) {
				szElement = RegExp.$1;
				szLevel = RegExp.$2;
				mod = getBaneLevelModifer(szLevel);

				//	core.debug("1 szElement: " + szElement + ", szLevel: " + szLevel + ", mod: " + mod);
				if (mod) {
					if (szElement == "Acid") {
						protAcid += mod;
						continue;
					} else if (szElement == "Bludgeon") {
						protBludgeoning += mod;
						continue;
					} else if (szElement == "Frost") {
						protCold += mod;
						continue;
					} else if (szElement == "Lightning") {
						protElectrical += mod;
						continue;
					} else if (szElement == "Flame") {
						protFire += mod;
						continue;
					} else if (szElement == "Flame") {
						protFire += mod;
						continue;
					} else if (szElement == "Piercing") {
						protPiercing += mod;
						continue;
					} else if (szElement == "Blade") {
						protSlashing += mod;
						continue;
					}
				}
			}

			// Check for level 7 banes.
			else if (spell.szName.match(/Olthoi's Bane/)) {
				protAcid += 1.7;
				continue;
			} else if (spell.szName.match(/Tusker's Bane/)) {
				protBludgeoning += 1.7;
				continue;
			} else if (spell.szName.match(/Gelidite's Bane/)) {
				protCold += 1.7;
				continue;
			} else if (spell.szName.match(/Astyrrian's Bane/)) {
				protCold += 1.7;
				continue;
			} else if (spell.szName.match(/Inferno's Bane/)) {
				protFire += 1.7;
				continue;
			} else if (spell.szName.match(/Archer's Bane/)) {
				protPiercing += 1.7;
				continue;
			} else if (spell.szName.match(/Swordsman's Bane/)) {
				protSlashing += 1.7;
				continue;
			}

			// Check for level 8. 
			else if (spell.szName.match(/Incantation of Acid Bane/)) {
				protAcid += 2;
				continue;
			} else if (spell.szName.match(/Incantation of Bludgeon Bane/)) {
				protBludgeoning += 2;
				continue;
			} else if (spell.szName.match(/Incantation of Frost Bane/)) {
				protCold += 2;
				continue;
			} else if (spell.szName.match(/Incantation of Lightning Bane/)) {
				protCold += 2;
				continue;
			} else if (spell.szName.match(/Incantation of Flame Bane/)) {
				protFire += 2;
				continue;
			} else if (spell.szName.match(/Incantation of Piercing Bane/)) {
				protPiercing += 2;
				continue;
			} else if (spell.szName.match(/Incantation of Blade Bane/)) {
				protSlashing += 2;
				continue;
			}

			// Check for AL buffs
			else if (spell.szName.match(/^Impenetrability (.*)/)) {
				szLevel = RegExp.$1;
				if (szLevel == "I") {
					al += 20;
				} else if (szLevel == "II") {
					al += 50;
				} else if (szLevel == "III") {
					al += 75;
				} else if (szLevel == "IV") {
					al += 100;
				} else if (szLevel == "V") {
					al += 150;
				} else if (szLevel == "VI") {
					al += 200;
				}
				continue;
			} else if (spell.szName.match(/Brogard's Defiance/)) {
				al += 220;
				continue;
			} else if (spell.szName.match(/Incantation of Impenetrability/)) {
				al += 240;
				continue;
			} else if (spell.szName.match(/(Minor|Major|Epic|Legendary) (.*) Bane/)) {
				szLevel = RegExp.$1;
				szElement = RegExp.$2;
				mod = getBaneLevelModifer(szLevel);

				//	core.debug("2 szElement: " + szElement + ", szLevel: " + szLevel + ", mod: " + mod);
				if (mod) {
					if (szElement == "Acid") {
						protAcid += mod;
						continue;
					} else if (szElement == "Bludgeon") {
						protBludgeoning += mod;
						continue;
					} else if (szElement == "Frost") {
						protCold += mod;
						continue;
					} else if (szElement == "Lightning") {
						protElectrical += mod;
						continue;
					} else if (szElement == "Flame") {
						protFire += mod;
						continue;
					} else if (szElement == "Flame") {
						protFire += mod;
						continue;
					} else if (szElement == "Piercing") {
						protPiercing += mod;
						continue;
					} else if (szElement == "Blade") {
						protSlashing += mod;
						continue;
					}
				}
			} else if (spell.szName.match(/Minor Impenetrability/)) {
				al += 20;
				continue;
			} else if (spell.szName.match(/Major Impenetrability/)) {
				al += 40;
				continue;
			} else if (spell.szName.match(/Epic Impenetrability/)) {
				al += 60;
				continue;
			} else if (spell.szName.match(/Legendary Impenetrability/)) {
				al += 80;
				continue;
			}

			core.debug("Missing spell type, i: " + i + ", spell: " + spell.szName);
		}

		protAcid            = Math.min(protAcid, 2);
		protBludgeoning     = Math.min(protBludgeoning, 2);
		protCold            = Math.min(protCold, 2);
		protElectrical      = Math.min(protElectrical, 2);
		protFire            = Math.min(protFire, 2);
		protPiercing        = Math.min(protPiercing, 2);
		protSlashing        = Math.min(protSlashing, 2);

		var alAcid          = al * protAcid;
		var alBludgeoning   = al * protBludgeoning;
		var alCold          = al * protCold;
		var alElectrical    = al * protElectrical;
		var alFire          = al * protFire;
		var alPiercing      = al * protPiercing;
		var alSlashing      = al * protSlashing;

		var totalAL = alAcid + alBludgeoning + alCold + alElectrical + alFire + alPiercing + alSlashing;
		var avgAL = totalAL / 7;
		core.debug("al: " + al + ", totalAL: " + round(totalAL) + ", avgAL: " + round(avgAL));
		return avgAL;
	};
	
	(function() {
		var id = "alBuffed";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Armor Level (Banes)"
		});
		property.assessment = true;
		property.getValue = core.getBuffedAL;
	})();
	
	function fProtCompareLowestFirst(a, b)  {
		if (a.prot < b.prot) return -1;
		if (a.prot > b.prot) return 1;
		return 0;
	}
	
	function fALAvg(prots2, al) {
		var protAL = 0;
		for (var i = 0; i < prots2.length; i++) {
			protAL += al * prots2[i].prot;
		}
		return (protAL / prots2.length);
	}
	
	function fProtTinksNeeded(aco, targetAL) {
		var baseAl                  = aco["oai.iai.al"] || aco.oai && aco.oai.iai && aco.oai.iai.al;
		
		var prots = [];
		prots.push({prot: aco["oai.iai.protSlashing"] || aco.oai && aco.oai.iai && aco.oai.iai.protSlashing, material: materialBronze});
		prots.push({prot: aco["oai.iai.protPiercing"] || aco.oai && aco.oai.iai && aco.oai.iai.protPiercing, material: materialAlabaster});
		prots.push({prot: aco["oai.iai.protBludgeoning"] || aco.oai && aco.oai.iai && aco.oai.iai.protBludgeoning, material: materialMarble});
		prots.push({prot: aco["oai.iai.protFire"] || aco.oai && aco.oai.iai && aco.oai.iai.protFire, material: materialCeramic});
		prots.push({prot: aco["oai.iai.protCold"] || aco.oai && aco.oai.iai && aco.oai.iai.protCold, material: materialWool});
		prots.push({prot: aco["oai.iai.protAcid"] || aco.oai && aco.oai.iai && aco.oai.iai.protAcid, material: materialDilloHide});
		prots.push({prot: aco["oai.iai.protElectrical"] || aco.oai && aco.oai.iai && aco.oai.iai.protElectrical, material: materialReedsharkHide});
		
		prots.sort(fProtCompareLowestFirst);
		var tinksNeeded = 0;
		var baseAvgAL = fALAvg(prots, baseAl);
		if (baseAvgAL >= targetAL) {
			return tinksNeeded; // 0 tinks
		}
		var avgAL;
		for (var t = 0; t < 100; t++) {
			tinksNeeded++;
			prot = prots[0].prot;
			material = prots[0].material;
			materialUtil = TinkeringUtil.getMaterial(material);

			//Debug("t: " + t + ", prot: " + Round(prot,1) + ", material: " + material);
			if (materialUtil) {
				prots[0].prot = prots[0].prot + materialUtil.modAmount;
				avgAL = fALAvg(prots, baseAl);
				if (avgAL >= targetAL) {
					core.debug("It would take " + tinksNeeded + " tinks to raise protAL from " + round(baseAvgAL) + " to " + round(avgAL));
					return tinksNeeded;
				}
			}
			prots.sort(fProtCompareLowestFirst);
		}
		return tinksNeeded;
	}
	
	(function() {
		var id = "alAvg";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Armor Level (Avg prots)"
		});
		property.assessment = true;

		property.getValue = function getValue(aco) {
			var baseAl                  = aco["oai.iai.al"] || aco.oai && aco.oai.iai && aco.oai.iai.al;
			var protAL = 0;
			protAL += baseAl * (aco["oai.iai.protSlashing"] || aco.oai && aco.oai.iai && aco.oai.iai.protSlashing);
			protAL += baseAl * (aco["oai.iai.protPiercing"] || aco.oai && aco.oai.iai && aco.oai.iai.protPiercing);
			protAL += baseAl * (aco["oai.iai.protBludgeoning"] || aco.oai && aco.oai.iai && aco.oai.iai.protBludgeoning);
			protAL += baseAl * (aco["oai.iai.protFire"] || aco.oai && aco.oai.iai && aco.oai.iai.protFire);
			protAL += baseAl * (aco["oai.iai.protCold"] || aco.oai && aco.oai.iai && aco.oai.iai.protCold);
			protAL += baseAl * (aco["oai.iai.protAcid"] || aco.oai && aco.oai.iai && aco.oai.iai.protAcid);
			protAL += baseAl * (aco["oai.iai.protElectrical"] || aco.oai && aco.oai.iai && aco.oai.iai.protElectrical);
			return (protAL / 7);
		};
		
		function fALAvg(prots2, al) {
			var protAL = 0;
			for (var i = 0; i < prots2.length; i++) {
				protAL += al * prots2[i].prot;
			}
			return (protAL / prots2.length);
		}
		
		property.tinksToValueAboveChance = function (aco, operator, value, chanceThreshold, startingAttempt) {
			var al                  = aco["oai.iai.al"] || aco.oai && aco.oai.iai && aco.oai.iai.al;
			var prots = [];
			prots.push({prot: aco["oai.iai.protSlashing"] || aco.oai && aco.oai.iai && aco.oai.iai.protSlashing, material: materialBronze});
			prots.push({prot: aco["oai.iai.protPiercing"] || aco.oai && aco.oai.iai && aco.oai.iai.protPiercing, material: materialAlabaster});
			prots.push({prot: aco["oai.iai.protBludgeoning"] || aco.oai && aco.oai.iai && aco.oai.iai.protBludgeoning, material: materialMarble});
			prots.push({prot: aco["oai.iai.protFire"] || aco.oai && aco.oai.iai && aco.oai.iai.protFire, material: materialCeramic});
			prots.push({prot: aco["oai.iai.protCold"] || aco.oai && aco.oai.iai && aco.oai.iai.protCold, material: materialWool});
			prots.push({prot: aco["oai.iai.protAcid"] || aco.oai && aco.oai.iai && aco.oai.iai.protAcid, material: materialDilloHide});
			prots.push({prot: aco["oai.iai.protElectrical"] || aco.oai && aco.oai.iai && aco.oai.iai.protElectrical, material: materialReedsharkHide});

			prots.sort(fProtCompareLowestFirst);
			
			var protAL = fALAvg(prots, al);
			var neededTinks = fProtTinksNeeded(aco, value);
			
			
			core.debug("al: " + al + ", protAL: " + round(protAL) + ", value: " + value + ", neededTinks: " + neededTinks);
			
			if (neededTinks == 0) {
				return {neededTinks: neededTinks, attempt: startingAttempt, achievable: true};
			} else if (neededTinks > 10) {
				return {neededTinks: neededTinks, attempt: startingAttempt, achievable: false};
			}
			
			var workmanship = core.getWorkmanshipSync({aco: aco});
			if (!workmanship) return;
			
			var prot;
			var material;
			var materialUtil;
			var acoValue;
			var tinks;
			var attempt = 0;
			for (var t = 0; t < neededTinks + 1; t++) { //+1 just in case, shouldn't be needed.
				prot = prots[0].prot;
				material = prots[0].material;
				materialUtil = TinkeringUtil.getMaterial(material);
				
				//acoValue = materialUtil.getAcoValue(aco);
				//acoValue = aco.oai.iai.al * prot;
				
				skillCur = _settings.skills[materialUtil.skid];
				attempt = startingAttempt + t + 1;
				skillNeeded = materialUtil.getThisSkillNeeded({
					itemWorkmanship   : workmanship,
					salvageWorkmanship: _settings.salvageWorkmanship,
					attempt           : attempt
				});

				chance = round(materialUtil.getThisTinkChance({
					itemWorkmanship   : workmanship,
					salvageWorkmanship: _settings.salvageWorkmanship,
					skillCurrent      : skillCur,
					attempt           : attempt
				}));
				
				core.debug("t: " + t + ", prot: " + round(prot, 1) + ", material: " + material + ", skillCur: " + skillCur + ", attempt: " + attempt + ", skillNeeded: " + skillNeeded + ", chance: " + chance);

				if (chance < chanceThreshold) {
					// Chance below threshold, give up.
					core.debug("FAIL attempt: " + attempt + ", skillNeeded: " + skillNeeded + ", chance: " + chance + " (below threshold)");
					return {neededTinks: neededTinks, achievable: false};
				}
				
				prots[0].prot = prots[0].prot + materialUtil.modAmount;
				
				protAL = fALAvg(prots, al);
				
				if (protAL >= value) {
					// Tink would meet our target value, break and return success.
					core.debug("We've reached target value (" + round(protAL, 1) + ") after " + attempt + " tinks.");
					break;
				}

				prots.sort(fProtCompareLowestFirst);// Resort so lowest prot is at the bottom again.
			}
			
			return {neededTinks: neededTinks, attempt: attempt, achievable: true};
		};
	})();

	core.getWieldSkill = function getWieldSkill(aco) {
		var skidWieldReq;
		if (typeof aco["oai.ibi.skidWieldReq"] !== "undefined") {
			skidWieldReq = aco["oai.ibi.skidWieldReq"];
		} else {
			skidWieldReq = aco.oai && aco.oai.ibi && aco.oai.ibi.skidWieldReq;
		}
		if (skidWieldReq > 0) { 
			// Has an actual wield req, go with that..
			return skidWieldReq;
		}
		
		// No wield req, go with regular skill id.
		var skid;
		if (typeof aco["oai.iwi.skid"] !== "undefined") {
			skid = aco["oai.iwi.skid"];
		} else {
			skid = aco.oai && aco.oai.iwi && aco.oai.iwi.skid;
		}
		return skid;

		/*
		var acoValue;
		if (aco.oai) {
			if (aco.oai.iwi) {
				acoValue = aco.oai.iwi.skid;
			}
			if (aco.oai.ibi.skidWieldReq && aco.oai.ibi.skidWieldReq > 0) { // Has an actual wield req, go with that instead.
				acoValue = aco.oai.ibi.skidWieldReq;
			}
		}
		return acoValue;
		*/
	};

	
	(function() {
		var id = "avgDamage";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Weapon Average Damage"
		});
		property.assessment = true;
		
		property.valueExists = function valueExists(aco) {
			var scaleDamageRange;
			if (typeof aco["oai.iwi.scaleDamageRange"] !== "undefined") {
				scaleDamageRange = aco["oai.iwi.scaleDamageRange"];
			} else {
				scaleDamageRange = aco.oai && aco.oai.iwi && aco.oai.iwi.scaleDamageRange;
			}
			return (typeof scaleDamageRangee !== "undefined");
		};
		property.getValue = getAcoAvgDamage;
		property.tinksToValueAboveChance = function tinksToValueAboveChance(aco, operator, value, chanceThreshold, startingAttempt) {
			// Return the number of tinks needed to hit a 'value' but only if they're above our chance threshold. If chance goes below we return nothing.
			
			if (!this.valueExists(aco) || (getWieldSkill(aco) == skidMissileWeapons)) {
				return;
			}

			// Get the optimal iron/grante order.
			var order = TinkeringUtil.getOptimalIronGraniteOrder(aco);
			
			// Figure out how many tinks we'd need to do to reach our target value.
			var avgDamage;
			var neededTinks = -1;
			var o;
			var maxDmg;
			for (var oi = 0; oi < order.length; oi++) {
				o = order[oi];
				maxDmg = o.baseMaxDamage;
				avgDamage = getAvgDamage(maxDmg, o.variance);

				//debug("oi: " + oi + ", avgDamage: " + avgDamage);
				if (avgDamage >= value) {
					neededTinks = oi + 1;
					break;
				}
			}
			
			core.debug("neededTinks: " + neededTinks);
			
			if (neededTinks < 0) {
				// Could reach target, bail.
				return {neededTinks: neededTinks, achievable: false};
			}
			
			var workmanship = core.getWorkmanshipSync({aco: aco});
			if (!workmanship) return;
			
			for (var oi = 0; oi < Math.min(neededTinks, 10); oi++) { //
				o = order[oi];
				avgDamage = getAvgDamage(o.baseMaxDamage, o.variance);
				
				// Check our chance to apply tink.
				material = o.material;
				materialUtil = TinkeringUtil.getMaterial(material);
				skillCur = _settings.skills[materialUtil.skid];
				attempt = startingAttempt + oi + 1;
				skillNeeded = materialUtil.getThisSkillNeeded({
					itemWorkmanship   : workmanship,
					salvageWorkmanship: _settings.salvageWorkmanship,
					attempt           : attempt
				});

				chance = round(materialUtil.getThisTinkChance({
					itemWorkmanship   : workmanship,
					salvageWorkmanship: _settings.salvageWorkmanship,
					skillCurrent      : skillCur,
					attempt           : attempt
				}));
				
				
				core.debug("avgDamage: " + avgDamage + ", skillCur: " + skillCur + ", attempt: " + attempt + ", skillNeeded: " + skillNeeded + ", chance: " + chance);

				if (chance < chanceThreshold) {
					// Chance below threshold, give up.
					core.debug("attempt: " + attempt + ", skillNeeded: " + skillNeeded + ", chance: " + chance + " (below threshold)");
					return {neededTinks: neededTinks, achievable: false};
				}

				if (avgDamage >= value) {
					// Tink would meet our target value, break and return success.
					break;
				}
			}
			return {neededTinks: neededTinks, attempt: attempt, achievable: true};
		};
	})();

	(function() {
		var id = "burden";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Burden"
		});
		property.operatorMaterials = {};
		property.operatorMaterials["<="]    = materialLinen;
		property.operatorMaterials["<"]     = materialLinen;
		property.getValue = function getValue(aco) {
			return aco.burden;
		};
	})();
	
	(function() {
		var id = "cantrip";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Cantrip"
		});
		property.assessment = true;
		property.valueExists = function valueExists(aco) {
			var cspellid;
			if (typeof aco["oai.iei.cspellid"] !== "undefined") {
				cspellid = aco["oai.iei.cspellid"];
			} else {
				cspellid = aco.oai && aco.oai.iei && aco.oai.iei.cspellid;
			}
			return cspellid > 0;
		};
		property.getValues = function getValues(aco) {
			var spells = [];
			if (aco.oai && aco.oai.iei && aco.oai.iei.cspellid) {
				var values = [];
				var spellID;
				for (var i = 0; i < aco.oai.iei.cspellid; i++) {
					spellID = aco.oai.iei.rgspellid(i);
					if (!spellID) continue;
					spell = skapi.SpellInfoFromSpellid(spellID);
					if (!spell) continue;
					values.push(spell.szName);
				}
				return values;
			} else if (typeof aco["oai.iei._cspells"] === "object") {
				return aco["oai.iei._cspells"].map(function(spellID) {
					var spell = skapi.SpellInfoFromSpellid(spellID);
					return spell && spell.szName;
				});
			}
		};
	})();

	(function() {
		var id = "cpyValue";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Value"
		});
		property.assessment = true;
		property.operatorMaterials = {};
		property.operatorMaterials["<="]    = materialPine;
		property.operatorMaterials["<"]     = materialPine;
		property.operatorMaterials[">="]    = materialGold;
		property.operatorMaterials[">"]     = materialGold;
		property.getValue = function getValue(aco) {
			return aco.cpyValue;
		};
		property.compare = {};
		property.compare["<"] = property.compare["<="] = function(a, b) {
			if (this.property(a) < this.property(b)) return -1;
			if (this.property(a) > this.property(b)) return 1;
			return 0;
		};
		property.compare[">"] = property.compare[">="] = function(a, b) {
			if (this.property(a) > this.property(b)) return -1;
			if (this.property(a) < this.property(b)) return 1;
			return 0;
		};
	})();
		
	core.getSingleValue = function getSingleValue(aco) {
		return aco.citemStack > 1 ? (aco.cpyValue / aco.citemStack) : aco.cpyValue;
	};
	
	(function() {
		var id = "cpyValueS";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Value (Single)"
		});
		property.getValue = function getValue(aco) {
			return core.getSingleValue(aco);
		};
	})();
	
	(function() {
		var id = "ctink";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Tink Count"
		});
		property.assessment = true;
		property.getValue = function getValue(aco) {
			return getObjectValue(aco, "oai.ibi.ctink");
		};
	})();
	
	core.getDamageMax = function getDamageMax(aco) {
		core.debug("<getDamageMax> " + aco);
		
		var dhealth = aco["oai.iwi.dhealth"] || aco.oai && aco.oai.iwi && aco.oai.iwi.dhealth;
		
		var spells = core.getAcoSpells({aco: aco});
		var spellid, spell;
		for (var i = 0; i < spells.length; i++) {
			spellid = spells[i];
			szName = getSpellName(spellid);
			if (szName.match("Legendary Blood Thirst")) {
				dhealth += 10;
				break;
			} else if (szName.match("Epic Blood Thirst")) {
				dhealth += 7;
				break;
			} else if (szName.match("Major Blood Thirst")) {
				dhealth += 4;
				break;
			} else if (szName.match("Minor Blood Thirst")) {
				dhealth += 2;
				break;
			}
		}
		
		return dhealth;
	};
	
	(function() {
		var id = "dhealth";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Weapon Damage Max"
		});
		property.assessment = true;
		property.getValue = getDamageMax;
	})();
	
	(function() {
		var id = "dhealthElemBonus";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Weapon Elemental Bonus"
		});
		property.assessment = true;
		property.suggestions = {
			Slashing   : dmtySlashing,
			Piercing   : dmtyPiercing,
			Bludgeoning: dmtyBludgeoning,
			Cold       : dmtyCold,
			Fire       : dmtyFire,
			Acid       : dmtyAcid,
			Electrical : dmtyElectrical,
			Nether     : dmtyNether
		};
		property.getValue = function getValue(aco) {
			if (typeof aco["oai.iwi.dhealthElemBonus"] !== "undefined") {
				return aco["oai.iwi.dhealthElemBonus"];
			}
			if (aco.oai && aco.oai.iwi) {
				return aco.oai.iwi.dhealthElemBonus;
			}
		};
	})();

	(function() {
		var id = "Imbued";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Imbued"
		});
		property.assessment = true;
		property.suggestions = {
			"Critical Strike"  : 0x00000001, //1
			"Crippling Blow"   : 0x00000002, //2
			"Armor Rending"    : 0x00000004, //4
			"Slash Rending"    : 0x00000008, //8
			"Pierce Rending"   : 0x00000010, //16
			"Bludgeon Rending" : 0x00000020, //32
			"Acid Rending"     : 0x00000040, //64
			"Cold Rending"     : 0x00000080, //128
			"Lightning Rending": 0x00000100, //256
			"Fire Rending"     : 0x00000200, //512
			"Melee Defense"    : 0x00000400, //1,024
			"Missile Defense"  : 0x00000800, //2,048
			"Magic Defense"    : 0x00001000, //4,096
			"Augmented Spell"  : 0x00002000, //8,192 (not sure of this name)
			"Magic Absorbing"  : 0x20000000, //536,870,912
			"Phantasmal"       : 0x80000000, //2,147,483,648
			"Phantasmal"       : -2147483648//2,147,483,648
		};
		property.getValue = function getValue(aco) {
			if (typeof aco["oai.ibi.raw.65715"] !== "undefined") {
				return aco["oai.ibi.raw.65715"];
			} else if (typeof aco["oai.ibi.raw.100B3"] !== "undefined") {
				return aco["oai.ibi.raw.100B3"];
			}
			if (aco.oai && aco.oai.ibi && aco.oai.ibi.raw) {
				return aco.oai.ibi.raw.Item(0x000100B3);
			}
		};
	})();

	(function() {
		var id = "resistanceCleaving";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Resistance Cleaving"
		});
		property.assessment = true;
		property.suggestions = {
			Slashing   : dmtySlashing,
			Piercing   : dmtyPiercing,
			Bludgeoning: dmtyBludgeoning,
			Cold       : dmtyCold,
			Fire       : dmtyFire,
			Acid       : dmtyAcid,
			Electrical : dmtyElectrical,
			Nether     : dmtyNether
		};
		property.getValue = function getValue(aco) {
			if (typeof aco["oai.ibi.raw.65799"] !== "undefined") {
				return aco["oai.ibi.raw.65799"];
			} else if (typeof aco["oai.ibi.raw.10107"] !== "undefined") {
				return aco["oai.ibi.raw.10107"];
			}
			if (aco.oai && aco.oai.ibi && aco.oai.ibi.raw) {
				return aco.oai.ibi.raw.Item(0x00010107);
			}
		};
	})();
	
	(function() {
		var id = "ItemXP";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Item XP"
		});
		property.assessment = true;
		property.getValue = function getValue(aco) {
			if (typeof aco["oai.ibi.raw.536870916"] !== "undefined") {
				return aco["oai.ibi.raw.536870916"];
			} else if (typeof aco["oai.ibi.raw.20000004"] !== "undefined") {
				return aco["oai.ibi.raw.20000004"];
			}
			if (aco.oai && aco.oai.ibi && aco.oai.ibi.raw) {
				return aco.oai.ibi.raw.Item(0x20000004);
			}
		};
	})();

	core.isCleave = function isCleave(aco) {
		if (aco.oai && aco.oai.ibi && aco.oai.ibi.raw) {
			var raw;
			if (typeof aco["oai.ibi.raw.65828"] !== "undefined") {
				raw = aco["oai.ibi.raw.65828"];
			} else if (typeof aco["oai.ibi.raw.10124"] !== "undefined") {
				raw = aco["oai.ibi.raw.10124"];
			} else {
				raw = aco.oai && aco.oai.ibi && aco.oai.ibi.raw.Item(0x10124);
			}
			return raw && (raw == 2) || false;
		}
	};
	
	(function() {
		var id = "cleave";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Weapon Cleave"
		});
		property.assessment = true;
		property.getValue = isCleave;
	})();
	
	/*
	(function(){
		var id = "Raw_65801"; // Has info on multistrike property and maybe others.
		var vProperty = function () {
			var data = {
				id: id,
				name: "Raw_65801"
			}
			Property.call(this, data, true);
		}
		vProperty.prototype = new Property();
		vProperty.prototype.constructor       = vProperty;
		vProperty.prototype.valueExists = function (aco) {
			return (typeof this.getValue(aco) !== "undefined");
		};
		vProperty.prototype.getValue = function (aco) {
			if (aco.oai && aco.oai.ibi && aco.oai.ibi.raw) {
				return aco.oai.ibi.raw.Item(65801);
			}
		};
		_properties[id] = new vProperty();
	})();
	*/
		
	(function() {
		var id = "itemset";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Item Set"
		});
		property.assessment = true;
		property.suggestions = {
			"Acid Proof"                   : 27,
			"Adept"                        : 14,
			"Archer"                       : 15,
			"Cold Proof"                   : 28,
			"Crafter"                      : 18,
			"Darkened Mind"                : 92,
			"Defender"                     : 16,
			"Dexterous"                    : 20,
			"Flame Proof"                  : 26,
			"Hardened"                     : 23,
			"Hearty"                       : 19,
			"Heroic Protector"             : 40,
			"Interlocking"                 : 25,
			"Lightning Proof"              : 29,
			"Reinforced"                   : 24,
			"Sigil of Armor Tinkering"     : 51,
			"Sigil of Defense"             : 35,
			"Sigil of Destruction"         : 36,
			"Sigil of Fury"                : 37,
			"Sigil of Growth"              : 38,
			"Sigil of Loyalty"             : 66,
			"Sigil of Vigor"               : 39,
			"Soldier"                      : 13,
			"Swift"                        : 22,
			"Tinker"                       : 17,
			"Weave of Alchemy"             : 49,
			"Weave of Arcane Lore"         : 50,
			"Weave of Assess Creature"     : 83,
			"Weave of Assess Person"       : 52,
			"Weave of Cooking"             : 55,
			"Weave of Creature Enchantment": 56,
			"Weave of Deception"           : 59,
			"Weave of Dirty Fighting"      : 84,
			"Weave of Dual Wield"          : 85,
			"Weave of Finesse Weapons"     : 58,
			"Weave of Fletching"           : 60,
			"Weave of Healing"             : 61,
			"Weave of Heavy Weapons"       : 76,
			"Weave of Item Enchantment"    : 62,
			"Weave of Item Tinkering"      : 63,
			"Weave of Leadership"          : 64,
			"Weave of Life Magic"          : 65,
			"Weave of Light Weapons"       : 53,
			"Weave of Magic Defense"       : 68,
			"Weave of Magic Item Tinkering": 69,
			"Weave of Mana Conversion"     : 70,
			"Weave of Melee Defense"       : 71,
			"Weave of Missile Defense"     : 72,
			"Weave of Missile Weapons"     : 54,
			"Weave of Recklessness"        : 86,
			"Weave of Salvaging"           : 73,
			"Weave of Shield"              : 87,
			"Weave of Sneak Attack"        : 88,
			"Weave of Summoning"           : 90,
			"Weave of Two Handed Combat"   : 78,
			"Weave of Void Magic"          : 80,
			"Weave of War Magic"           : 81,
			"Weave of Weapon Tinkering"    : 82,
			"Wise"                         : 21
		};
		property.getValue = function getValue(aco) {
			if (typeof aco["oai.ibi.raw.65801"] !== "undefined") {
				return aco["oai.ibi.raw.65801"];
			} else if (typeof aco["oai.ibi.raw.10109"] !== "undefined") {
				return aco["oai.ibi.raw.10109"];
			}
			if (aco.oai && aco.oai.ibi && aco.oai.ibi.raw) {
				return aco.oai.ibi.raw.Item(0x10109);
			}
		};
	})();
	
	(function() {
		var id = "difficulty";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Arcane Lore Requirment"
		});
		property.assessment = true;
		property.suggestions = {                                  // Note, game does a math.floor on base value, then adds buffs and does a floor again. It doesn't combine floating points.
			"maxed trained 10 att"           : 327,                // 208 + (200/3) = 274 + 40 + (40/3) = 327
			"maxed trained 10 att w/Essence" : 367,      // 208 + (200/3) = 274 + 40 + (40/3) = 327 + 40 = 367
			"maxed trained 100 att"          : 359,               // 208 + (290/3) = 305 + 40 + (40/3) = 358
			"maxed trained 100 att w/Essence": 409,     // 208 + (290/3) = 305 + 40 + (40/3) = 358 + 40 = 398
			"maxed spec 10 att"              : 345,                   // 226 + (200/3) = 292 + 40 + (40/3) = 345
			"maxed spec 10 att w/Essence"    : 385,         // 226 + (200/3) = 292 + 40 + (40/3) = 345 + 40 = 385
			"maxed spec 100 att"             : 375,                  // 226 + (290/3) = 322 + 40 + (40/3) = 375
			"maxed spec 100 att w/Essence"   : 415         // 226 + (290/3) = 322 + 40 + (40/3) = 375 + 40 = 415
		};
		property.getValue = function getValue(aco) {
			if (typeof aco["oai.iei.difficulty"] !== "undefined") {
				return aco["oai.iei.difficulty"];
			}
			if (aco.oai && aco.oai.iei) {
				return aco.oai.iei.difficulty;
			}
		};
	})();
	
	(function() {
		var id = "distRange";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Weapon Range"
		});
		property.assessment = true;
		property.getValue = function getValue(aco) {
			if (typeof aco["oai.iwi.distRange"] !== "undefined") {
				return aco["oai.iwi.distRange"];
			}
			if (aco.oai && aco.oai.iwi) {
				return aco.oai.iwi.distRange;
			}
		};
	})();
	
	(function() {
		var id = "dmty";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Weapon Damage Type"
		});
		property.assessment = true;
		property.suggestions = {
			Slashing   : dmtySlashing,
			Piercing   : dmtyPiercing,
			Bludgeoning: dmtyBludgeoning,
			Cold       : dmtyCold,
			Fire       : dmtyFire,
			Acid       : dmtyAcid,
			Electrical : dmtyElectrical,
			Nether     : dmtyNether
		};
		property.getValue = function getValue(aco) {
			if (typeof aco["oai.iwi.dmty"] !== "undefined") {
				return aco["oai.iwi.dmty"];
			}
			if (aco.oai && aco.oai.iwi) {
				return aco.oai.iwi.dmty;
			}
		};
	})();

	(function() {
		var id = "eqm";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Equipment Coverage"
		});
		property.suggestions = {
			AbdomenOuter  : eqmAbdomenOuter,
			AbdomenUnder  : eqmAbdomenUnder,
			Ammo          : eqmAmmo,
			Bracelets     : eqmBracelets,	// Both bracelet slots.
			ChestOuter    : eqmChestOuter,
			ChestUnder    : eqmChestUnder,
			Feet          : eqmFeet,
			FocusWeapon   : eqmFocusWeapon,	// Wand, staff, or orb slot.
			Hands         : eqmHands,
			Head          : eqmHead,
			LBracelet     : eqmLBracelet,
			LRing         : eqmLRing,
			LowerArmsOuter: eqmLowerArmsOuter,
			LowerArmsUnder: eqmLowerArmsUnder,
			LowerLegsOuter: eqmLowerLegsOuter,
			LowerLegsUnder: eqmLowerLegsUnder,
			Necklace      : eqmNecklace,
			RBracelet     : eqmRBracelet,
			RRing         : eqmRRing,
			RangedWeapon  : eqmRangedWeapon,	// Bow or crossbow slot.
			Rings         : eqmRings,	// Both ring slots.
			Shield        : eqmShield,
			TwoHand       : eqmTwoHand,	//Two handed weapons.
			UpperArmsOuter: eqmUpperArmsOuter,
			UpperArmsUnder: eqmUpperArmsUnder,
			UpperLegsOuter: eqmUpperLegsOuter,
			UpperLegsUnder: eqmUpperLegsUnder,
			Weapon        : eqmWeapon,	// Melee weapon slot.
			Weapons       : eqmWeapons	// All weapons of any kind.
		};
		property.getValue = function getValue(aco) {
			//core.debug("aco.eqm: " + aco.eqm);
			return aco.eqm;
		};
	})();
	
	(function() {
		var id = "fractManaConvMod";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Mana Conversion Modifier"
		});
		property.assessment = true;
		property.operatorMaterials = {};
		property.operatorMaterials[">="]    = materialOpal;
		property.operatorMaterials[">"]     = materialOpal;
		property.getValue = function getValue(aco) {
			if (typeof aco["oai.ibi.fractManaConvMod"] !== "undefined") {
				return aco["oai.ibi.fractManaConvMod"];
			}
			if (aco.oai && aco.oai.ibi) {
				return aco.oai.ibi.fractManaConvMod;
			}
		};
	})();
	
	(function() {
		var id = "icon";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Icon"
		});
		property.getValue = function getValue(aco) {
			return aco.icon;
		};
	})();
	
	(function() {
		var id = "iconOverlay";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Icon Overlay"
		});
		property.suggestions = {
			"Level 1": 27700,
			"Level 2": 27701,
			"Level 3": 27702,
			"Level 4": 27703,
			"Level 5": 27704
		};
		property.getValue = function getValue(aco) {
			return aco.iconOverlay;
		};
	})();
	
	(function() {
		var id = "iconUnderlay";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Icon Underlay"
		});
		property.getValue = function getValue(aco) {
			return aco.iconUnderlay;
		};
	})();
	
	(function() {
		var id = "lvlWieldReq";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Wield Skill Level Req"
		});
		property.assessment = true;
		property.suggestions = {
			"0 All"           : 0,
			"250 Melee"       : 250,
			"270 Missile"     : 270,
			"290 Missile/Wand": 290,
			"300 Melee"       : 300,
			"310 Wand"        : 310,
			"315 Missile"     : 315,
			"325 Melee"       : 325,
			"330 Wand"        : 330,
			"335 Missile"     : 335,
			"350 Melee"       : 350,
			"355 Wand"        : 355,
			"360 Missile"     : 360,
			"370 Melee"       : 370,
			"375 Missile/Wand": 375,
			"385 Missile/Wand": 385,
			"400 Melee"       : 400,
			"420 Melee"       : 420,
			"430 Melee"       : 430
		};
		property.getValue = function getValue(aco) {
			if (typeof aco["oai.ibi.lvlWieldReq"] !== "undefined") {
				return aco["oai.ibi.lvlWieldReq"] || 0;
			}
			if (aco.oai && aco.oai.ibi) {
				return aco.oai.ibi.lvlWieldReq || 0;
			}
		};
	})();
	
	(function() {
		var id = "wieldMagicDReq";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Wield MagicD Req"
		});
		property.assessment = true;
		property.suggestions = {
			"maxed train 10 innate att" : 265,   // maxed trained magicD 10 att: 208+(((190+10)/7)*2)=265
			"maxed train 100 innate att": 290,  // maxed trained magicD 100 att: 208+(((190+100)/7)*2)=290
			"maxed spec 10 innate att"  : 293,    // maxed spec magicD 10 att: 226+((190+10/7)*2)+10=293
			"maxed spec 100 innate att" : 318    // maxed spec magicD 100 att: 226+((190+100/7)*2)+10=318
		};
		property.getValue = function getValue(aco) {
			if (aco.oai && aco.oai.ibi && aco.oai.ibi.skidWieldReq == skidMagicDefense) {
				return aco.oai.ibi.lvlWieldReq || 0;
			}
			
			if (aco["oai.ibi.skidWieldReq"] == skidMagicDefense) {
				return aco["oai.ibi.lvlWieldReq"] || 0;
			}
			
			// If property doesn't exist on item, return 0.
			return 0;
		};
	})();
	
	(function() {
		var id = "wieldMeleeDReq";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Wield MeleeD Req"
		});
		property.assessment = true;
		property.suggestions = {
			"maxed train 10 innate att" : 341,   // maxed trained meleeD 10 att: 208+(((190+10)/3)*2)=341
			"maxed train 100 innate att": 401,  // maxed trained meleeD 100 att: 208+(((190+100)/3)*2)=401
			"maxed spec 10 innate att"  : 369,    // maxed spec meleeD 10 att: 226+(((190+10)/3)*2)+10=369
			"maxed spec 100 innate att" : 429    // maxed spec meleeD 100 att: 226+(((190+100)/3)*2)+10=429
		};
		property.getValue = function getValue(aco) {
			if (aco.oai && aco.oai.ibi && aco.oai.ibi.skidWieldReq == skidMeleeDefense) {
				return aco.oai.ibi.lvlWieldReq || 0;
			}
			
			if (aco["oai.ibi.skidWieldReq"] == skidMeleeDefense) {
				return aco["oai.ibi.lvlWieldReq"] || 0;
			}
			
			// If property doesn't exist on item, return 0.
			return 0;
		};
	})();
	
	(function() {
		var id = "wieldMissileDReq";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Wield MissileD Req"
		});
		property.assessment = true;
		property.suggestions = {
			"maxed train 10 innate att" : 288,   // maxed trained MissileD 10 att: 208+(((190+10)/5)*2)=288
			"maxed train 100 innate att": 321,  // maxed trained MissileD 100 att: 208+(((190+100)/5)*2)=321
			"maxed spec 10 innate att"  : 316,    // maxed spec MissileD 10 att: 226+(((190+10)/5)*2)+10=316
			"maxed spec 100 innate att" : 352    // maxed spec MissileD 100 att: 226+(((190+100)/5)*2)+10=352
		};
		property.getValue = function getValue(aco) {
			if (aco.oai && aco.oai.ibi && aco.oai.ibi.skidWieldReq == skidMissileDefense) {
				return aco.oai.ibi.lvlWieldReq || 0;
			}
			
			if (aco["oai.ibi.skidWieldReq"] == skidMissileDefense) {
				return aco["oai.ibi.lvlWieldReq"] || 0;
			}
			
			// If property doesn't exist on item, return 0.
			return 0;
		};
	})();
	
	(function() {
		var id = "wieldLevelReq";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Wield Char Level Req"
		});
		property.assessment = true;
		property.getValue = function getValue(aco) {
			if (aco.oai && aco.oai.ibi && aco.oai.ibi.skidWieldReq == skidNil) {
				return aco.oai.ibi.lvlWieldReq || 0;
			}
			
			if (aco["oai.ibi.skidWieldReq"] == skidNil) {
				return aco["oai.ibi.lvlWieldReq"] || 0;
			}
			
			// If property doesn't exist on item, return 0.
			return 0;
		};
	})();
	
	(function() {
		var id = "material";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Material"
		});
		property.suggestions = {
			Agate        : materialAgate,
			Alabaster    : materialAlabaster,
			Amber        : materialAmber,
			Aquamarine   : materialAquamarine,
			Azurite      : materialAzurite,
			BlackGarnet  : materialBlackGarnet,
			BlackOpal    : materialBlackOpal,
			Bloodstone   : materialBloodstone,
			Brass        : materialBrass,
			Bronze       : materialBronze,
			Carnelian    : materialCarnelian,
			Ceramic      : materialCeramic,
			Citrine      : materialCitrine,
			Copper       : materialCopper,
			Diamond      : materialDiamond,
			DilloHide    : materialDilloHide,
			Ebony        : materialEbony,
			Emerald      : materialEmerald,
			FireOpal     : materialFireOpal,
			Gold         : materialGold,
			Granite      : materialGranite,
			GreenGarnet  : materialGreenGarnet,
			GromnieHide  : materialGromnieHide,
			Hematite     : materialHematite,
			ImperialTopaz: materialImperialTopaz,
			Iron         : materialIron,
			Ivory        : materialIvory,
			Jet          : materialJet,
			LapisLazuli  : materialLapisLazuli,
			LavenderJade : materialLavenderJade,
			Leather      : materialLeather,
			Linen        : materialLinen,
			Mahogany     : materialMahogany,
			Malachite    : materialMalachite,
			Marble       : materialMarble,
			Moonstone    : materialMoonstone,
			Oak          : materialOak,
			Opal         : materialOpal,
			Peridot      : materialPeridot,
			Pine         : materialPine,
			Porcelain    : materialPorcelain,
			Pyreal       : materialPyreal,
			RedGarnet    : materialRedGarnet,
			RedJade      : materialRedJade,
			ReedsharkHide: materialReedsharkHide,
			RoseQuartz   : materialRoseQuartz,
			Ruby         : materialRuby,
			Sandstone    : materialSandstone,
			Sapphire     : materialSapphire,
			Satin        : materialSatin,
			Silk         : materialSilk,
			Silver       : materialSilver,
			SmokeyQuartz : materialSmokeyQuartz,
			Steel        : materialSteel,
			Sunstone     : materialSunstone,
			Teak         : materialTeak,
			Velvet       : materialVelvet,
			WhiteSapphire: materialWhiteSapphire,
			Wool         : materialWool,
			YellowTopaz  : materialYellowTopaz,
			Zircon       : materialZircon
		};
		property.getValue = function getValue(aco) {
			return aco.material;
		};
	})();
	
	(function() {
		var id = "mcm";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Merchant Category Mask"
		});
		property.getValue = function getValue(aco) {
			return aco.mcm;
		};
	})();
	
	core.isMultiStrike = function isMultiStrike(aco) {
		var raw;
		if (aco.oai && aco.oai.ibi && aco.oai.ibi.raw) {
			raw = aco.oai.ibi.raw.Item(65583);
		} else if (typeof aco["oai.ibi.raw.65583"] !== "undefined") {
			raw = aco["oai.ibi.raw.65583"];
		} else if (typeof aco["oai.ibi.raw.1002F"] !== "undefined") {
			raw = aco["oai.ibi.raw.1002F"];
		}
		return raw && (raw == 160 || raw == 320) || false;
	};
	
	(function() {
		var id = "multistrike";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Weapon Multistrike"
		});
		property.assessment = true;
		property.getValue = isMultiStrike;
	})();
	
	(function() {
		var id = "weaponType";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Weapon Type"
		});
		property.assessment = true;
		property.suggestions = {
			"Axe"           : 3,
			"Bow"           : 8,
			"Crossbow"      : 9,
			"Dagger"        : 6,
			"Mace"          : 4,
			"Spear"         : 5,
			"Staff"         : 7,
			"Sword"         : 2,
			"Thrown Weapon" : 10,
			"Unarmed Weapon": 1
		};
		property.getValue = function getValue(aco) {
			if (typeof aco["oai.ibi.raw.65889"] !== "undefined") {
				return aco["oai.ibi.raw.65889"];
			} else if (typeof aco["oai.ibi.raw.10161"] !== "undefined") {
				return aco["oai.ibi.raw.10161"];
			}
			return (
				aco.oai &&
				aco.oai.ibi &&
				aco.oai.ibi.raw &&
				aco.oai.ibi.raw.Item(0x10161) //65889
			);	
		};
	})();
	
	(function() {
		var id = "ocm";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Object category"
		});
		property.suggestions = {

			//		Nil: ocmNil,
			Player       : ocmPlayer,
			Monster      : ocmMonster,
			PlayerCorpse : ocmPlayerCorpse,
			MonsterCorpse: ocmMonsterCorpse,
			Lifestone    : ocmLifestone,
			Portal       : ocmPortal,
			Merchant     : ocmMerchant,
			Equipment    : ocmEquipment,
			PK           : ocmPK,
			NonPK        : ocmNonPK,
			NPC          : ocmNPC,
			Hook         : ocmHook,
			All          : ocmAll
		};
		property.getValue = function getValue(aco) {
			return aco.ocm;
		};
	})();
	
	(function() {
		var id = "olc";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Object Location Category"
		});
		property.suggestions = {
			Nil      : olcNil,
			Inventory: olcInventory,
			Contained: olcContained,
			Equipped : olcEquipped,
			OnGround : olcOnGround,
			All      : olcAll
		};
		property.getValue = function getValue(aco) {
			return aco.olc;
		};
	})();
	
	(function() {
		var id = "oty";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Object Type"
		});
		property.suggestions = {
			Container  : otyContainer,
			Inscribable: otyInscribable, 	
			NoPickup   : otyNoPickup,	
			Player     : otyPlayer,
			Selectable : otySelectable, 	
			PK         : otyPK,
			Readable   : otyReadable,	
			Merchant   : otyMerchant,	 	
			Door       : otyDoor,	
			Corpse     : otyCorpse,	
			Lifestone  : otyLifestone,	
			Food       : otyFood,
			HealingKit : otyHealingKit, 	
			Lockpick   : otyLockpick, 	
			Portal     : otyPortal, 	
			Foci       : otyFoci,
			PKL        : otyPKL 
		};
		property.getValue = function getValue(aco) {
			return aco.oty;
		};
	})();

	(function() {
		var id = "rareid";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Rare ID"
		});
		property.assessment = true;
		property.getValue = function getValue(aco) {
			if (typeof aco["oai.ibi.rareid"] !== "undefined") {
				return aco["oai.ibi.rareid"];
			}
			if (aco.oai && aco.oai.ibi) {
				return aco.oai.ibi.rareid;
			}
		};
	})();

	/*
	(function(){
		var id = "rareID2";
		var vProperty = function () {
			var data = {
				id: id,
				name: "Rare ID2"
			}
			Property.call(this, data, true);
		}
		vProperty.prototype = new Property();
		vProperty.prototype.constructor       = vProperty;
		vProperty.prototype.valueExists = function (aco) {
			return (typeof this.getValue(aco) !== "undefined");
		};
		vProperty.prototype.getValue = function (aco) {
			if (aco.oai && aco.oai.ibi && aco.oai.ibi.raw) {
				return aco.oai.ibi.raw.Item(0x00010011);
			}
		};
		_properties[id] = new vProperty();
	})();
*/
	core.getAttackBonus = function getAttackBonus(aco) {
		if (aco.oai && aco.oai.iwi) {
			var value = aco.oai.iwi.scaleAttackBonus;
			
			if (aco.oai && aco.oai.iei) {
				var szName;
				for (var i = 0; i < aco.oai.iei.cspellid; i++) {
					szName = getSpellName(aco.oai.iei.rgspellid(i));
					if (szName.match("Legendary Heart Thirst")) {
						value += 0.09;
						break;
					} else if (szName.match("Epic Heart Thirst")) {
						value += 0.07;
						break;
					} else if (szName.match("Major Heart Thirst")) {
						value += 0.05;
						break;
					} else if (szName.match("Minor Heart Thirst")) {
						value += 0.03;
						break;
					}
				}
			}
			return value;
		}
	};
	
	(function() {
		var id = "scaleAttackBonus";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Weapon Attack Bonus"
		});
		property.assessment = true;
		property.operatorMaterials = {};
		property.operatorMaterials[">="]    = materialVelvet;
		property.operatorMaterials[">"]     = materialVelvet;
		property.getValue = function getValue(aco) {
			if (aco.oai && aco.oai.iwi) {
				return getAttackBonus(aco);
			}
		};
	})();
	
	(function() {
		var id = "scaleDamageBonus";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Weapon Damage Bonus"
		});
		property.assessment = true;
		property.operatorMaterials = {};
		property.operatorMaterials[">="]    = materialMahogany;
		property.operatorMaterials[">"]     = materialMahogany;
		property.getValue = function getValue(aco) {
			if (typeof aco["oai.iwi.scaleDamageBonus"] !== "undefined") {
				return aco["oai.iwi.scaleDamageBonus"];
			}
			if (aco.oai && aco.oai.iwi) {
				return aco.oai.iwi.scaleDamageBonus;
			}
		};
	})();
	
	core.getDefenseBonus = function getDefenseBonus(aco) {
		var value = 0;
		if (typeof aco["oai.iwi.scaleDefenseBonus"] !== "undefined") {
			value = aco["oai.iwi.scaleDefenseBonus"];
		} else if (aco.oai && aco.oai.iwi) {
			value = aco.oai.iwi.scaleDefenseBonus;
		}

		if (aco.oai && aco.oai.iei) {
			var szName;
			for (var i = 0; i < aco.oai.iei.cspellid; i++) {
				szName = getSpellName(aco.oai.iei.rgspellid(i));
				if (szName.match("Legendary Defender")) {
					value += 0.09;
					break;
				} else if (szName.match("Epic Defender")) {
					value += 0.07;
					break;
				} else if (szName.match("Major Defender")) {
					value += 0.05;
					break;
				} else if (szName.match("Minor Defender")) {
					value += 0.03;
					break;
				}
			}
		}
		return value;
	};
	
	(function() {
		var id = "scaleDefenseBonus";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Weapon Melee Defense"
		});
		property.assessment = true;
		property.operatorMaterials = {};
		property.operatorMaterials[">="]    = materialBrass;
		property.operatorMaterials[">"]     = materialBrass;
		property.getValue = getDefenseBonus;
	})();
	
	(function() {
		var id = "scaleDamageRange";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Weapon Damage Range (min damage)"
		});
		property.assessment = true;
		property.operatorMaterials = {};
		property.operatorMaterials[">="]    = materialGranite;
		property.operatorMaterials[">"]     = materialGranite;
		property.getValue = function getValue(aco) {
			if (typeof aco["oai.iwi.scaleDamageRange"] !== "undefined") {
				return aco["oai.iwi.scaleDamageRange"];
			}
			if (aco.oai && aco.oai.iwi) {
				return aco.oai.iwi.scaleDamageRange;
			}
		};
	})();
	
	(function() {
		var id = "scaleMagicDBonus";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Weapon MagicD Bonus"
		});
		property.assessment = true;
		property.getValue = function getValue(aco) {
			if (typeof aco["oai.iwi.scaleMagicDBonus"] !== "undefined") {
				return aco["oai.iwi.scaleMagicDBonus"];
			}
			if (aco.oai && aco.oai.iwi) {
				return aco.oai.iwi.scaleMagicDBonus;
			}
		};
	})();
	
	(function() {
		var id = "scaleMissileDBonus";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Weapon MissileD bonus"
		});
		property.assessment = true;
		property.getValue = function getValue(aco) {
			if (typeof aco["oai.iwi.scaleMissileDBonus"] !== "undefined") {
				return aco["oai.iwi.scaleMissileDBonus"];
			}
			if (aco.oai && aco.oai.iwi) {
				return aco.oai.iwi.scaleMissileDBonus;
			}
		};
	})();
	
	/*
	core.getPvMElemBonus = function getPvMElemBonus(aco) {
		if (aco.oai && aco.oai.iwi) {
			var value = aco.oai.iwi.scalePvMElemBonus;
			
			if (aco.oai.iei) {
				var szName;
				for (var i = 0; i < aco.oai.iei.cspellid; i++) {
					szName = getSpellName(aco.oai.iei.rgspellid(i));
					if (szName.match("Legendary Spirit Thirst")) {
						value += 0.07;
						break;
					} else if (szName.match("Epic Spirit Thirst")) {
						value += 0.05; // wiki says 4%, in game says 5%.
						break;
					} else if (szName.match("Major Spirit Thirst")) {
						value += 0.03;
						break;
					} else if (szName.match("Minor Spirit Thirst")) {
						value += 0.01;
						break;
					}
				}
			}
			return value;
		}
	};
	*/
	
	(function() {
		var id = "scalePvMElemBonus";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Weapon PvM elemental bonus"
		});
		property.assessment = true;
		property.operatorMaterials = {};
		property.operatorMaterials[">="]    = materialGreenGarnet;
		property.operatorMaterials[">"]     = materialGreenGarnet;
		property.getValue = function getValue(aco) {
			var value = 0;
			if (typeof aco["oai.iwi.scalePvMElemBonus"] !== "undefined") {
				value = aco["oai.iwi.scalePvMElemBonus"];
			} else if (aco.oai && aco.oai.iwi) {
				value = aco.oai.iwi.scalePvMElemBonus;
			}
			var spells = core.getAcoSpells({aco: aco});
			var bonus = spells.reduce(function(tally, spellId) {
				var szName = core.getSpellName(spellId);
				if (szName.match("Legendary Spirit Thirst")) {
					return tally + 0.07;
				} else if (szName.match("Epic Spirit Thirst")) {
					return tally + 0.05;
				} else if (szName.match("Major Spirit Thirst")) {
					return tally + 0.03;
				} else if (szName.match("Minor Spirit Thirst")) {
					return tally + 0.01;
				}
				return tally;
			}, 0);
			return value;// bonus
		};
	})();
	
	/*
	core.getPvPElemBonus = function getPvPElemBonus(aco) {
		if (aco.oai && aco.oai.iwi) {
			var value = aco.oai.iwi.scalePvMElemBonus;
			
			if (aco.oai.iei) {
				var szName;
				for (var i = 0; i < aco.oai.iei.cspellid; i++) {
					szName = getSpellName(aco.oai.iei.rgspellid(i));
					if (szName.match("Legendary Spirit Thirst")) {
						value += (0.07 / 2);
						break;
					} else if (szName.match("Epic Spirit Thirst")) {
						value += (0.05/2); // wiki says 4%, in game says 5%.
						break;
					} else if (szName.match("Major Spirit Thirst")) {
						value += (0.03/2);
						break;
					} else if (szName.match("Minor Spirit Thirst")) {
						value += (0.01/2);
						break;
					}
				}
			}
			return value;
		}
	};
	*/
	
	(function() {
		var id = "scalePvPElemBonus";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Weapon PvP elemental bonus"
		});
		property.assessment = true;
		property.getValue = function (aco) {
			var value = 0;
			if (typeof aco["oai.iwi.scalePvPElemBonus"] !== "undefined") {
				value = aco["oai.iwi.scalePvPElemBonus"];
			} else if (aco.oai && aco.oai.iwi) {
				value = aco.oai.iwi.scalePvPElemBonus;
			}
			var spells = core.getAcoSpells({aco: aco});
			var bonus = spells.reduce(function(tally, spellId) {
				var szName = core.getSpellName(spellId);
				if (szName.match("Legendary Spirit Thirst")) {
					return tally + (0.07 / 2);
				} else if (szName.match("Epic Spirit Thirst")) {
					return tally + (0.05 / 2);
				} else if (szName.match("Major Spirit Thirst")) {
					return tally + (0.03 / 2);
				} else if (szName.match("Minor Spirit Thirst")) {
					return tally + (0.01 / 2);
				}
				return tally;
			}, 0);
			return value + bonus;
		};
	})();
	
	(function() {
		var id = "skidWieldReq";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Wield Skill ID Req"
		});
		property.assessment = true;
		property.suggestions = {
			"Heavy Weapons"     : skidHeavyWeapons,
			"Light Weapons"     : skidLightWeapons,
			"Finesse Weapons"   : skidFinesseWeapons,
			"Missile Weapons"   : skidMissileWeapons,
			"Two Handed Weapons": skidTwoHanded,
			"War Magic"         : skidWarMagic,
			"Life Magic"        : skidLifeMagic,
			"Void Magic"        : skidVoidMagic
		};
		property.getValue = function getValue(aco) {
			if (typeof aco["oai.ibi.skidWieldReq"] !== "undefined") {
				return aco["oai.ibi.skidWieldReq"];
			}
			if (aco.oai && aco.oai.ibi) {
				return aco.oai.ibi.skidWieldReq;
			}
		};
	})();

	(function() {
		var id = "weapSkill";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Weapon Skill ID"
		});
		property.assessment = true;
		property.suggestions = {
			"Heavy Weapons"     : skidHeavyWeapons,
			"Light Weapons"     : skidLightWeapons,
			"Finesse Weapons"   : skidFinesseWeapons,
			"Missile Weapons"   : skidMissileWeapons,
			"Two Handed Weapons": skidTwoHanded,
			"War Magic"         : skidWarMagic,
			"Life Magic"        : skidLifeMagic,
			"Void Magic"        : skidVoidMagic
		};
		property.getValue = function getValue(aco) {
			if (typeof aco["oai.iwi.skid"] !== "undefined") {
				return aco["oai.iwi.skid"];
			}
			if (aco.oai && aco.oai.iwi) {
				return aco.oai.iwi.skid;
			}
		};
	})();
	
	
	(function() {
		var eqmTrinket = 0x04000000;
		var slots = [eqmTrinket, eqmHead, eqmChestUnder, eqmUpperLegsUnder, eqmHands, eqmFeet, eqmChestOuter, eqmAbdomenOuter, eqmUpperArmsOuter, eqmLowerArmsOuter, eqmUpperLegsOuter, eqmLowerLegsOuter, eqmNecklace, eqmBracelets, eqmRings];
		var id = "slotcount";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Slot Count"
		});

		//property.valueExists = function valueExists(aco) {
		//	return (aco.eqm !== "undefined");
		//};
		property.getValue = function getValue(aco) {
			var acoValue = 0;
			for (var i = 0; i < slots.length; i++) {
				if (aco.eqm & slots[i]) {
					acoValue += 1;
				}
			}
			return acoValue;
		};
	})();

	(function() {
		var id = "oid";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Object ID"
		});
		property.getValue       = function getValue(aco) {
			return aco.oid;
		};
	})();
	
	(function() {
		var id = "acoWearer_szName";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Wearer Name"
		});
		property.getValue       = function getValue(aco)  {
			if (aco["acoWearer.szName"]) {
				return aco["acoWearer.szName"];
			}
			if (aco.acoWearer) {
				return aco.acoWearer.szName;
			}			
		};
	})();
	
	(function() {
		var id = "acoWearer_oid";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Wearer Object ID"
		});
		property.getValue       = function getValue(aco)  {
			if (aco["acoWearer.oid"]) {
				return aco["acoWearer.oid"];
			}
			if (aco.acoWearer) {
				return aco.acoWearer.oid;
			}			
		};
	})();
	
	(function() {
		var id = "acoContainer_szName";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Container Name"
		});
		property.getValue       = function getValue(aco)  {
			if (aco["acoContainer.szName"]) {
				return aco["acoContainer.szName"];
			}
			if (aco.acoContainer) {
				return aco.acoContainer.szName;
			}			
		};
	})();
	
	(function() {
		var id = "acoContainer_oid";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Container oid"
		});
		property.getValue       = function getValue(aco) {
			if (aco["acoContainer.oid"]) {
				return aco["acoContainer.oid"];
			}
			if (aco.acoContainer) {
				return aco.acoContainer.oid;
			}			
		};
	})();
	
	(function() {
		var id = "szName";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Name"
		});
		property.getValue       = core.getFullName;
	})();

	(function() {
		var id = "szInscription";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Inscription"
		});
		property.assessment = true;
		property.getValue = function getValue(aco) {
			if (typeof aco["oai.ibi.szInscription"] !== "undefined") {
				return aco["oai.ibi.szInscription"];
			}
			if (aco.oai && aco.oai.ibi) {
				return aco.oai.ibi.szInscription;
			}
		};
	})();
	
	(function() {
		var id = "szInscriber";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Inscriber"
		});
		property.assessment = true;
		property.getValue = function (aco) {
			if (typeof aco["oai.ibi.szInscriber"] !== "undefined") {
				return aco["oai.ibi.szInscriber"];
			}
			if (aco.oai && aco.oai.ibi) {
				return aco.oai.ibi.szInscriber;
			}
		};
	})();
	
	(function() {
		var id = "szRaceReq";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Race Requirment"
		});
		property.assessment = true;
		property.getValue = function getValue(aco) {
			if (typeof aco["oai.iei.szRaceReq"] !== "undefined") {
				return aco["oai.iei.szRaceReq"];
			}
			if (aco.oai && aco.oai.iei) {
				return aco.oai.iei.szRaceReq;
			}
		};
	})();
	
	(function() {
		var id = "vLaunch";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Weapon Launch Speed"
		});
		property.getValue = function getValue(aco) {
			if (typeof aco["oai.iwi.vLaunch"] !== "undefined") {
				return aco["oai.iwi.vLaunch"];
			}
			if (aco.oai && aco.oai.iwi) {
				return aco.oai.iwi.vLaunch;
			}
		};
	})();
	
	(function() {
		var id = "workmanship";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Workmanship"
		});

		/*
		property.getValueAsync = function getValueAsync(aco, callback) {
			return core.getWorkmanship({aco:aco}, callback);
		};
		property.valueExistsAsync = function valueExistsAsync(aco, callback) {
			return this.getValueAsync(aco, function onValue(err, results) {
				callback(err, (typeof results !== "undefined"));
			});
		};
		*/
		property.getValue = function getValue(aco) {
			return aco.workmanship;
		};
	})();

	core.getWeaponAnimation = function getWeaponAnimation(aco) {
		var szName = aco.szName;
		szName = szName.replace("Acid ", ""); 
		szName = szName.replace("Frost ", ""); 
		szName = szName.replace("Flaming ", ""); 
		szName = szName.replace("Lightning ", ""); 
		szName = szName.replace("Seasoned Explorer ", ""); 
		szName = szName.replace("Academy ", ""); 
		szName = szName.replace("Electric ", ""); 
		szName = szName.replace("Fire ", ""); 
		szName = szName.replace("Acidic ", ""); 
		szName = szName.replace("Frozen ", ""); 
		szName = szName.replace("Weeping ", ""); 
		szName = szName.replace("Hollow ", ""); 
		szName = szName.replace("Peerless ", ""); 
		szName = szName.replace("Atlan ", ""); 
		szName = szName.replace(" of Black Fire", ""); 
		szName = szName.replace(" of the Quiddity", ""); 
		szName = szName.replace("Fetid ", ""); // http://acpedia.org/wiki/Fetid_Dirk
		szName = szName.replace("Royal Runed Two Handed ", ""); 
		szName = szName.replace("Princely Runed Two Handed ", ""); 
		return data.weaponAnimations[szName];
	};
	
	core.getAttacksPerSecond = function getAttacksPerSecond(aco) {
		var a = getWeaponAnimation(aco);

		if (!a) {
		//	core.info("I don't know the animation for " + aco.szName + ".");
			return;
		}
		
		var stanceID = a.stance;

		var duration	= data.animationTimes[stanceID][a.low];
		duration += data.animationTimes[stanceID][a.mid];
		duration += data.animationTimes[stanceID][a.high];
		duration = duration / 3;

		if (a.stance == 68) { // stance_2H
			// If it's a 2H weapon, it hits twice. So half the duration.
			duration = duration / 2;
		}
		if (isMultiStrike(aco)) {
			duration = duration / 2; 
		}
		
		return 1000 / duration;
	};

	(function() {
		var id = "dps";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Weapon DPS"
		});
		property.assessment = true;
		property.getValue = function getValue(aco) {
			var attacksPerSecond = getAttacksPerSecond(aco);
			if (!attacksPerSecond) {
				return;
			}
			var avgDamage = getAcoAvgDamage(aco);
			if (!avgDamage) {
				return;
			}
			return attacksPerSecond * avgDamage;
		};
		property.tinksToValueAboveChance = function tinksToValueAboveChance(aco, operator, value, chanceThreshold, startingAttempt) {
			if (!this.valueExists(aco) || (getWieldSkill(aco) == skidMissileWeapons)) {
				return;
			}
			
			var attacksPerSecond = getAttacksPerSecond(aco);// How many swings per second this weapon can do.
			
			// Get the optimal iron/grante order.
			var order = TinkeringUtil.getOptimalIronGraniteOrder(aco);
			var dps;

			var cantripBonus = getCantripDamageBonus(aco);
			
			// Figure out how many tinks we'd need to do to reach our target value.
			var avgDamage;
			var neededTinks = -1;
			var o;
			var maxDmg;
			for (var oi = 0; oi < order.length; oi++) {
				o = order[oi];
				maxDmg = o.baseMaxDamage + cantripBonus;
				avgDamage = getAvgDamage(maxDmg, o.variance);
				dps = avgDamage * attacksPerSecond;

				//debug("oi: " + oi + ", avgDamage: " + avgDamage);
				if (dps >= value) {
					neededTinks = oi + 1;
					break;
				}
			}
			core.debug("neededTinks: " + neededTinks);
			if (neededTinks == 0) {
				return {neededTinks: neededTinks, attempt: startingAttempt, achievable: true};
			} else if (neededTinks > 10) {
				return {neededTinks: neededTinks, attempt: startingAttempt, achievable: false};
			}
			
			var workmanship = core.getWorkmanshipSync({aco: aco});
			if (!workmanship) return;
			
			var o;
			for (var oi = 0; oi < Math.min(neededTinks, 10); oi++) { //
				o = order[oi];
				avgDamage = getAvgDamage(o.baseMaxDamage + cantripBonus, o.variance);
				dps = avgDamage * attacksPerSecond;
				
				material = o.material;
				materialUtil = TinkeringUtil.getMaterial(material);
				
				skillCur = _settings.skills[materialUtil.skid];
				attempt = startingAttempt + oi + 1;

				skillNeeded = materialUtil.getThisSkillNeeded({
					itemWorkmanship   : workmanship,
					salvageWorkmanship: _settings.salvageWorkmanship,
					attempt           : attempt
				});
				
				chance = round(materialUtil.getThisTinkChance({
					itemWorkmanship   : workmanship,
					salvageWorkmanship: _settings.salvageWorkmanship,
					skillCurrent      : skillCur,
					attempt           : attempt
				}));
				
				if (chance < chanceThreshold) {
					// Chance below threshold, give up.
					core.debug("attempt: " + attempt + ", skillNeeded: " + skillNeeded + ", chance: " + chance + " (below threshold)");
					return {neededTinks: neededTinks, achievable: false};
				}
				
				if (dps >= value) {
					// Tink would meet our target value, break and return success.
					break;
				}
			}
			return {neededTinks: neededTinks, attempt: attempt, achievable: true};
		};
	})();
	
	core.getPossibleTinkCount = function getPossibleTinkCount(aco) {
		var skid = 0;
		var material = materialSteel; // The material type doesn't really matter, all the materials have the same calc.
		if (aco.mcm & mcmArmor) {
			skid = skidArmorTinkering;
		} else if (aco.mcm & (mcmWeaponsMelee | mcmWeaponsMissile | mcmMagicItems)) {
			skid = skidWeaponTinkering;
		} else if (aco.mcm & (mcmClothing)) {
			skid = skidItemTinkering;// Pine?
		}
		if (skid == 0) {
			return;
		}
		var skillCur = _settings.skills[skid];
		
		var workmanship = core.getWorkmanshipSync({aco: aco});
		if (!workmanship) return;
		
		core.debug("aco: " + aco.szName + ", workmanship: " + workmanship + ", skid: " + skid + ", skillCur: " + skillCur + ", salvageWorkmanship: " + _settings.salvageWorkmanship);
		
		var attempt = 1;
		var skillNeeded;
		var chance;
		for (var i = 0; i < 10; i++) {
			skillNeeded = TinkeringUtil.getSkillNeeded(workmanship, material, _settings.salvageWorkmanship, attempt); //(itemWorkmanship, tinkMaterial, quaility, attempt)
			chance = round(TinkeringUtil.getTinkChance(workmanship, material, _settings.salvageWorkmanship, skillCur, attempt));//fGetTinkChance(itemWorkmanship, tinkMaterial, quaility, skill, attempt)
			core.debug("attempt: " + attempt + ", skillNeeded: " + skillNeeded + ", chance: " + chance);
			if (chance < _settings.tinkChanceThreshold) {
				break;
			}
			attempt += 1;
		}
		return attempt - 1; // -1 since we won't be attempting the last attempt number, we need to start it at 1 for calc reasons though.
	};
	
	(function() {
		var id = "manaMax";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Mana Max"
		});
		property.assessment = true;
		property.operatorMaterials = {};
		property.operatorMaterials[">="]    = materialMoonstone;
		property.operatorMaterials[">"]     = materialMoonstone;
		property.getValue = function getValue(aco) {
			if (typeof aco["oai.iei.manaMax"] !== "undefined") {
				return aco["oai.iei.manaMax"];
			}
			if (aco.oai && aco.oai.iei) {
				return aco.oai.iei.manaMax;
			}
		};
	})();
	
	core.getCompareRating = function getCompareRating(wStat, uStat) {
		// a 15% meleeD weapon will show as 1.15. We remove the 1 and compare the over half.
		wStat -= 1;
		uStat -= 1;
		return (wStat / uStat) * 100;
	};
	
	(function() {
		var id = "palette0";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Colour palette #0 ID"
		});
		property.getValue = function getValue(aco) {
			var palettes = data.palettes[aco.oid] || core.getPalettes(aco.oid);
			if (palettes) {
				return palettes[0];
			}
		};
	})();

	(function() {
		var id = "palette1";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Colour palette #1 ID"
		});
		property.getValue = function getValue(aco) {
			var palettes = data.palettes[aco.oid] || core.getPalettes(aco.oid);
			if (palettes) {
				return palettes[1];
			}
		};
	})();
	
	(function() {
		var id = "palette2";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Colour palette #2 ID"
		});
		property.getValue = function getValue(aco) {
			var palettes = data.palettes[aco.oid] || core.getPalettes(aco.oid);
			if (palettes) {
				return palettes[2];
			}
		};
	})();
	
	(function() {
		var id = "palette3";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Colour palette #3 ID"
		});
		property.getValue = function getValue(aco) {
			var palettes = data.palettes[aco.oid] || core.getPalettes(aco.oid);
			if (palettes) {
				return palettes[3];
			}
		};
	})();
	
	(function() {
		var id = "palette0RGB";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Colour palette #0 RGB"
		});
		property.getValue = function getValue(aco) {
			var palettes = data.palettes[aco.oid] || core.getPalettes(aco.oid);
			if (palettes) {
				var pID = palettes[0];
				if (core.colourIds[pID]) {
					return core.colourIds[pID].rgb;
				}
			}
		};
	})();
	
	(function() {
		var id = "palette1RGB";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Colour palette #1 RGB"
		});
		property.getValue = function getValue(aco) {
			var palettes = data.palettes[aco.oid] || core.getPalettes(aco.oid);
			if (palettes) {
				var pID = palettes[1];
				if (core.colourIds[pID]) {
					return core.colourIds[pID].rgb;
				}
			}
		};
	})();
	
	(function() {
		var id = "palette2RGB";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Colour palette #2 RGB"
		});
		property.getValue = function getValue(aco) {
			var palettes = data.palettes[aco.oid] || core.getPalettes(aco.oid);
			if (palettes) {
				var pID = palettes[2];
				if (core.colourIds[pID]) {
					return core.colourIds[pID].rgb;
				}
			}
		};
	})();
	
	(function() {
		var id = "palette3RGB";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Colour palette #3 RGB"
		});
		property.getValue = function getValue(aco) {
			var palettes = data.palettes[aco.oid] || core.getPalettes(aco.oid);
			if (palettes) {
				var pID = palettes[3];
				if (core.colourIds[pID]) {
					return core.colourIds[pID].rgb;
				}
			}
		};
	})();
	
	core.hexToRgb = function hexToRgb(hex) {
		var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
		return result ? {
			r: parseInt(result[1], 16),
			g: parseInt(result[2], 16),
			b: parseInt(result[3], 16)
		} : null;
	};
	
	core.getPaletteRSL = function getPaletteRSL(pID) {
		if (!core.colourIds[pID]) {
			return;
		}
		var aRGB = core.colourIds[pID].rgb; //ACColours[pID].getRGB();
		var splitRGB = hexToRgb(aRGB);
		return rgbToHsl(splitRGB.r, splitRGB.g, splitRGB.b);
	};
	
	core.rgbToHsl = function rgbToHsl(r, g, b) {
		r /= 255, g /= 255, b /= 255;
		var max = Math.max(r, g, b), min = Math.min(r, g, b);
		var h, s, l = (max + min) / 2;

		if(max == min) {
			h = s = 0; // achromatic
		} else {
			var d = max - min;
			s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
			switch(max) {
			case r: h = (g - b) / d + (g < b ? 6 : 0); break;
			case g: h = (b - r) / d + 2; break;
			case b: h = (r - g) / d + 4; break;
			}
			h /= 6;
		}

		return [h, s, l];
	};
	
	(function() {
		var id = "palette0Heu";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Colour palette #0 heu"
		});
		property.getValue = function getValue(aco) {
			var palettes = data.palettes[aco.oid] || core.getPalettes(aco.oid);
			if (palettes) {
				var pID = palettes[0];
				if (pID) {
					var HSL = getPaletteRSL(pID);
					return HSL[0];
				}
			}
		};
	})();
	
	(function() {
		var id = "palette1Heu";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Colour palette #1 heu"
		});
		property.getValue = function getValue(aco) {
			var palettes = data.palettes[aco.oid] || core.getPalettes(aco.oid);
			if (palettes) {
				var pID = palettes[1];
				if (pID) {
					var HSL = getPaletteRSL(pID);
					return HSL[0];
				}
			}
		};
	})();
	
	(function() {
		var id = "palette2Heu";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Colour palette #2 heu"
		});
		property.getValue = function getValue(aco) {
			var palettes = data.palettes[aco.oid] || core.getPalettes(aco.oid);
			if (palettes) {
				var pID = palettes[2];
				if (pID) {
					var HSL = getPaletteRSL(pID);
					return HSL[0];
				}
			}
		};
	})();
	
	(function() {
		var id = "palette3Heu";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Colour palette #3 heu"
		});
		property.getValue = function getValue(aco) {
			var palettes = data.palettes[aco.oid] || core.getPalettes(aco.oid);
			if (palettes) {
				var pID = palettes[3];
				if (pID) {
					var HSL = getPaletteRSL(pID);
					return HSL[0];
				}
			}
		};
	})();
	
	(function() {
		var id = "palette0Sat";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Colour palette #0 saturation"
		});
		property.getValue = function getValue(aco) {
			var palettes = data.palettes[aco.oid] || core.getPalettes(aco.oid);
			if (palettes) {
				var pID = palettes[0];
				if (pID) {
					var HSL = getPaletteRSL(pID);
					return HSL[1];
				}
			}
		};
	})();
	
	(function() {
		var id = "palette1Sat";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Colour palette #1 saturation"
		});
		property.getValue = function getValue(aco) {
			var palettes = data.palettes[aco.oid] || core.getPalettes(aco.oid);
			if (palettes) {
				var pID = palettes[1];
				if (pID) {
					var HSL = getPaletteRSL(pID);
					return HSL[1];
				}
			}
		};
	})();
	
	(function() {
		var id = "palette2Sat";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Colour palette #2 saturation"
		});
		property.getValue = function getValue(aco) {
			var palettes = data.palettes[aco.oid] || core.getPalettes(aco.oid);
			if (palettes) {
				var pID = palettes[2];
				if (pID) {
					var HSL = getPaletteRSL(pID);
					return HSL[1];
				}
			}
		};
	})();
	
	(function() {
		var id = "palette3Sat";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Colour palette #3 saturation"
		});
		property.getValue = function getValue(aco) {
			var palettes = data.palettes[aco.oid] || core.getPalettes(aco.oid);
			if (palettes) {
				var pID = palettes[3];
				if (pID) {
					var HSL = getPaletteRSL(pID);
					return HSL[1];
				}
			}
		};
	})();
	
	(function() {
		var id = "palette0lgt";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Colour palette #0 lightness"
		});
		property.getValue = function getValue(aco) {
			var palettes = data.palettes[aco.oid] || core.getPalettes(aco.oid);
			if (palettes) {
				var pID = palettes[0];
				if (pID) {
					var HSL = getPaletteRSL(pID);
					return HSL[2];
				}
			}
		};
	})();
	
	(function() {
		var id = "palette1lgt";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Colour palette #1 lightness"
		});
		property.getValue = function getValue(aco) {
			var palettes = data.palettes[aco.oid] || core.getPalettes(aco.oid);
			if (palettes) {
				var pID = palettes[1];
				if (pID) {
					var HSL = getPaletteRSL(pID);
					return HSL[2];
				}
			}
		};
	})();
	
	(function() {
		var id = "palette2lgt";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Colour palette #2 lightness"
		});
		property.getValue = function getValue(aco) {
			var palettes = data.palettes[aco.oid] || core.getPalettes(aco.oid);
			if (palettes) {
				var pID = palettes[2];
				if (pID) {
					var HSL = getPaletteRSL(pID);
					return HSL[2];
				}
			}
		};
	})();
	
	(function() {
		var id = "palette3lgt";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Colour palette #3 lightness"
		});
		property.getValue = function getValue(aco) {
			var palettes = data.palettes[aco.oid] || core.getPalettes(aco.oid);
			if (palettes) {
				var pID = palettes[3];
				if (pID) {
					var HSL = getPaletteRSL(pID);
					return HSL[2];
				}
			}
		};
	})();

	(function() {
		var id = "ratingDam";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Rating Damage" // Rating first so the properties sort together.
		});
		property.assessment = true;
		property.getValue = function getValue(aco) {
			if (typeof aco["oai.ibi.raw.65906"] !== "undefined") {
				return aco["oai.ibi.raw.65906"];
			} else if (typeof aco["oai.ibi.raw.10172"] !== "undefined") {
				return aco["oai.ibi.raw.10172"];
			}
			return (
				aco.oai &&
				aco.oai.ibi &&
				aco.oai.ibi.raw &&
				aco.oai.ibi.raw.Item(0x10172)
			);
		};
	})();
	
	(function() {
		var id = "ratingDamResist";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Rating Dam Resist" // Rating first so the properties sort together.
		});
		property.assessment = true;
		property.getValue = function getValue(aco) {
			if (typeof aco["oai.ibi.raw.65907"] !== "undefined") {
				return aco["oai.ibi.raw.65907"];
			} else if (typeof aco["oai.ibi.raw.10173"] !== "undefined") {
				return aco["oai.ibi.raw.10173"];
			}
			return (
				aco.oai &&
				aco.oai.ibi &&
				aco.oai.ibi.raw &&
				aco.oai.ibi.raw.Item(0x10173)
			);
		};
	})();

	(function() {
		var id = "ratingCrit";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Rating Crit" // Rating first so the properties sort together.
		});
		property.assessment = true;
		property.getValue = function getValue(aco) {
			if (typeof aco["oai.ibi.raw.65908"] !== "undefined") {
				return aco["oai.ibi.raw.65908"];
			} else if (typeof aco["oai.ibi.raw.10174"] !== "undefined") {
				return aco["oai.ibi.raw.10174"];
			}
			return (
				aco.oai &&
				aco.oai.ibi &&
				aco.oai.ibi.raw &&
				aco.oai.ibi.raw.Item(0x10174)
			);
		};
	})();
	
	(function() {
		var id = "ratingCritResist";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Rating Crit Resist" // Rating first so the properties sort together.
		});
		property.assessment = true;
		property.getValue = function getValue(aco) {
			if (typeof aco["oai.ibi.raw.65909"] !== "undefined") {
				return aco["oai.ibi.raw.65909"];
			} else if (typeof aco["oai.ibi.raw.10175"] !== "undefined") {
				return aco["oai.ibi.raw.10175"];
			}
			return (
				aco.oai &&
				aco.oai.ibi &&
				aco.oai.ibi.raw &&
				aco.oai.ibi.raw.Item(0x10175)
			);
		};
	})();
	
	(function() {
		var id = "ratingCritDam";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Rating Crit Dam" // Rating first so the properties sort together.
		});
		property.assessment = true;
		property.getValue = function getValue(aco) {
			if (typeof aco["oai.ibi.raw.65910"] !== "undefined") {
				return aco["oai.ibi.raw.65910"];
			} else if (typeof aco["oai.ibi.raw.10176"] !== "undefined") {
				return aco["oai.ibi.raw.10176"];
			}
			return (
				aco.oai &&
				aco.oai.ibi &&
				aco.oai.ibi.raw &&
				aco.oai.ibi.raw.Item(0x10176)
			);
		};
	})();
	
	(function() {
		var id = "ratingCritDamResist";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Rating Crit Dam Resist" // Rating first so the properties sort together.
		});
		property.assessment = true;
		property.getValue = function getValue(aco) {
			if (typeof aco["oai.ibi.raw.65911"] !== "undefined") {
				return aco["oai.ibi.raw.65911"];
			} else if (typeof aco["oai.ibi.raw.10177"] !== "undefined") {
				return aco["oai.ibi.raw.10177"];
			}
			return (
				aco.oai &&
				aco.oai.ibi &&
				aco.oai.ibi.raw &&
				aco.oai.ibi.raw.Item(0x10177)
			);
		};
	})();

	(function() {
		var id = "ratingCombined";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Rating Combined" // All the ratings combined
		});
		property.assessment = true;
		property.getValue = function getValue(aco) {
			var total = 0;
			total += _properties["ratingDam"].getValue(aco) || 0;
			total += _properties["ratingDamResist"].getValue(aco) || 0;
			total += _properties["ratingCrit"].getValue(aco) || 0;
			total += _properties["ratingCritResist"].getValue(aco) || 0;
			total += _properties["ratingCritDam"].getValue(aco) || 0;
			total += _properties["ratingCritDamResist"].getValue(aco) || 0;
			return total;
		};
	})();

	(function() {
		var id = "essUberRating";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Essence Uber Rating"
		});
		property.assessment = true;
		property.getValue = function getValue(aco) {
			if (aco.oty == 0x4000012 && aco.mcm & 0x80) {
				// Essenses.
				if (aco.oai && aco.oai.ibi) {
					var ratings = [];
					
					
					var dam             = _properties["ratingDam"].getValue(aco);
					core.debug("dam: " + dam);
					if (dam) {
						//ratings.push((dam / data.uberEssenceStats[useSkillLevelRequired].dam) * 100);
						ratings.push((dam / 20) * 100);
					}
					
					var damResist       = _properties["ratingDamResist"].getValue(aco);
					core.debug("damResist: " + damResist);
					if (damResist) {
						ratings.push((damResist / 20) * 100);
					}
					
					var crit            = _properties["ratingCrit"].getValue(aco);
					core.debug("crit: " + crit);
					if (crit) {
						ratings.push((crit / 20) * 100);
					}
					
					var critResist      = _properties["ratingCritResist"].getValue(aco);
					core.debug("critResist: " + critResist);
					if (critResist) {
						ratings.push((critResist / 20) * 100);
					}
					
					var critDam         = _properties["ratingCritDam"].getValue(aco);
					core.debug("critDam: " + critDam);
					if (critDam) {
						ratings.push((critDam / 20) * 100);
					}
					
					var critDamResist   = _properties["ratingCritDamResist"].getValue(aco);
					core.debug("critDamResist: " + critDamResist);
					if (critDamResist) {
						ratings.push((critDamResist / 20) * 100);
					}
					
					var totalRating = 0;
					for (var i = 0; i < ratings.length; i++) {
						totalRating += ratings[i];
					}
					
					return totalRating / 6;
				}

			}
		};
	})();
	
	(function() {
		var id = "useLevelRequired";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Use Level Required"
		});
		property.assessment = true;
		property.getValue = function getValue(aco) {
			if (typeof aco["oai.ibi.raw.65905"] !== "undefined") {
				return aco["oai.ibi.raw.65905"];
			} else if (typeof aco["oai.ibi.raw.10171"] !== "undefined") {
				return aco["oai.ibi.raw.10171"];
			}
			return (
				aco.oai &&
				aco.oai.ibi &&
				aco.oai.ibi.raw &&
				aco.oai.ibi.raw.Item(0x10171)
			);
		};
	})();
	
	(function() {
		var id = "useSkillRequired";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Use Skill ID Required"
		});
		property.assessment = true;
		property.getValue = function getValue(aco) {
			if (typeof aco["oai.ibi.raw.65902"] !== "undefined") {
				return aco["oai.ibi.raw.65902"];
			} else if (typeof aco["oai.ibi.raw.1016E"] !== "undefined") {
				return aco["oai.ibi.raw.1016E"];
			}
			return (
				aco.oai &&
				aco.oai.ibi &&
				aco.oai.ibi.raw &&
				aco.oai.ibi.raw.Item(0x1016e)
			);
		};
	})();
	
	(function() {
		var id = "useSkillLevelRequired";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Use Skill Level Required"
		});
		property.assessment = true;
		property.getValue = function getValue(aco) {
			if (typeof aco["oai.ibi.raw.65903"] !== "undefined") {
				return aco["oai.ibi.raw.65903"];
			} else if (typeof aco["oai.ibi.raw.1016F"] !== "undefined") {
				return aco["oai.ibi.raw.1016F"];
			}
			return (
				aco.oai &&
				aco.oai.ibi &&
				aco.oai.ibi.raw &&
				aco.oai.ibi.raw.Item(0x1016f)
			);
		};
	})();
	
	(function() {
		var id = "usesRemaining";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Uses Remaining"
		});
		property.assessment = true;
		property.getValue = function getValue(aco) {
			if (typeof aco["oai.ibi.raw.65628"] !== "undefined") {
				return aco["oai.ibi.raw.65628"];
			} else if (typeof aco["oai.ibi.raw.1005C"] !== "undefined") {
				return aco["oai.ibi.raw.1005C"];
			}
			return (
				aco.oai &&
				aco.oai.ibi &&
				aco.oai.ibi.raw &&
				aco.oai.ibi.raw.Item(0x1005c)
			);
		};
	})();
	
	(function() {
		var id = "itemXP";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Item XP (Current)"
		});
		property.assessment = true;
		property.getValue = function getValue(aco) {
			if (typeof aco["oai.ibi.raw.536870916"] !== "undefined") {
				return aco["oai.ibi.raw.536870916"];
			} else if (typeof aco["oai.ibi.raw.20000004"] !== "undefined") {
				return aco["oai.ibi.raw.20000004"];
			}
			return (
				aco.oai &&
				aco.oai.ibi &&
				aco.oai.ibi.raw &&
				aco.oai.ibi.raw.Item(0x20000004)
			);
		};
	})();
	
	(function() {
		var id = "itemXPMax";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Item XP (Max)"
		});
		property.assessment = true;
		property.getValue = function getValue(aco) {
			if (typeof aco["oai.ibi.raw.536870917"] !== "undefined") {
				return aco["oai.ibi.raw.536870917"];
			} else if (typeof aco["oai.ibi.raw.20000005"] !== "undefined") {
				return aco["oai.ibi.raw.20000005"];
			}
			return (
				aco.oai &&
				aco.oai.ibi &&
				aco.oai.ibi.raw &&
				aco.oai.ibi.raw.Item(0x20000005)
			);
		};
	})();
	
	(function() {
		var id = "itemLevel";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Item Level (Current)"
		});
		property.assessment = true;
		property.getValue = function getValue(aco) {
			if (typeof aco["oai.ibi.raw.65645"] !== "undefined") {
				return aco["oai.ibi.raw.65645"];
			} else if (typeof aco["oai.ibi.raw.1006D"] !== "undefined") {
				return aco["oai.ibi.raw.1006D"];
			}
			return (
				aco.oai &&
				aco.oai.ibi &&
				aco.oai.ibi.raw &&
				aco.oai.ibi.raw.Item(0x0001006D)
			);
		};
	})();
	
	(function() {
		var id = "itemLevelMax";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Item Level (Max)"
		});
		property.assessment = true;
		property.getValue = function getValue(aco) {
			if (typeof aco["oai.ibi.raw.65855"] !== "undefined") {
				return aco["oai.ibi.raw.65855"];
			} else if (typeof aco["oai.ibi.raw.1013F"] !== "undefined") {
				return aco["oai.ibi.raw.1013F"];
			}
			return (
				aco.oai &&
				aco.oai.ibi &&
				aco.oai.ibi.raw &&
				aco.oai.ibi.raw.Item(0x0001013F)
			);
		};
	})();
	
	(function() {
		var id = "bonded";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Bonded"
		});
		property.assessment = true;
		property.getValue = function getValue(aco) {
			if (typeof aco["oai.ibi.fBonded"] !== "undefined") {
				return aco["oai.ibi.fBonded"];
			}
			return (
				aco.oai &&
				aco.oai.ibi &&
				aco.oai.ibi.fBonded
			);
		};
	})();
	
	(function() {
		var id = "attuned";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Attuned"
		});
		property.assessment = true;
		property.getValue = function getValue(aco) {
			if (typeof aco["oai.ibi.fAttuned"] !== "undefined") {
				return aco["oai.ibi.fAttuned"];
			}
			return (
				aco.oai &&
				aco.oai.ibi &&
				aco.oai.ibi.fAttuned
			);
		};
	})();
	
	(function() {
		var id = "retained";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Retained"
		});
		property.assessment = true;
		property.getValue = function getValue(aco) {
			if (typeof aco["oai.ibi.fRetained"] !== "undefined") {
				return aco["oai.ibi.fRetained"];
			}
			return (
				aco.oai &&
				aco.oai.ibi &&
				aco.oai.ibi.fRetained
			);
		};
	})();
	
	(function() {
		var id = "cantripFamily";
		var property = _properties[id] = new Property({
			id  : id,
			name: "Cantrip Family"
		});
		property.assessment = true;
		property.valueExists = function valueExists(aco) {
			var cspellid;
			if (typeof aco["oai.iei.cspellid"] !== "undefined") {
				cspellid = aco["oai.iei.cspellid"];
			} else {
				cspellid = aco.oai && aco.oai.iei && aco.oai.iei.cspellid;
			}
			return cspellid > 0;
		};
		property.getValues = function getValues(aco) {
			var spells = [];
			if (aco.oai && aco.oai.iei && aco.oai.iei.cspellid) {
				var values = [];
				var spellID;
				for (var i = 0; i < aco.oai.iei.cspellid; i++) {
					spellID = aco.oai.iei.rgspellid(i);
					if (!spellID) continue;
					spell = skapi.SpellInfoFromSpellid(spellID);
					if (!spell) continue;
					values.push(spell.family);
				}
				return values;
			} else if (typeof aco["oai.iei._cspells"] === "object") {
				return aco["oai.iei._cspells"].map(function(spellID) {
					var spell = skapi.SpellInfoFromSpellid(spellID);
					return spell && spell.family;
				});
			}
		};
	})();
	
	//////////////
	// Rules    //
	//////////////
	
	core.getRGBPeakVariance = function getRGBPeakVariance(aHexRGB, bHexRGB) {
		var aRGB = hexToRgb(aHexRGB);
		var bRGB = hexToRgb(bHexRGB);
		if (!aRGB || !bRGB) return;
		var rV = Math.abs(aRGB.r - bRGB.r);
		var gV = Math.abs(aRGB.g - bRGB.g);
		var bV = Math.abs(aRGB.b - bRGB.b);
		var variance = rV;
		variance = Math.max(variance, gV);
		variance = Math.max(variance, bV);
		core.debug("rV: " + rV + ", gV: " + gV + ", bV: " + bV + ", variance :" + variance);
		return variance;
	};
	
	core.escapeRegExp = function escapeRegExp(str) {
		return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
	};
	
	var Rule;
	(function() {
		var evaluateOperators = {};
		evaluateOperators["=="] = function evalEqual(acoValue, value) {
			return acoValue == value;
		};
		evaluateOperators["!="] = function evalNotEqual(acoValue, value) {
			return acoValue != value;
		};
		evaluateOperators[">"] = function evalGreaterThan(acoValue, value) {
			return acoValue > value;
		};
		evaluateOperators[">="] = function evalGreaterOrEqual(acoValue, value) {
			return acoValue >= value;
		};
		evaluateOperators["<"] = function evalLessThan(acoValue, value) {
			return acoValue < value;
		};
		evaluateOperators["<="] = function evalLessOrEqual(acoValue, value) {
			return acoValue <= value;
		};
		evaluateOperators["&"] = function evalAnd(acoValue, value) {
			return acoValue & value;
		};
		evaluateOperators["!&"] = function evalNotAnd(acoValue, value) {
			return !(acoValue & value);
		};
		evaluateOperators["match"] = function evalMatch(acoValue, value) {
			var itemExp = new RegExp(value);
			var match;// = acoValue.match(itemExp);
			if (typeof acoValue === "string") {
				match = acoValue.match(itemExp);
			} else {
				match = acoValue.toString().match(itemExp);
			}
			return (typeof match !== "undefined" && match != null);
		};
		evaluateOperators["!match"] = function evalNotMatch(acoValue, value) {
			var itemExp = new RegExp(value);
			var match;// = acoValue.match(itemExp);
			if (typeof acoValue === "string") {
				match = acoValue.match(itemExp);
			} else {
				match = acoValue.toString().match(itemExp);
			}
			var ret = !(typeof match !== "undefined" && match != null);
			return ret;
		};
		evaluateOperators["RGBvariance"] = function evalRGBvariance(acoValue, value) {
			var values = value.split(":");
			if (values.length >= 2) {
				var maxVariance = values[1];
				var diff = getRGBPeakVariance(acoValue, values[0]);
				return (diff <= maxVariance);
			}
		};
		evaluateOperators["!Suggestion"] = function evalNoSuggestion(acoValue, value, aco) {
			var property = _properties[this.data.propertyId];
			var suggestions = property.getSuggestions();
			if (typeof suggestions !== "undefined") {
				for (var szName in suggestions)  {
					if (!suggestions.hasOwnProperty(szName)) continue;
					v = suggestions[szName];

					//inform(v + " == " + acoValue + " = " + (v == acoValue));
					if (v == acoValue) {
						core.debug("Found matching suggestion (" + acoValue + " = " + szName + ")");
						return false;
					}
				}
			}
			core.debug("No matching suggestion");
			return true;
		};
		evaluateOperators["Exists"] = function(acoValue, value, aco) {
			var property = _properties[this.data.propertyId];
			return ((typeof property.getValue(aco) !== "undefined").toString() == value);
		};
		
		Rule = function(data)  {
			this.data = data || {};

			//this.data.values = this.data.values || [];
			this.evaluateOperator = evaluateOperators[this.data.operator];
		};
		
		
		Rule.prototype.getEnabled = function getEnabled(value) {
			return this.data.enabled;
		};
		
		Rule.prototype.setEnabled = function setEnabled(value) {
			this.data.enabled = value;
		};
		
		Rule.prototype.getName = function getName() {
			return this.data.name;
		};
		
		Rule.prototype.setName = function setName(value) {
			this.data.name = value;
		};
		
		Rule.prototype.getPropertyID = function getPropertyID() {
			return this.data.propertyId;
		};
		
		Rule.prototype.setPropertyID = function setPropertyID(value) {
			this.data.propertyId = value;
		};
		
		Rule.prototype.getOperator = function getOperator() {
			return this.data.operator;
		};
		
		Rule.prototype.setOperator = function setOperator(value) {
			this.data.operator = value;
			this.evaluateOperator = evaluateOperators[this.data.operator];
		};
		
		Rule.prototype.getValue = function getValue() {
			return this.data.value;
		};
		
		Rule.prototype.setValue = function setValue(value) {
			this.data.value = value;
		};

		function fEvaluateValue(acoValue, operator, value) {
			//core.console("acoValue: " + acoValue + ", value: " + value);
			if (operator.match(/^==$/)) {
				return acoValue == value;
			} else if (operator.match(/^!=$/)) {
				return acoValue != value;
			} else if (operator.match(/^>$/)) {
				return acoValue > value;
			} else if (operator.match(/^>=$/)) {
				return acoValue >= value;
			} else if (operator.match(/^<$/)) {
				return acoValue < value;
			} else if (operator.match(/^<=$/)) {
				return acoValue <= value;
			} else if (operator.match(/^&$/)) {
				return acoValue & value;
			} else if (operator.match(/^!&$/)) {
				return !(acoValue & value);
			} else if (operator.match(/^match$/)) {
				//acoValue = escapeRegExp(acoValue);
				//value = escapeRegExp(value);
				var itemExp = new RegExp(value);
				var match;// = acoValue.match(itemExp);
				if (typeof acoValue === "string") {
					match = acoValue.match(itemExp);
				} else {
					match = acoValue.toString().match(itemExp);
				}
				
				//core.console("Match '" + acoValue + "' to '" + value + "': " + match);
				return (typeof match !== "undefined" && match != null);
			} else if (operator.match(/^!match$/)) {
				var itemExp = new RegExp(value);
				var match;// = acoValue.match(itemExp);
				if (typeof acoValue === "string") {
					match = acoValue.match(itemExp);
				} else {
					match = acoValue.toString().match(itemExp);
				}
				
				var ret = !(typeof match !== "undefined" && match != null);

				//core.console("Match '" + acoValue + "' to '" + value + "': " + match + ", ret: " + ret);
				return ret;

				//return (typeof match === "undefined" || match == null);
			} else if (operator.match(/^RGBvariance$/)) {
				var values = value.split(":");
				if (values.length >= 2) {
					var maxVariance = values[1];
					var diff = getRGBPeakVariance(acoValue, values[0]);
					core.debug("maxVariance: " + maxVariance + ", diff: " + diff);
					return (diff <= maxVariance);
				}
			} else {
				core.console("Unknown operator: " + operator);
			}
		}
		
		Rule.prototype.evaluateSync = function evaluateSync(params) {
			var aco = params.aco;
			var property = _properties[this.data.propertyId];
			var operator = this.data.operator;// || "==";
			var targetValue = this.data.value;
			var verbose = (!params || params.verbose === undefined ? core.debug : params.verbose);
			
			if (core.isNumeric(targetValue)) {
				targetValue = Number(targetValue);
			}
			
			
			var itemValue;// = property.getValue(aco);
			if (typeof property.getValue === "function") {
				itemValue = property.getValue(aco);
			} else if (typeof property.getValues === "function") {
				itemValue = property.getValues(aco);
			} else {
				verbose("No getValue(s) funciton for " + this.data.propertyId);
				throw new core.Error("No getValue(s) funciton for " + this.data.propertyId);
			}
			
			if (typeof itemValue === "undefined") {
				return false;
			}
			
			var evalResult;

			// Check if result is a array of results.
			if (!isArray(itemValue)) {
				var result = this.evaluateOperator(itemValue, targetValue, aco);
				
				verbose("property: " + property.data.id, "itemValue: " + itemValue, "operator: " + operator, "targetValue: " + targetValue);
				
				return result;
			}
			
			verbose("itemValue is a array", itemValue.length, itemValue.join(", "));
			
			var matches = 0;
			var acoValue;
			for (var i = 0; i < itemValue.length; i++) {
				acoValue = itemValue[i];
				evalResult = this.evaluateOperator(acoValue, targetValue, aco);
				verbose("i: " + i, "acoValue: " + acoValue, "evalResult: " + evalResult);
				if (evalResult) {
					matches += 1;
				}
			}
			verbose("matches: " + matches);
			
			if (operator == "!match" || operator == "!&" || operator == "!=") {
				// If none of the values match, then we should have equal 'matches' to the number of values. And less and there is a match.
				return (matches >= itemValue.length);
			} else {
				return (matches > 0);
			}
		};
		
		Rule.prototype.evaluateAsync = function evaluateAsync(params, callback) {
			core.debug("<evaluateAsync>");
			var cancellation = params.cancellation;// || new core.Cancellation();
			var aco = params.aco;
			var thisArg = params.thisArg;
			
			var self = this;
			
			var property = _properties[this.data.propertyId];
			var operator = this.data.operator;// || "==";
			var value = this.data.value;
			core.debug("property: " + property, "operator: " + operator, "value: " + value);
			core.debug("aco: " + aco);
			
			
			//core.console("property: " + property + ", operator: " + operator + ", value: " + value);
			
			if (typeof property.getValueAsync === "function") {// check async first.
				return property.getValueAsync(aco, onValue);
			} else if (typeof property.getValue === "function") {
				//core.debug("calling property.getValue(aco)...");
				return onValue(null, property.getValue(aco));
			} else if (typeof property.getValues === "function") {
				return onValue(null, property.getValues(aco));
			}

			function onValue(err, result) {
				core.debug("<evaluateAsync|onValue>", err, result);
				if (err) return callback.call(thisArg, err);
				if (cancellation && cancellation.canceled) return callback.call(thisArg, new Error("CANCELLATION"));

				if (typeof result === "undefined" && self.data.operator != "Exists") {
					return callback.call(thisArg, undefined, false);
				}
				
				var evalResult;

				// Check if result is a array of results.
				if (!isArray(result)) {
					evalResult = self.evaluateOperator(result, value, aco);
					//core.console(property.getName() + " " + evalResult);
					return callback.call(thisArg, undefined, evalResult);
				}
				
				var matches = 0;
				var acoValue;
				for (var i = 0; i < result.length; i++) {
					acoValue = result[i];
					evalResult = self.evaluateOperator(acoValue, value, aco);
					if (evalResult) {
						matches += 1;
					}
				}
				core.debug("matches: " + matches);
				
				if (operator == "!match" || operator == "!&" || operator == "!=") {
					// If none of the values match, then we should have equal 'matches' to the number of values. And less and there is a match.
					return callback.call(thisArg, undefined, (matches >= result.length));
				} else {
					return callback.call(thisArg, undefined, (matches > 0));
				}
			}
		};

		Rule.prototype.getProperty = function getProperty() {
			return _properties[this.data.propertyId];
		};

		Rule.prototype.tinksNeeded = function tinksNeeded(aco) {
			if (!_properties[this.data.propertyId]) {
				return;
			}
			var property = _properties[this.data.propertyId];
			return property.tinksToValue(aco, this.data.operator, this.data.value);
		};

		//var tinks = property.tinksToValueAboveChance(aco, "<=", 1000, _settings.tinkChanceThreshold, ctink);
		
		Rule.prototype.tinksNeededAboveChance = function tinksNeededAboveChance(aco, chanceThreshold, ctink) {
			if (!_properties[this.data.propertyId]) {
				core.debug("<tinksNeededAboveChance> No property object for " + this.data.propertyId + "!");
				return;
			}
			var property = _properties[this.data.propertyId];
			return property.tinksToValueAboveChance(aco, this.data.operator, this.data.value, chanceThreshold, ctink);
		};

		Rule.prototype.toString = function toString() {
			if (!_properties[this.data.propertyId]) {
				return "??";
			}
			var property = _properties[this.data.propertyId];
			var operator = this.data.operator || "??";
			
			var value = this.data.value;
			
			var szSuggestion = property.getSzSuggestion(value);
			
			if (typeof szSuggestion !== "undefined") {
				value += " (" + szSuggestion + ")";
			}
			
			return property.getName() + " " + operator + " " + value;
		};
		
		Rule.prototype.getData = function getData() {
			return this.data;
		};
		
		Rule.prototype.setData = function setData(value) {
			this.data = value;
		};
	})();

	//////////////
	// Profile  //
	//////////////
	
	var Profile;
	(function() {
		Profile = function(data, filePath) {
			this.data = data || {};
			this.data.maxTinks = this.data.maxTinks || 0;
			this.data.rules = this.data.rules || [];
			this.filePath = filePath;

			this.rules = [];
			var r;
			for (var i = 0; i < this.data.rules.length; i++) {
				r = this.data.rules[i];
				this.rules.push(new Rule(r.data));
			}
		};
		
		Profile.prototype.flags = {};
		Profile.prototype.flags.match               = Math.pow(2, 0);    // 1;
		Profile.prototype.flags.needsTinks          = Math.pow(2, 1);    // 2;
		Profile.prototype.flags.doesntHaveValue     = Math.pow(2, 2);    // 4;

		
		Profile.prototype.getName = function fGetName() {
			return this.data.name;
		};
		Profile.prototype.setName = function fSetName(value) {
			this.data.name = value;
		};

		Profile.prototype.addRule = function fAddRule(rule) {
			this.data.rules.push(rule);
			this.rules.push(new Rule(rule.data));
		};

		Profile.prototype.removeRule = function fRemoveRule(index) {
			this.data.rules.splice(index, 1);
			this.rules.splice(index, 1);
		};

		/*
		Profile.prototype.getRules = function fGetRules() {
			return this.data.rules;
		}
		*/

		Profile.prototype.getRule = function fGetRule(index) {
			//return new Rule(this.data.rules[index].data);
			return this.rules[index];
		};
		
		Profile.prototype.getMaxTinks = function fGetMaxTinks(value) {
			return this.data.maxTinks;
		};
		Profile.prototype.setMaxTinks = function fSetMaxTinks(value) {
			this.data.maxTinks = value;
		};

		Profile.prototype.getIncludeImbuneTink = function fGetIncludeImbuneTink() {
			return this.data.includeImbuneTink;
		};
		Profile.prototype.setIncludeImbuneTink = function fSetIncludeImbuneTink(value) {
			this.data.includeImbuneTink = value;
		};
		
		Profile.prototype.getIcon = function fGetIcon() {
			return this.data.icon;
		};
		Profile.prototype.setIcon = function fSetIcon(value) {
			this.data.icon = value;
		};
		
		function compare_rules(ruleA, ruleB) {
			/*
			if (a.diff == b.diff) {
				if (a.szName < b.szName) return 1;
				if (a.szName > b.szName) return -1;
			} else {
				if (a.diff < b.diff) return 1;
				if (a.diff > b.diff) return -1;
			}
			*/
			//return new Rule(this.data.rules[index].data);
				
			//var ruleA = new Rule(a.data);
			//var ruleB = new Rule(b.data);
			
			var propertyIDA = ruleA.getPropertyID();
			var propertyIDB = ruleB.getPropertyID();

			var propertyA = _properties[propertyIDA];
			var propertyB = _properties[propertyIDB];
			
			if (propertyA && propertyB) {
				// Sort tinkering related properties by lowest skill. So pine tinks are evaulated before brass tinks.
				var skidA = propertyA.getTinkeringSkid();
				var skidB = propertyB.getTinkeringSkid();
				
				core.debug("propertyIDA: " + propertyIDA + ", skidA: " + skidA + ", propertyIDB: " + propertyIDB + ", skidB: " + skidB);
				
				if (skidA && skidB) {
					if (_settings.skills[skidA] > _settings.skills[skidB]) return 1;
					if (_settings.skills[skidA] < _settings.skills[skidB]) return -1;
				} else if (skidA) {
					return 1;
				} else if (skidB) {
					return -1;
				}
			} else if (!propertyA) {
				core.warn("Missing property: " + propertyIDA);
			} else if (!propertyB) {
				core.warn("Missing property: " + propertyIDB);
			}
			
			// Cantrip property has to check each spell, slightly computation.
			if (propertyIDA == "cantrip" && propertyIDB != "cantrip") {
				return 1;
			} else if (propertyIDA != "cantrip" && propertyIDB == "cantrip") {
				return -1;
			}
			
			return 0;
		}
		
		Profile.prototype.sortRules = function fSortRules() {
			// Sort rules by least computationally hard, then by lowest tink skill.
			this.rules.sort(compare_rules);
		};
		
		Profile.prototype.evaluateRulesSync = function factory() {
			return function evaluateRulesSync(params) {
				var aco = params.aco;
				var verbose = (!params || params.verbose === undefined ? core.debug : params.verbose);
				var neededTinks = 0;
				var ctink;// = aco.oai.ibi.ctink;
				var attempt = 0;
				var rules = this.rules;
				var maxTinks = this.data.maxTinks;
				
				
				var self = this;
				return rules.every(function onEvery(rule, i) {
					if (!rule.data.enabled) return true;

					var result = rule.evaluateSync({aco: aco, verbose: verbose});
					core.debug("rule #" + i + " = " + result);
					if (result == true) {
						//return {
						//	rule: rule,
						//	pass: true,
						//	neededTinks: 0
						//}

						return true;
					}

					//var attempt;
					if (typeof ctink === "undefined") {
						core.debug("Getting ctink...");
						ctink = core.getObjectValue(aco, "oai.ibi.ctink", 0);
						attempt = ctink;
					}
					
					if (self.data.includeImbuneTink == true && ctink == 0) {
						core.debug("ctink +1 for imbune");

						//ctink += 1;
						attempt = ctink + 1;

						//neededTinks += 1;
					}
					
					core.debug("Getting tinks...");
					var tinks = rule.tinksNeededAboveChance(aco, _settings.tinkChanceThreshold, attempt);

					//verbose("tinks: " + tinks);
					
					if (tinks) {
						if (tinks.achievable == true) {
							if ((neededTinks + tinks.neededTinks) > maxTinks) {
								verbose(" We need " + tinks.neededTinks + " tinks to match '" + rule.toString() + "'");
								verbose(" BUT previous rules plus this rule goes past profiles max tinks. (" + neededTinks + " + " + tinks.neededTinks + " > " + maxTinks + ")");
								return false;
							}
							
							
							verbose(" We need " + tinks.neededTinks + " tinks to match '" + rule.toString() + "'");
							attempt = tinks.attempt;
							neededTinks += tinks.neededTinks;

							//return {
							//	rule: rule,
							//	pass: true,
							//	neededTinks: neededTinks
							//}
							return true;
						} else {
							verbose(" We'd need " + tinks.neededTinks + " tinks to match '" + rule.toString() + "' (unachievable).", neededTinks);
						}
					}
				
					var property = _properties[rule.data.propertyId];
					
					
					verbose(" Failed on '" + rule.toString() + "'");
					return false;
				});
			};
		}();
		
		
		Profile.prototype.evaluateRulesAsync = (function factory() {
			return function evaluateRulesAsync(params, callback) {
				core.debug("<evaluateRulesAsync>");
				var self = this;
				
				var aco = params.aco;
				var inform = params.inform;
				var cancellation = params.cancellation || new core.Cancellation();
				var thisArg = params.thisArg;
				
				core.debug("aco: " + aco);
				
				var neededTinks = 0;
				
				var ctink;// = aco.oai.ibi.ctink;
				var attempt = 0;
				
				var r;

				var rules = this.rules;
				
				//core.debug("rules: " + rules.length);
				core.debug("Checking profile " + this.getName() + "'s " + rules.length + " rules...");
				
				var property;

				// We're looking for a rule that doesn't match our item. 
				async_js.detectSeries(rules, function each(r, callback) {
					if (cancellation && cancellation.canceled) return callback(new Error("CANCELLATION")); 
					if (!r.data.enabled) return callback();

					property = _properties[r.data.propertyId]; // r.getProperty()

					core.debug(" evaluating... " + r.toString() + "...");
					r.evaluateAsync({
						aco         : aco,
						cancellation: cancellation
					}, function onEval(err, results) {
						core.debug("  <evaluateRulesAsync|each|onEval>", err, results);
						if (err) return callback(err);
						if (cancellation && cancellation.canceled) return callback(new Error("CANCELLATION"));
						
						if (typeof results === "undefined") return callback(undefined, true); // new Error("NO_PROPERTY_VALUE")
						
						// Item matches rule, move on.
						if (results) return callback();

						//// Check tinkering
						if (aco.oai || aco._cloned) {
							onAssess();
						} else {
							core.queueAssessAco({
								aco: aco
							}, onAssess);
						}

						function onAssess(err) {
							core.debug("<evaluateRulesAsync|each|onEval|onAssess>", err);
							if (err) return callback(err);
							if (cancellation && cancellation.canceled) return callback(new Error("CANCELLATION"));
							
							if (typeof ctink === "undefined") {
								ctink = getObjectValue(aco, "oai.ibi.ctink", 0);
								attempt = ctink;
							}
							
							if (self.data.includeImbuneTink == true && ctink == 0) {
								core.debug("ctink +1 for imbune");

								//ctink += 1;
								attempt = ctink + 1;

								//neededTinks += 1;
							}
							
							
							var tinks = r.tinksNeededAboveChance(aco, _settings.tinkChanceThreshold, attempt);
							core.debug("Checking tinks, attempt: " + attempt + ", tinks: " + tinks);

							if (tinks) {
								if (tinks.achievable == true) {
									if (inform == true) {
										core.info(" We need " + tinks.neededTinks + " tinks to match '" + r.toString() + "'");
									} else {
										core.debug("We need " + tinks.neededTinks + " tinks to match " + r.toString());
									}

									//ctink = tinks.attempt;
									attempt = tinks.attempt;
									neededTinks += tinks.neededTinks;
									return callback();
								} else {
									if (inform == true) {
										core.info(" We'd need " + tinks.neededTinks + " tinks to match '" + r.toString() + "' (unachievable).");
									} else {
										core.debug("We'd need " + tinks.neededTinks + " tinks to match " + r.toString() + ".");
									}
								}
							}
						
							if (inform == true) {
								core.info(" Failed on '" + r.toString() + "'");
							} else {
								core.debug("Failed on " + r.toString());
							}
							
							callback(undefined, true);
						}
					});
					
					
					//core.debug(" Checking has value on " + property.data.name);
					/*
					if (typeof property.valueExistsAsync == "function") {
						property.valueExistsAsync(aco, onExists);
					} else if (typeof property.valueExists == "function") {
						onExists(null, property.valueExists(aco));
					} else {
						return callback(new Error("MISSING_VALUE_FUNCTION"));
					}

					function onExists(err, results) {
						if (err) return callback(err);
						if (cancellation && cancellation.canceled) return callback(new Error("CANCELLATION"));
						
						core.debug(" has value " + property.data.name + " = " + results);

						if (!results) {
							//return core.setImmediate(callback);
							return callback(undefined, true); // new Error("NO_PROPERTY_VALUE")
						}
						
						//core.console("aco: ", err, JSON.stringify(aco, null, "\t"));

						core.debug(" evaluating... " + r.toString() + "...");
							
						r.evaluateAsync({
							aco:aco,
							cancellation: cancellation
						}, function onEval(err, results) {
							core.debug("  <evaluateRulesAsync|each|onEval>", err, results);
							if (err) return callback(err);
							if (cancellation && cancellation.canceled) return callback(new Error("CANCELLATION"));
							
							// Item matches rule, move on.
							if (results) return callback();
							
							//// Check tinkering
							//callback(null, !results);
							
							////////////////////////////////////////////////////////////////
							if (aco.oai || aco._cloned) {
								onAssess();
							} else {
								core.assessAco({
									aco:aco
								}, onAssess);
							}

							function onAssess(err, results) {
								core.debug("<evaluateRulesAsync|each|onEval|onAssess>", err, results);
								if (err) return callback(err);
								if (cancellation && cancellation.canceled) return callback(new Error("CANCELLATION"));
								
								if (typeof ctink === "undefined") {
									ctink = getObjectValue(aco, "oai.ibi.ctink", 0);
									attempt = ctink;
								}
								
								if (self.data.includeImbuneTink == true && ctink == 0) {
									core.debug("ctink +1 for imbune");
									//ctink += 1;
									attempt = ctink + 1;
									//neededTinks += 1;
								}
								
								
								var tinks = r.tinksNeededAboveChance(aco, _settings.tinkChanceThreshold, attempt);
								core.debug("Checking tinks, attempt: " + attempt + ", tinks: " + tinks);

								if (tinks) {
									if (tinks.achievable == true) {
										if (inform == true) {
											core.info(" We need " + tinks.neededTinks + " tinks to match '" + r.toString() + "'");
										} else {
											core.debug("We need " + tinks.neededTinks + " tinks to match " + r.toString());
										}
										//ctink = tinks.attempt;
										attempt = tinks.attempt;
										neededTinks += tinks.neededTinks;
										return callback();
									} else {
										if (inform == true) {
											core.info(" We'd need " + tinks.neededTinks + " tinks to match '" + r.toString() + "' (unachievable).");
										} else {
											core.debug("We'd need " + tinks.neededTinks + " tinks to match " + r.toString() + ".");
										}
									}
								}
							
								if (inform == true) {
									core.info(" Failed on '" + r.toString() + "'");
								} else {
									core.debug("Failed on " + r.toString());
								}
								
								callback(undefined, true);
							};
						});
					};
					*/

				}, function finishedSeries(err, results) {
					core.debug("<evaluateRulesAsync|finishedSeries>", "err: " + err, "results: " + JSON.stringify(results));
					if (err) return callback.call(thisArg, err);
					if (cancellation && cancellation.canceled) return callback.call(thisArg, new Error("CANCELLATION")); 

					if (results) {
						return callback.call(thisArg, undefined, {
							profile     : self,
							match       : false, 
							failedOnRule: results
						});
					}
					
					var maxTinks = self.data.maxTinks;
					return callback.call(thisArg, undefined, {
						profile    : self,
						match      : (neededTinks <= maxTinks),
						neededTinks: neededTinks
					});
				});
			};
		})();
		
		Profile.prototype.getNeededTinks = function fGetNeededTinks() {
			return this.neededTinks;
		};

		Profile.prototype.setEnabled = function fSetEnabled(value) {
			this.data.enabled = value;
		};

		Profile.prototype.getEnabled = function fGetEnabled() {
			return this.data.enabled;
		};
		
		Profile.prototype.getFilePath = function fGetFilePath() {
			// Return the file path of this profile.
			return getProfileDirectory() + "\\" + this.getName() + ".json";
		};
		
		Profile.prototype.saveFile = function fSaveFile() {
			// Delete the old file if we've changed names.
			if (this.filePath && this.filePath != this.getFilePath()) {
				core.unlinkSync(this.filePath);
			}

			// Save profile data to disk.
			core.saveJsonFile({
				path: this.getFilePath(),
				data: this.data
			});

			this.filePath = this.getFilePath();
			core.matchingProfiles = {};
			core.emit("onProfileSaved", this);
		};

		Profile.prototype.deleteFile = function fDeleteFile() {
			// Delete this profile from disk.
			core.unlinkSync(this.getFilePath());
			core.matchingProfiles = {};
			core.emit("onProfileDeleted", this);
		};
		
		// Compare two items and return the one that best matches the rules.
		Profile.prototype.compare = function compare(a, b) {
			var rules = this.rules;
			var rule, aMatch, bMatch, property, aValue, bValue;
			for (var i = 0; i < rules.length; i++) {
				rule = rules[i];
				aMatch = rule.evaluateSync({aco: a});
				bMatch = rule.evaluateSync({aco: b});
				if (aMatch && !bMatch) return -1;
				if (!aMatch && bMatch) return 1;
				property = _properties[rule.data.propertyId];
				if (rule.data.operator == ">" || rule.data.operator == ">=" || rule.data.operator == "<" || rule.data.operator == "<=") {
					aValue = property.getValue(a);
					bValue = property.getValue(b);
					if (rule.data.operator == ">" || rule.data.operator == ">=") {
						if (aValue > bValue) return -1;
						if (aValue < bValue) return 1;
					} else if (rule.data.operator == "<" || rule.data.operator == "<=") {
						if (aValue < bValue) return -1;
						if (aValue > bValue) return 1;
					}
				}
			}
			return 0;
		};
	})();
	
	/*
	var ACColours = {};
	function ACColour(id, rgb, name) {
		var _id		= id;
		var _rgb	= rgb;
		var _name	= name;
		function fGetID() {
			return _id;
		}
		function fGetRGB() {
			return _rgb;
		}
		function fGetName() {
			return _name;
		}
		return {
			getID	: fGetID,
			getRGB	: fGetRGB,
			getName	: fGetName
		};
	};
	*/
	
	function LoadColourTableXML() {
		/*
			Load our decal colour system file (2112_dcs.xml) to get the RGB colours.
		*/
		var gFile;
		try {
			gFile = new ActiveXObject("MSXML2.DOMDocument.4.0");
		} catch (e) {
			skapi.Outputsz("Error initializing XML object. You may not have the current ActiveScripting engine.\n");
			skapi.Outputsz("Actual error message: " + e + "\n");
			return;
		}
		
		gFile.async = false;
		gFile.resolveExternals = false;
		var colourPath = _dir + "src\\data\\2012_dcs.xml";

		//	core.console("colourPath: " + colourPath);
		gFile.load(colourPath);

		var err = gFile.parseError;
		if (err.errorCode != 0) {
			skapi.Outputsz("   No 2012_dcs.xml available or error parsing file.  This file has possibly been corrupted.\n");
			return;
		}
		var colortable;
		try {
			colortable = gFile.documentElement.getElementsByTagName('ac_color');
		} catch (e) {
			skapi.Outputsz("Unable to load colour table.\n");
			skapi.Outputsz("Actual error message: " + e + "\n");
			return;
		}

		var palettes = {};

		// <ac_color id="1077" rgb="0E6DA7" name="OpenSkyBlue"/>
		var c;
		var id, rgb, name;
		for (i = 0; i < colortable.length; i++) {
			c		= colortable[i];//.getAttribute("name");
			id		= c.getAttribute("id");
			rgb		= c.getAttribute("rgb");
			name	= c.getAttribute("name");
			
			palettes[id] = {
				rgb : rgb,
				name: name
			};

			//ACColours[id] = ACColour(id, rgb, name);
		}
		
		core.saveJsonFile({
			path: "src\\data\\colourIds.json",
			data: palettes
		});
		core.info("saved colour ids.");
	}
	
	core.colourIds = {};
	function LoadColourTable() {
		var colourIds = core.colourIds = core.loadJsonFile({path: _dir + "src\\data\\colourIds.json"}) || {};
		
		var count = Object.keys(colourIds).length;
		core.info("Loaded " + count + " palette ids.");
	}

	var detectedAcos = [];
	function fAddDetectedAco(aco) {
		//lstDetected.addRow(aco.oid, (0x06000000 + aco.icon), core.getFullName(aco));
		
		for (var i = (detectedAcos.length - 1); i >= 0; i--) {
			if (detectedAcos[i] == aco) {
				return;
			}
		}
		
		detectedAcos.push(aco);
		populateDetectedList = true;
	}
	
	function fCullDetectedItems() {
		/******************************************************************************\
			fCullDetectedItems: Cull items from detected list.
		\******************************************************************************/
		//debug("<fCullDetectedItems>");
		var aco;
		for (var i = (detectedAcos.length - 1); i >= 0; i--) {
			aco = detectedAcos[i];
			if (!aco || !aco.fExists || (aco.maploc && skapi.maplocCur.Dist3DToMaploc(aco.maploc) > 2)) {
				detectedAcos.splice(i, 1);
			}
		}
	}

	function fRemoveDetectedOid(oid) {
		core.debug("<fRemoveDetectedAco> " + oid);

		var rows = lstDetected.rows;
		var row;
		for (var i = rows.length - 1; i >= 0; i--) {
			row = rows[i];
			if (row[0] == oid) {
				lstDetected.deleteRow(i);
				break;
			}
		}
	}
	
	function fRemoveDetectedAcoContainer(acoContainer) {
		if (!acoContainer.coacoContents) return;
		var items = core.coacoToArray(acoContainer.coacoContents);
		var aco;
		var containerItems;
		for (var i = items.length - 1; i >= 0; i--) {
			aco = items[i];
			if (aco.oty & otyContainer) {
				items.splice(i, 1);
				if (!aco.coacoContents) continue;
				containerItems = core.coacoToArray(aco.coacoContents);
				items = items.concat(containerItems);
			}
		}
		core.debug("items: " + items.length);
		
		var oids = [];
		for (var i = 0; i < items.length; i++) {
			oids.push(items[i].oid);
		}

		var rows = lstDetected.rows;
		core.debug("rows: " + rows.length);
		var row, oid;
		
		for (var i = rows.length - 1; i >= 0; i--) {
			row = rows[i];
			oid = row[0];
			if (oids.indexOf(oid) > 0) {
				lstDetected.deleteRow(i);
			}
		}
	}

	function getParentContainer(aco) {
		var acoParentContainer = aco;
		while (acoParentContainer.acoContainer) {
			acoParentContainer = acoParentContainer.acoContainer;
		}
		return acoParentContainer;
	}

	core.getDateString = function getDateString(includeMS) {
		var now = new Date();
		
		var day = now.getDate();
		if (day < 10) {
			day = "0" + day;
		}
		
		var month = now.getMonth() + 1;
		if (month < 10) {
			month = "0" + month;
		}
		
		var hour = now.getHours();
		if (hour < 10) {
			hour = "0" + hour;
		}
		
		var sec = now.getSeconds();
		if (sec < 10) {
			sec = "0" + sec;
		}
		
		var min = now.getMinutes();
		if (min < 10) {
			min = "0" + min;
		}
		
		var nowString = now.getFullYear();
		nowString += "-" + month;
		nowString += "-" + day;
		nowString += "_" + hour;
		nowString += "-" + min;// I would use : but I use this for file names as well.
		nowString += "-" + sec;

		if (includeMS == true) {
			var ms = "" + now.getMilliseconds();
			while (ms.length < 3) {
				ms = "0" + ms;
			}
			nowString += "." + ms;
		}
		
		return nowString;
	};
	
	core.getCantripName = function getCantripName(name) {
		if (name.match(/(Minor|Major|Epic|Legendary) (.*)/)) {
			return RegExp.$2;
		}
		return name;
	};

	//core.on("onTick", function tick() {});

	core.main = function main() {
		core.initialize();
		core.enable();

		//core.disable();
		//core.enable();
		while (core._script.enabledState == true) {
			core.tick();
			skapi.WaitEvent(1000, wemFullTimeout);
			if (skapi.cmsecQueueDelay > 60 * 1000) {
				// No events from server in 60 seconds. Lagging out?
				core.disable();
			} else if (skapi.cmsecQueueDelay > 10000) {
				// No events from server in over 5 seconds.
				core.warn("cmsecQueueDelay is " + skapi.cmsecQueueDelay + "!");
			}
		}

		skapi.OutputLine("console.StopScript()", opmConsole);
		console.StopScript();
	};

	core.getWorkmanshipSync = function() {
		return function getWorkmanshipSync(params) {
			var aco = params.aco;
			if (aco.workmanship && isFinite(aco.workmanship)) {
				return aco.workmanship;
			}
			if (typeof aco["oai.ibi.raw.65641"] !== "undefined") {
				return aco["oai.ibi.raw.65641"];
			} else if (typeof aco["oai.ibi.raw.10069"] !== "undefined") {
				return aco["oai.ibi.raw.10069"];
			}
			if (aco.oai && aco.oai.ibi) {
				return aco.oai.ibi.raw.Item(0x10069);
			}
		};
	}();

	/*
	// GDLe shows some aco.workmanship as being Infinity, so assess and get the raw value.
	core.getWorkmanship = (function() {
		function onAssess(err) {
			//core.console("<getWorkmanship|onAssess>", this.aco, results);
			var aco = this.aco;
			if (aco.oai && aco.oai.ibi) {
				this.callback.call(this.thisArg, undefined, aco.oai.ibi.raw.Item(0x10069) );
			} else {
				this.callback.call(this.thisArg);
			}
			//this.callback = noop;
		};
		return function getWorkmanship(params, callback) {
			
			var aco = params.aco;
			
			//core.console("<getWorkmanship>", aco && aco.szName);
			//core.console(" aco.workmanship: ", aco && aco.workmanship);
			//core.console(" typeof " + (typeof aco.workmanship));
			
			if (aco.workmanship && isFinite(aco.workmanship)) {
				//core.console(" Returning aco.workmanship");
				return core.setImmediate(core.applyMethod, callback, (params && params.thisArg), [undefined, aco.workmanship]);
			}

			// Check for flattened object.
			if (typeof aco["oai.ibi.raw.65641"] !== "undefined") {
				var rawWork = aco["oai.ibi.raw.65641"];
				//core.console("rawWork: " + rawWork);
				return core.setImmediate(core.applyMethod, callback, (params && params.thisArg), [undefined, rawWork]);
			}
			
			
			var thisArg = {
				callback:callback, 
				aco:aco, 
				thisArg:params.thisArg
			};
		
			if (aco.oai || aco._cloned) {
				//core.console(" item already assessed.");
				onAssess.call(thisArg);
			} else {
				//core.console(" Assessing " + aco.szName + "...");
				core.queueAssessAco({
					aco:aco, 
					thisArg:thisArg
				}, onAssess);
			}
		};
	})();
	*/
	
	/*
	core.WaitEvent.getWorkmanshipSync = function getWorkmanshipSync(params) {
		core.debug("<getWorkmanshipSync>");
		var aco = params.aco;
		
		if (aco.workmanship && isFinite(aco.workmanship)) {
			return aco.workmanship;
		}
		
		if (typeof aco["oai.ibi.raw.65641"] !== "undefined") {
			return aco["oai.ibi.raw.65641"];
		}
		
		if (aco.oai) {
			if (aco.oai.ibi) {
				return aco.oai.ibi.raw.Item(0x10069);
			} else {
				return;
			}
		}
		
		// Fallback to SkunkSynchronous. This may cause a WaitEvent to fire within a WaitEvent. :(
		var operation = core.synchronous({method:core.getWorkmanship, args:[params]});
		//var err = operation.results.length > 0 && operation.results[0];
		var workmanship = operation.results.length > 1 && operation.results[1];
		return workmanship;
	};
	*/
	
	function isArray(arr) {
		return Object.prototype.toString.call(arr) == '[object Array]';
	}
	
	core.applyMethod = function applyMethod() {
		var args = [].slice.call(arguments);
		var thisArg;
		if (typeof args[0] !== "function") {
			thisArg = args.splice(0, 1)[0];
		}
		var method = args.splice(0, 1)[0];
		var methodArgs = args.splice(0, 1)[0];
		return method && method.apply(thisArg, methodArgs);
	};
	
	core.getAcoSpells = function getAcoSpells(params) {
		var aco = params.aco;
		var spells = [];
		if (typeof aco["oai.iei.cspellid"] !== "undefined") {
			for (var key in aco)  {
				if (!aco.hasOwnProperty(key)) continue;
				if (key.substring(0, 18) == "oai.iei.rgspellid.") {
					spells.push(Number(key.substring(18, key.length)));
				}
			}
		} else {
			if (aco.oai && aco.oai.iei && aco.oai.iei.cspellid > 0) {
				for (var i = 0; i < aco.oai.iei.cspellid; i++) {
					spells.push(aco.oai.iei.rgspellid(i));
				}
			}
		}
		return spells;
	};
	
	/**
		* loadJsonFile() Get the contents of a text file that is formatted as json.
	 *
	 * @param {object} params: Collection of arguments.
	 * @param {string} params.path: File path to the file.
	 * @return {variable} Contents of the file.
	 */
	core.loadJsonFile = function loadJsonFile(params) {
		if (core.existsSync(params.path)) {
			var json = core.readFileSync(params.path);
			var items;
			try {
				items = JSON.parse(json);
			} catch (e) {
				return;
			}
			return items;
		}
	};
	
	/**
	 * saveJsonFile() Stringify a object and save it to a text file.
	 *
	 * @param {object} params: Collection of arguments.
	 * @param {string} params.path: File path to the file.
	 * @param {variable} params.data: Data to stringify.
	 * @return {variable} Contents of the file.
	 */
	core.saveJsonFile = function saveJsonFile(params) {
		var path = params.path;
		var data = params.data;
		var json = JSON.stringify(data, null, "\t");
		if (!core.existsSync(params.path)) {
			core.touchSync(params.path);
		}
		core.writeFileSync(params.path, json);
	};
	
	core.clone = function clone(obj) {
		// Clone a object.
		var copy;

		// Handle the 3 simple types, and null or undefined
		if (null == obj || "object" != typeof obj) return obj;

		// Handle Date
		if (obj instanceof Date) {
			copy = new Date();
			copy.setTime(obj.getTime());
			return copy;
		}

		// Handle Array
		if (obj instanceof Array) {
			copy = [];
			for (var i = 0, len = obj.length; i < len; i++) {
				copy[i] = fClone(obj[i]);
			}
			return copy;
		}

		// Handle Object
		if (obj instanceof Object) {
			copy = {};
			for (var attr in obj) {
				if (obj.hasOwnProperty(attr)) copy[attr] = fClone(obj[attr]);
			}
			return copy;
		}

		throw new Error("Unable to copy obj! Its type isn't supported.");
	};
	
	/**
	 * coacoToArray() Convert Skunkworks coaco object to javascript array.
	 *
	 * @param {object} coaco: CoAco object.
	 * @return {array} list of items in the coaco.
	 */
	core.coacoToArray = function coacoToArray(coaco) {
		var arr = [];
		for (var i = 0; i < coaco.Count; i++) {
			arr.push(coaco.Item(i));
		}
		return arr;
	};
	
	core.scanItems = (function factory() {
		return function scanItems(params, callback) {
			core.debug("<scanItems>");
			var logger = (!params || params.logger === undefined ? core.debug : params.logger);
			var items = params.items;
			var itemCallback = params.itemCallback;

			//var stopMethod = params.stopMethod;
			//var stop = false;
			//var cancellation = params.cancellation || new core.Cancellation();
			
			//mapValuesSeries
			var index = 0;
			async_js.mapSeries(items, function process(aco, callback) {
				//if (cancellation && cancellation.canceled) return callback(new Error("CANCELLATION")); 
				logger("<scanItems|process>", aco);

				if (aco.oai || aco._cloned) {
					onAssess();
				} else {
					logger("Assessing " + aco.szName + "...");
					core.assessAco({
						aco: aco
					}, onAssess);
				}

				function onAssess(err) {
					if (err) {
						logger("Error during assess", err);
						return callback(err);
					}
					
					logger("Getting profile matches...");
					var matches = core.getMatchingProfilesSync({aco: aco});
					logger(matches.length + " matches.");
					
					var payload = {
						index   : index++,
						aco     : aco,
						profiles: matches
					};
					if (typeof itemCallback === "function") {
						itemCallback(err, payload);
					}
					
					callback(undefined, payload);
				}
			}, callback);
		};
	})();

	core.scanContainer = (function factory() {
		return function scanContainer(params, callback) {
			var logger = (!params || params.logger === undefined ? core.debug : params.logger);
			logger("<scanContainer>");
			
			var acoContainer = params.acoContainer;
			var thisArg = params.thisArg;
			
			var items = core.coacoToArray(acoContainer.coacoContents);
			core.debug("1 items: " + items.length);
			
			var aco;
			var containerItems;
			for (var i = items.length - 1; i >= 0; i--) {
				aco = items[i];
				if (aco.oty & otyContainer) {
					items.splice(i, 1);
					containerItems = core.coacoToArray(aco.coacoContents);
					items = items.concat(containerItems);
				}
			}

			core.debug("2 items: " + items.length);
			if (items.length == 0) return core.setImmediate(callback, undefined, []);
				
			//callback = core.once(callback);
			
			var cancellation = params.cancellation || new core.Cancellation();

			var handler = {};
			if (params.monitorClosed == true) {
				handler.OnCloseContainer = function OnCloseContainer(acoContainer2) {
					if (acoContainer2.oid == acoContainer.oid) {
						//stop = true;
						core.removeHandler(evidNil, handler);
						if (cancellation) cancellation.canceled = true;
						callback(new Error("CONTAINER_CLOSED"));
					}
				};
				core.addHandler(evidOnCloseContainer, handler);
			}

			core.info("Scanning " + acoContainer.szName + "'s " + items.length + " items...");
			
			/*
			var queue = async_js.queue(function(aco, callback) {
				logger("Assessing " + aco.szName + "...");
				core.assessAco({
					aco: aco
				}, callback);
			}, 1);
			
			queue.error(function(err, task) {
				core.info('task experienced an error');
			});
			
			queue.drain(function() {
				core.info('all items have been processed');
			});
			
			
			items.forEach(function(aco) {
				queue.push(aco);
			});
			*/
			
			/*
			for (var key in queue)  {
				//if (!queue.hasOwnProperty(key)) continue;
				logger("key: " + key, typeof queue[key], queue.hasOwnProperty(key));
				
			}
			*/
			
			
			/**/
			prgAssessProgress.setValue(100); 
			lstDetected.clear();

			core.scanItems({
				items       : items,
				itemCallback: function itemCallback(err, results) {
					core.debug("<itemCallback>");

					core.console("results: " + JSON.stringify(results, undefined, "\t"));

					//core.console(results.index + ", " + JSON.stringify(results, null, "\t"));
					var progress = round(((results.index + 1) / items.length) * 100);
					prgAssessProgress.setValue(progress); 
					
					if (results.profiles.length > 0) {
						var aco = results.aco;
						
						
						
						var name;
						var tinks;
						var profile;
						for (var i = 0; i < results.profiles.length; i++) {
							name = results.profiles[i]; //.profile;
							core.info(" " + core.getFullName(aco) + " matches " + name  + ".");
							/*
							profile = core.getProfileByName(name);
							
							core.info("p: " + typeof p);
							
							tinks = results.profiles[i].neededTinks; //p.getNeededTinks();
							if (tinks && tinks > 0) {
								core.info(" " + core.getFullName(aco) + " matches " + name + " (" + tinks + " tinks).");
							} else {
								core.info(" " + core.getFullName(aco) + " matches " + name  + ".");
							}
							*/
						}

						lstDetected.addRow(aco.oid, (0x06000000 + aco.icon), core.getFullName(aco));
					}
				},
				cancellation: cancellation
			}, function onScanItems(err, results) {
				core.debug("<onScanItems>", err, results);
				core.removeHandler(evidNil, handler);
				prgAssessProgress.setValue(100); 
				
				if (err) {
					logger("Error during scan", err);
					return callback.call(thisArg, err);
				}
				if (cancellation && cancellation.canceled) return callback.call(thisArg, new Error("CANCELLATION")); 
				
				callback.call(thisArg, err, results);
			});
			
		};
	})();
	
	/**
	 * getObjectValue() Get object value at a given path. Supports normal and flattened aco objects.
	 *
	 * @param {aco} target: aco object.
	 * @param {string} path: Variable path.
	 * @return {variable} Value from the path.
	 */
	core.getObjectValue = function getObjectValue(target, path, def) {
		if (typeof target[path] !== "undefined") {
			return target[path];
		}
		
		var segs = path.split(".");
		var len = segs.length;
		var idx = 0;
		
		var value;
		do {
			var prop = segs[idx];
			if (typeof prop === 'number') {
				prop = String(prop);
			}
			
			try {
				value = target[prop];
			} catch (e) {
				break;
			}

			if (typeof prop === "undefined") break;
			
			target = target[prop];
		} while (++idx < len && typeof target !== "undefined");
		
		if (idx === len) {
			return target;
		}
		
		return def;
	};
		
	core.getInventoryItems = function getInventoryItems() {
		var items = [];
		var acf = skapi.AcfNew();

		//acf.olc = olcInventory;
		acf.olc = olcInventory | olcEquipped;
		var coaco = acf.CoacoGet();
		
		for (var i = 0; i < coaco.Count; i++) {
			aco = coaco.Item(i);
			if (!core.AcoBelongsToMe(aco)) continue;
			items.push(aco);
		}
		
		return items;
	};
	
	/*
	core.getFlattnedAcos = (function factory() {
		return function getFlattnedAcos(params, callback) {

			core.debug("<getFlattnedAcos>");
			
			var items = params.items;
			//items.splice(3, items.length);
			
			var percentages = {};
			var index = 0;
			var started = new Date().getTime();
			async_js.mapSeries(items, function each(aco, callback) {
				
				if (aco.oai) {
					return onAssess();
				} else {
					return core.queueAssessAco({aco:aco}, onAssess);
				}

				function onAssess(err) {
					if (err) return callback(err);
					var cloned = core.cloneAco({aco:aco});
					//core.console("<onAssess>" + key, aco); // JSON.stringify(cloned, null, "\t")
					
					var elapsed = new Date().getTime() - started;
					var perItem = elapsed / (index+1);
					var remaining = items.length - (index+1);
					
					var percent = round((index / items.length) * 100);
					if ((percent % 10 == 0) && !percentages[percent]) {
						core.info("Scanned " + index + "/" + items.length + " ("+percent+"%) items. eta: " + core.getElapsedString((perItem*remaining) / 1000));
						percentages[percent] = true;
					}
					
					index++;
					callback(undefined, cloned)
				};

			}, callback);
		};
	})();
	*/
	
	core.saveInventory = function saveInventory(params, callback) {
		//var items = core.getInventoryItems();
		//core.debug("items: " + items.length);
		//var thisArg = params && params.thisArg;
		var inventory = core.getInventory();
		core.info("Assessing " + inventory.length + " items...");
		async_js.mapSeries(inventory, function _each(aco, callback) {
			if (aco.oai) return callback(undefined, aco);
			
			var cache = core.itemStats.get(aco.oid);
			if (cache) return callback(undefined, cache);
			
			core.getAssessedAco({aco: aco}, callback);
		}, function onAssessed(err, results) {
			if (err) return callback(err);
			var flattened = results.map(function _flattenAco(aco) {
				return core.flattenAco({aco: aco});
			});

			flattened.forEach(function(flat) {
				if (!core.itemStats.equals(flat.oid, flat)) {
					core.itemStats.set(flat.oid, flat);
				}
			});
			core.itemStats.saveIfChanged();

			core.saveJsonFile({
				path: _dir + "src/data/worlds/" + skapi.szWorld + "/characters/" + skapi.acoChar.szName + "/inventory.json",
				data: flattened.map(function(flat) {
					return flat.oid;
				})
			});

			/*
			core.inventory.clear();
			flattened.forEach(_setItem);
			core.inventory.save();
			*/
			core.info("Saved inventory.");
			callback(undefined, true);
		});
	};

	/**
	 * getElapsedString() Convert seconds to a more human readable string.
	 *
	 * @param {number} seconds: Seconds
	 * @return {string} Time string.
	 */
	core.getElapsedString = function getElapsedString(seconds) {
		//var numyears = Math.floor(seconds / 31536000);
		var numdays = Math.floor((seconds % 31536000) / 86400); 
		var numhours = Math.floor(((seconds % 31536000) % 86400) / 3600);
		var numminutes = Math.floor((((seconds % 31536000) % 86400) % 3600) / 60);
		var numseconds = (((seconds % 31536000) % 86400) % 3600) % 60;
		
		numseconds = Math.max(numseconds, 1);
		
		var string = core.round(numseconds) + "s";
		
		if (numminutes > 0) {
			string = numminutes + "m " + string;
		}
		if (numhours > 0) {
			string = numhours + "h " + string;
		}
		if (numdays > 0) {
			string = numdays + "d " + string;
		}
		
		return string;
	};
	
	/*
	core.launchInventorySearchOld = (function factory() {
		
		var me = skapi.acoChar.szName;
		function compareResults(a, b) {
			// Lowest tinks first.
			if (a.neededTinks < b.neededTinks) return -1;
			if (a.neededTinks > b.neededTinks) return 1;
			
			// Me first.
			if (a.characterName == me && b.characterName != me) return -1;
			if (a.characterName != me && b.characterName == me) return 1;
			
			// Sort by equipment slot
			if (a.aco.eqm < b.aco.eqm) return -1;
			if (a.aco.eqm > b.aco.eqm) return 1;

			// Merchant catatory
			if (a.aco.mcm < b.aco.mcm) return -1;
			if (a.aco.mcm > b.aco.mcm) return 1;

			// Workmanship
			var aW = core.getWorkmanshipSync({aco: a.aco});
			var bW = core.getWorkmanshipSync({aco: b.aco});
			if (aW && bW) {
				if (aW < bW) return -1;
				if (aW > bW) return 1;
			}
			
			// Value
			if (a.aco.cpyValue < b.aco.cpyValue) return -1;
			if (a.aco.cpyValue > b.aco.cpyValue) return 1;

			// Name
			if (core.getFullName(a.aco) < core.getFullName(b.aco)) return -1;
			if (core.getFullName(a.aco) > core.getFullName(b.aco)) return 1;
			
			return 0;
		}
		
		return function launchInventorySearchOld(params, callback) {
			var folderPath = _dir + "src\\data\\inventory\\" + skapi.szWorld;
			var stats = core.statSync(folderPath);

			//core.console("folder stats:" + JSON.stringify(stats, null, "\t"));
			if (!stats) {
				var err = new Error("NO_CHARACTER_SAVES");
				err.folderPath = folderPath;
				return core.setImmediate(callback, err);
			}
			
			
			var view = core.createView({
				title : "Inventory Search", 
				width : 300, 
				height: 300
			});
		
			var layout = new SkunkSchema.FixedLayout({
				parent: view
			});

			var choCharacter = new SkunkSchema.Choice({
				parent: layout, 
				name  : "choCharacter"
			})
				.setWidth(150)
				.addOption({text: "All Characters"});

			//.on("onControlEvent", onControlEvent);

			var characterFiles = core.readdirSync(folderPath);

			//core.console("files:" + JSON.stringify(files, null, "\t"));
			var path;
			for (var i = 0; i < characterFiles.length; i++) {
				path = characterFiles[i];
				stats = core.statSync(path);

				//core.console("file stats:" + JSON.stringify(stats, null, "\t"));
				if (!stats) continue;
				choCharacter.addOption({text: stats.BaseName, data: stats.Path});
			}
			
			var choProfile = new SkunkSchema.Choice({
				parent: layout, 
				name  : "choProfile"
			})
				.setAnchor(choCharacter, "TOPRIGHT")
				.setWidth(150)
				.addOption({text: "Profile..."})
				.on("onControlEvent", onControlEvent);

			var p;
			for (var i = 0; i < _loadedProfiles.length; i++) {
				p = _loadedProfiles[i];
				choProfile.addOption({text: p.getName()});
			}
			
			
			var lstResults = new SkunkSchema.List({
				parent: layout, 
				name  : "lstResults"
			})
				.setAnchor(choCharacter, "BOTTOMLEFT")
				.setWidth(view.getWidth())
				.setHeight(view.getHeight() - 40)
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 0}) // oid
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 125}) // character
				.addColumn({progid: "DecalControls.IconColumn"}) // item icon
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 125}) // item name
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 30}) // Needed tinks
				.on("onControlEvent", onControlEvent);

			// Footer.
			var btnExit = new SkunkSchema.PushButton({
				parent: layout, 
				name  : "btnExit", 
				text  : "Exit"
			})
				.setAnchor(layout, "BOTTOMRIGHT", "BOTTOMRIGHT")
				.setWidth(30)
				.on("onControlEvent", onControlEvent);
			
			function onControlEvent(control, szPanel, szControl, value) {
				if (szControl == "btnExit") {
					view.removeControls();
					callback();
					return;
				} else if (szControl == "choProfile") {
					if (value == "Profile...") return;
					var cv = choCharacter.getValue();
					core.debug("value: " + value + ", cv: " + cv);
					
					var profile;
					var p;
					for (var i = 0; i < _loadedProfiles.length; i++) {
						p = _loadedProfiles[i];
						if (p.getName() == value) {
							profile = p;
							break;
						}
					}
					if (!profile) {
						core.warn("Profile '" + value + "' is missing!");
						return;
					}
					
					var characterPaths = [];
					var path;
					for (var i = 0; i < characterFiles.length; i++) {
						path = characterFiles[i];
						if (cv == "All Characters" || path == cv) {
							characterPaths.push(path);
						}
					}
					
					
					lstResults.clear();
					core.info("Searching " + characterPaths.length + " characters for items matching " + profile.getName() + ".");
					var started = new Date().getTime();
					checkCharacters({
						profile       : profile,
						characterPaths: characterPaths
					}, function onCharacters(err, results) {
						if (err) return dumpException(err);
						
						
						var elapsed = new Date().getTime() - started;
						core.info("Completed search after " + core.numberWithCommas(elapsed) + "ms. Scanned " + results.totalItems + " items, found " + results.length + " matches.");

						//core.console("<onCharacters>", err, results && results.length); //JSON.stringify(results, null, "\t")

						results.sort(compareResults);

						var rows = [];
						var r, aco, characterName, neededTinks;
						for (var i = 0; i < results.length; i++) {
							r = results[i];
							aco = r.aco;
							characterName = r.characterName;
							var neededTinks = r.neededTinks;
							rows.push([aco.oid, characterName, (0x06000000 + aco.icon), core.getFullName(aco), neededTinks]);

							//core.info(i,aco.szName, neededTinks);
						}
						
						//lstResults.clear();
						lstResults.setRows(rows);
					});
					
					return;
				} else if (szControl == "lstResults") {
					var intCol = parseInt(value.split(",")[0]);
					var intRow = parseInt(value.split(",")[1]);
					
					var row = control.rows[intRow];
					var oid = row[0];
					var aco = skapi.AcoFromOid(oid);
					if (!aco || !aco.fExists) {
						core.warn("Item isn't in client memory anymore.");
						return;
					}
					
					core.info("Selecting " + aco.szName + "...");
					skapi.SelectAco(aco);
					return;
				}

				core.console("uncaught onControlEvent", szPanel, szControl, value);
			}

			function onMatch(params) {
				var characterName = params.characterName;
				var aco = params.aco;
				var neededTinks = params.neededTinks;

				//core.info("<onMatch>", characterName, aco.szName);
				
				lstResults.addRow(aco.oid, characterName, (0x06000000 + aco.icon), core.getFullName(aco), neededTinks);
			}

			function checkCharacters(params, callback) {
				var profile = params.profile;
				var characterPaths = params.characterPaths;
				
				core.debug("characterPaths:" + characterPaths.length);
				
				var totalItems = 0;
				async_js.concatSeries(characterPaths, function each(path, callback) {
					core.debug("path: " + path);
					
					var stats = core.statSync(path);
					var start = new Date().getTime();
					var results = SkunkDB.select().from(path)
						.execute();
					var elapsed = new Date().getTime() - start;
					core.debug("DB results:" + results.rows.length + ", elapsed: " + elapsed);
					var items = results.rows;
					totalItems += items.length;
					
					checkItems({
						profile      : profile,
						items        : items,
						characterName: stats.BaseName
					}, callback);
				}, function onCharacters(err, results) {
					results.totalItems = totalItems;
					callback(err, results);
				});
			}

			function checkItems(params, callback) {
				var profile = params.profile;
				var items = params.items;
				var characterName = params.characterName;

				//items.splice(1, items.length);
				core.info("Scanning " + characterName + "'s " + items.length + " items...");
				
				var index = 0;
				var percentages = {0: true};
				async_js.concatSeries(items, function each(aco, callback) {
					//core.console("aco", JSON.stringify(aco, null, "\t"));
					
					index++;
					
					profile.evaluateRulesAsync({
						aco   : aco, 
						inform: false
					}, onEvaleRules);

					function onEvaleRules(err, results) {
						if (err) return callback(err);

						var percent = round((index / items.length) * 100);

						//core.debug("cur: " + index + ", max: " + items.length + ", percent: " + percent);
						
						if ((percent % 25 == 0) && !percentages[percent]) {
							core.debug("Scanned " + index + "/" + items.length + " (" + percent + "%) items.");
							percentages[percent] = true;
						}
						
						core.debug(aco.szName + ", match: " + results.match);
						
						var size = core.getCallStackSize();

						//core.info("inv search stack size: " + size);
						
						if (results.match) {
							var payload = {
								characterName: characterName,
								aco          : aco,
								neededTinks  : results.neededTinks
							};
							
							onMatch(payload);

							if (size < maxCallSize) return callback(null, payload);
							return core.setImmediate(callback, null, payload);
						} else {
							if (size < maxCallSize) return callback();
							return core.setImmediate(callback);
						}
					}
				}, callback);
			}

			view.showControls(true);
		};
	})();
	*/

	// Object passed between multiple async functions to indicate that we want to cancel a current process.
	core.Cancellation = (function factory() {
		function Cancellation() {
			//EventEmitter.apply(this);
			this.canceled = false;
		}

		//Cancellation.prototype = new EventEmitter();
		//Cancellation.prototype.constructor = Cancellation;
		/*
		Cancellation.prototype.cancel = function cancel(value) {
			if (this.canceled) return;
			this.canceled = true;
			//this.emit("canceled", value);
			//delete this.events;
			return this;
		};
		*/
		return Cancellation;
	})();
	
	core.once = function once(fn) {
		var called, value;

		if (typeof fn !== 'function') {
			throw new Error('expected a function but got ' + fn);
		}

		return function wrap() {
			if (called) {
				return value;
			}
			called = true;
			value = fn.apply(this, arguments);
			return value;
		};
	};
	
	core.numberWithCommas = function numberWithCommas(x) {
		var parts = x.toString().split(".");
		parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		return parts.join(".");
	};
	
	function onDisappeared(oid) {
		core.debug("<onDisappeared>", oid);
		fRemoveDetectedOid(oid);
		
		//if (data.palettes[oid] !== "undefined") {
		delete data.palettes[oid];

		//}
		
		delete core.matchingProfiles[oid];
	}

	var arrayContains = Array.prototype.indexOf ?
		function(arr, val) {
			return arr.indexOf(val) > -1;
		} :
		function(arr, val) {
			for (var i = 0, len = arr.length; i < len; ++i) {
				if (arr[i] === val) {
					return true;
				}
			}
			return false;
		};

	core.getCallStackSize = function getCallStackSize() {
		var count = 0, fn = arguments.callee, functionsSeen = [fn];

		while ((fn = fn.caller) && !arrayContains(functionsSeen, fn)) {
			functionsSeen.push(fn);
			count++;
		}

		return count;
	};
	
	core.flattenAco = function flattenAco(params) {
		var aco = params.aco;
		
		// Object was already flattened. I'd rename this to _flattened but LP3 already uses _cloned.
		if (aco._cloned) return aco;
		if (aco.time) return aco;
		
		var o = {};
		o.time              = new Date().getTime();
		o.oid				= aco.oid;
		o.szName			= aco.szName;
		o.oty				= aco.oty;
		o.ocm				= aco.ocm;
		o.olc				= aco.olc;
		o.mcm				= aco.mcm;
		o.eqm				= aco.eqm;
		o.cpyValue			= aco.cpyValue;
		o.material			= aco.material;
		o.workmanship		= aco.workmanship;
		o.citemStack		= aco.citemStack;
		o.citemMaxStack		= aco.citemMaxStack;
		o.cuseLeft			= aco.cuseLeft;
		o.cuseMax			= aco.cuseMax;
		o.burden			= aco.burden;
		o.iitem				= aco.iitem;
		o.eqmWearer			= aco.eqmWearer;
		o.spellid			= aco.spellid;
		o.citemMax			= aco.citemMax;
		o.citemContents		= aco.citemContents;
		o.icon				= aco.icon;
		o.iconOverlay		= aco.iconOverlay;
		o.iconUnderlay		= aco.iconUnderlay;
		
		
		if (aco.oai != null) {// Check assessed info.
			o["oai.time"] = new Date(new Date().getTime() - aco.oai.cmsecSinceAssess).getTime();

			if (aco.oai.cbi != null) { 	// Creature Basic Information 
				o["oai.cbi.species"]    = aco.oai.cbi.species;
				o["oai.cbi.lvl"]        = aco.oai.cbi.lvl;
				o["oai.cbi.healthCur"]  = aco.oai.cbi.healthCur;
				o["oai.cbi.healthMax"]  = aco.oai.cbi.healthMax;
			}
			
			if (aco.oai.cai != null) {	// Creature Attribute Information
				o["oai.cai.strength"] = aco.oai.cai.strength;
				o["oai.cai.endurance"] = aco.oai.cai.endurance;
				o["oai.cai.quickness"] = aco.oai.cai.quickness;
				o["oai.cai.coordination"] = aco.oai.cai.coordination;
				o["oai.cai.focus"] = aco.oai.cai.focus;
				o["oai.cai.self"] = aco.oai.cai.self;
				o["oai.cai.staminaCur"] = aco.oai.cai.staminaCur;
				o["oai.cai.staminaMax"] = aco.oai.cai.staminaMax;
				o["oai.cai.manaCur"] = aco.oai.cai.manaCur;
				o["oai.cai.manaMax"] = aco.oai.cai.manaMax;
			}	
			
			if (aco.oai.ibi != null) {	//  Item Basic Information 
				o["oai.ibi.citemSalvaged"] = aco.oai.ibi.citemSalvaged;
				o["oai.ibi.szDescSimple"] = aco.oai.ibi.szDescSimple;
				o["oai.ibi.szDescDetailed"] = aco.oai.ibi.szDescDetailed;
				o["oai.ibi.szComment"] = aco.oai.ibi.szComment;
				o["oai.ibi.szInscription"] = aco.oai.ibi.szInscription;
				o["oai.ibi.szInscriber"] = aco.oai.ibi.szInscriber;
				o["oai.ibi.cpageTotal"] = aco.oai.ibi.cpageTotal;
				o["oai.ibi.cpageUsed"] = aco.oai.ibi.cpageUsed;
				
				o["oai.ibi.manaCur"] = aco.oai.ibi.manaCur;
				o["oai.ibi.fractEfficiency"] = aco.oai.ibi.fractEfficiency;
				o["oai.ibi.probDestroy"] = aco.oai.ibi.probDestroy;
				o["oai.ibi.skidWieldReq"] = aco.oai.ibi.skidWieldReq;
				o["oai.ibi.lvlWieldReq"] = aco.oai.ibi.lvlWieldReq;
				o["oai.ibi.fractManaConvMod"] = aco.oai.ibi.fractManaConvMod;
				o["oai.ibi.vitalRestored"] = aco.oai.ibi.vitalRestored;
				o["oai.ibi.dlvlRestored"] = aco.oai.ibi.dlvlRestored;
				o["oai.ibi.fractRestorationBonus"] = aco.oai.ibi.fractRestorationBonus;
				o["oai.ibi.manaCost"] = aco.oai.ibi.manaCost;
				o["oai.ibi.ctink"] = aco.oai.ibi.ctink;
				o["oai.ibi.szTinkerer"] = aco.oai.ibi.szTinkerer;
				o["oai.ibi.szImbuer"] = aco.oai.ibi.szImbuer;
				o["oai.ibi.szCreator"] = aco.oai.ibi.szCreator;
				o["oai.ibi.ckeyOnRing"] = aco.oai.ibi.ckeyOnRing;
				o["oai.ibi.fAttuned"] = aco.oai.ibi.fAttuned;
				o["oai.ibi.fBonded"] = aco.oai.ibi.fBonded;
				o["oai.ibi.fRetained"] = aco.oai.ibi.fRetained;
				o["oai.ibi.fDropOnDeath"] = aco.oai.ibi.fDropOnDeath;
				o["oai.ibi.fDestroyOnDeath"] = aco.oai.ibi.fDestroyOnDeath;
				o["oai.ibi.fUnenchantable"] = aco.oai.ibi.fUnenchantable;
				o["oai.ibi.fSellable"] = aco.oai.ibi.fSellable;
				o["oai.ibi.fIvoryable"] = aco.oai.ibi.fIvoryable;
				o["oai.ibi.fDyeable"] = aco.oai.ibi.fDyeable;
				o["oai.ibi.fUnlimitedUses"] = aco.oai.ibi.fUnlimitedUses;
				o["oai.ibi.rareid"] = aco.oai.ibi.rareid;

				if (aco.oai.ibi.raw != null) {
					var raw = core.dictionaryToObject(aco.oai.ibi.raw);
					Object.keys(raw).forEach(function(key) {
						o["oai.ibi.raw." + Number(key).toString(16)
							.toUpperCase()] = raw[key];
					});
				}
			}

			if (aco.oai.iai != null) {	// Item Armor Information
			
				o["oai.iai.al"] = aco.oai.iai.al;
				o["oai.iai.protSlashing"] = aco.oai.iai.protSlashing;
				o["oai.iai.protPiercing"] = aco.oai.iai.protPiercing;
				o["oai.iai.protBludgeoning"] = aco.oai.iai.protBludgeoning;
				o["oai.iai.protFire"] = aco.oai.iai.protFire;
				o["oai.iai.protAcid"] = aco.oai.iai.protAcid;
				o["oai.iai.protCold"] = aco.oai.iai.protCold;
				o["oai.iai.protElectrical"] = aco.oai.iai.protElectrical;
			}
			
			if (aco.oai.iwi != null) {	//  Item Weapon Information
				o["oai.iwi.dmty"] = aco.oai.iwi.dmty;
				o["oai.iwi.speed"] = aco.oai.iwi.speed;
				o["oai.iwi.skid"] = aco.oai.iwi.skid;
				o["oai.iwi.dhealth"] = aco.oai.iwi.dhealth;
				o["oai.iwi.scaleDamageRange"] = aco.oai.iwi.scaleDamageRange;
				o["oai.iwi.scaleDamageBonus"] = aco.oai.iwi.scaleDamageBonus;
				o["oai.iwi.scaleDefenseBonus"] = aco.oai.iwi.scaleDefenseBonus;
				o["oai.iwi.scaleMissileDBonus"] = aco.oai.iwi.scaleMissileDBonus;
				o["oai.iwi.scaleMagicDBonus"] = aco.oai.iwi.scaleMagicDBonus;
				o["oai.iwi.scaleAttackBonus"] = aco.oai.iwi.scaleAttackBonus;
				o["oai.iwi.scalePvMElemBonus"] = aco.oai.iwi.scalePvMElemBonus;
				o["oai.iwi.scalePvPElemBonus"] = aco.oai.iwi.scalePvPElemBonus;
				o["oai.iwi.dhealthElemBonus"] = aco.oai.iwi.dhealthElemBonus;
				o["oai.iwi.dwHighlights"] = aco.oai.iwi.dwHighlights;
				o["oai.iwi.distRange"] = aco.oai.iwi.distRange;
				o["oai.iwi.vLaunch"] = aco.oai.iwi.vLaunch;
				o["oai.iwi.fCriticalStrike"] = aco.oai.iwi.fCriticalStrike;
				o["oai.iwi.fCripplingBlow"] = aco.oai.iwi.fCripplingBlow;
				o["oai.iwi.fArmorRending"] = aco.oai.iwi.fArmorRending;
				o["oai.iwi.fSlashRending"] = aco.oai.iwi.fSlashRending;
				o["oai.iwi.fPierceRending"] = aco.oai.iwi.fPierceRending;
				o["oai.iwi.fBludgeonRending"] = aco.oai.iwi.fBludgeonRending;
				o["oai.iwi.fAcidRending"] = aco.oai.iwi.fAcidRending;
				o["oai.iwi.fColdRending"] = aco.oai.iwi.fColdRending;
				o["oai.iwi.fFireRending"] = aco.oai.iwi.fFireRending;
				o["oai.iwi.fLightningRending"] = aco.oai.iwi.fLightningRending;
				o["oai.iwi.fPhantasmal"] = aco.oai.iwi.fPhantasmal;
			}
			
			if (aco.oai.iei != null) {	//  Item Enchantment Information
				o["oai.iei.difficulty"] = aco.oai.iei.difficulty;
				o["oai.iei.spellcraft"] = aco.oai.iei.spellcraft;
				o["oai.iei.manaCur"] = aco.oai.iei.manaCur;
				o["oai.iei.manaMax"] = aco.oai.iei.manaMax;
				o["oai.iei.csecPerMana"] = aco.oai.iei.csecPerMana;
				o["oai.iei.fractEfficiency"] = aco.oai.iei.fractEfficiency;
				o["oai.iei.szRaceReq"] = aco.oai.iei.szRaceReq;
				o["oai.iei.skidReq"] = aco.oai.iei.skidReq;
				o["oai.iei.sklvlReq"] = aco.oai.iei.sklvlReq;
				o["oai.iei.rankReq"] = aco.oai.iei.rankReq;
				o["oai.iei.cspellid"] = aco.oai.iei.cspellid;

				if (aco.oai.iei.cspellid > 0) {
					o["oai.iei._cspells"] = core.getItemSpells(aco).map(function(spell) {
						return spell.spellid;
					});
					o["oai.iei._cspells"].sort();
				}
			}
			
			if (aco.oai.ipi != null) {	//  Item Portal Information
				o["oai.ipi.lvlMin"] = aco.oai.ipi.lvlMin;
				o["oai.ipi.lvlMax"] = aco.oai.ipi.lvlMax;
				o["oai.ipi.szDest"] = aco.oai.ipi.szDest;
				o["oai.ipi.flags"] = aco.oai.ipi.flags;
			}	
		}

		return o;
	};
	
	core.getItemSpells = function getItemSpells(aco) {
		var spells = [];
		var spellID, spell;
		// eslint-disable-next-line no-restricted-syntax
		for (var i = 0; i < aco.oai.iei.cspellid; i++) {
			spellID = aco.oai.iei.rgspellid(i);
			spell = skapi.SpellInfoFromSpellid(spellID);
			spells.push(spell);
		}
		return spells;
	};
	
	// Convert VBScript dictionary to JS object.
	core.dictionaryToObject = function dictionaryToObject(dictionary) {
		var obj = {};
		var keys = new Enumerator(dictionary);
		var key;
		// eslint-disable-next-line no-restricted-syntax
		for (; !keys.atEnd(); keys.moveNext()) {
			key = keys.item();
			obj[key] = dictionary.Item(key);
		}
		return obj;
	};
	
	core.getInventory = function factory() {
		function _isMine(aco) {
			return core.doesAcoBelongsToMe(aco);
		}
		return function getInventory() {
			var acf = skapi.AcfNew();
			acf.olc = olcInventory | olcEquipped;
			var items = core.coacoToArray(acf.CoacoGet());
			return items.filter(_isMine);
		};
	}();


	core.doesAcoBelongsToMe = function doesAcoBelongsToMe(acoItem) {
		if (acoItem.acoWearer && acoItem.acoWearer.oid != skapi.acoChar.oid) {
			return false;
		}
		if (acoItem.acoContainer && acoItem.acoContainer.oty & otyContainer) {
			if (!acoItem.acoContainer.acoContainer || acoItem.acoContainer.acoContainer.oid != skapi.acoChar.oid) {
				return false;
			}
		}
		return true;
	};
	
	core.getAssessedAco = function factory() {
		return function getAssessedAco(params, callback) {
			core.debug("<getAssessedAco>");
			var aco = params.aco;
			
			function finish(err, results) {
				core.debug("<getAssessedAco>|finish", err, results);
				core.clearTimeout(tid);
				core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
				callback = undefined;
			}

			if (aco.oai) {
				core.debug("Already assessed " + aco.szName);
				return finish(undefined, aco);
			}
			
			core.debug("Assessing " + aco.szName, aco.oai);
			var s1 = new Date().getTime();
			core.queueAssessAco({
				aco     : aco,
				priority: 1
			}, function _onAssess(err) {
				core.debug("<_onAssess>", err, (new Date().getTime() - s1));
				if (err) return finish(err);
				finish(undefined, aco);
			});
			
			var tid = core.setTimeout(core.timedOut, 10 * 1000, finish, "GET_ASSESSED_TIMED_OUT");
		};
	}();
	
	core.timedOut = function timedOut(callback, name) {
		callback(new core.Error(name || "TIMED_OUT"));
	};

	core.launchInventorySearch = function factory() {
		function _characterHasInventory(f) {
			//return (f.Type == "File folder" && core.existsSync(f.Path + "/inventory"));
			return (f.Type == "File folder" && core.existsSync(f.Path + "/inventory.json"));
		}
		return function launchInventorySearch(params, callback) {
			core.debug("<launchInventorySearch>");
			var logger = (!params || params.logger === undefined ? core.debug : params.logger);
			
			function finish(err, results) {
				logger("<launchInventorySearch>|finish", err, results);

				//	core.clearTimeout(tid);
				core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
				callback = undefined;
			}

			var charactersPath = _dir + "src/data/worlds/" + skapi.szWorld + "/characters";
			var folders = core.readdirSync2(charactersPath).filter(_characterHasInventory);
			
			var caches = {};

			/*
		core.inventory = new core.Cache({
			path      : _dir + "src/data/worlds/" + skapi.szWorld + "/characters/" + skapi.acoChar.szName + "/inventory",
			filePerKey: true
		}).load();
		*/


			var view = core.createView({
				title : "Inventory Search", 
				width : 300, 
				height: 300
			});
		
			var layout = new SkunkSchema.FixedLayout({
				parent: view
			});

			var choCharacter = new SkunkSchema.Choice({
				parent: layout, 
				name  : "choCharacter"
			})
				.setWidth(150)
				.addOption({text: "All Characters"})
				.setSelected(0)
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					//if (value == "Profile...") return;
					refreshList();
				});
				
			folders.forEach(function(f) {
				choCharacter.addOption({text: f.Name});
			});
			
			var choProfile = new SkunkSchema.Choice({
				parent: layout, 
				name  : "choProfile"
			})
				.setWidth(150)
				.setAnchor(choCharacter, "TOPRIGHT")
				.addOption({text: "Profile..."})
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					if (value == "Profile...") return;
					refreshList();
				});
			
			function refreshList() {
				lstResults.clear();
				var cv = choCharacter.getValue();
				var profileName = choProfile.getValue();
				if (!profileName || profileName == "Profile...") return;
				
				//Melee Wand 0
				var profile = core.getProfileByName(profileName);
				if (!profile) {
					return core.warn("Couldn't find profile data for " + profileName + "?");
				}

				var characters = [];
				if (cv == "All Characters") {
					characters = folders.slice();
				} else {
					var file = folders.find(function(f) {
						return f.Name == cv;
					})
					if (!file) {
						core.warn("Can't find " + cv + "'s inventory file.");
						return;
					}
					characters.push(file);
				}

				characters.forEach(function(file) {
					
					var inventoryPath = file.Path + "/inventory.json";
					var oids = core.loadJsonFile({path: inventoryPath});
					if (!oids || !oids.length) return;
					logger("inventoryPath: " + inventoryPath, "oids: " + oids.length);

					var items = oids.map(function(oid) {
						return core.itemStats.get(oid);
					});

					var matches = items.filter(function(flat) {
						return profile.evaluateRulesSync({aco: flat});
					});

					logger("matches: " + matches.length);
					if (matches.length == 0) {
						core.info("No matches found.");
						return;
					}
					var rules = profile.rules;
					
					logger("rules1: " + rules.length);
					
					matches.sort(function(a, b) {
						return profile.compare(a, b);
					});
					
					
					/*
					var compareRules = profile.rules.filter(function(rule) {
						return rule.data.operator == ">=" || rule.data.operator == ">" || rule.data.operator == "<" || rule.data.operator == "<=";
					});
					
					
					logger("compareRules: " + compareRules.length);
					if (compareRules.length > 0) {
						var start = new Date().getTime();
						matches.sort(function(a, b) {
							var rule, property;
							for (var i = 0; i < compareRules.length; i++) {
								rule = compareRules[i];
								property = _properties[rule.data.propertyId];
								if (rule.data.operator == ">" || rule.data.operator == ">=") {
									if (property.getValue(a) > property.getValue(b)) return -1;
									if (property.getValue(a) < property.getValue(b)) return 1;
								} else if (rule.data.operator == "<" || rule.data.operator == "<=") {
									if (property.getValue(a) < property.getValue(b)) return -1;
									if (property.getValue(a) > property.getValue(b)) return 1;
								}
							}
							return 0;
						});
						var elapsed = new Date().getTime() - start;
						logger("sort time: " + elapsed + "ms.");
					}

*/
					

					matches.forEach(function(flat, i) {
						lstResults.addRow(flat.oid, file.Name, icon_textures + flat.icon, flat.szName); //  + ": " + _properties["scalePvMElemBonus"].getValue(flat) core.numberWithCommas(flat.cpyValue)
					});
					
					logger("Done");
				});
			};
			
			_loadedProfiles.forEach(function(p) {
				choProfile.addOption({text: p.getName()});
			});
			
			var lstResults = new SkunkSchema.List({
				parent: layout, 
				name  : "lstResults"
			})
				.setAnchor(choCharacter, "BOTTOMLEFT")
				.setWidth(view.getWidth())
				.setHeight(view.getHeight() - 40)
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 0}) // oid
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 125}) // character
				.addColumn({progid: "DecalControls.IconColumn"}) // item icon
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 125}) // item name
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 30}) // Needed tinks
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					if (szControl == "lstResults") {
						var intCol = parseInt(value.split(",")[0]);
						var intRow = parseInt(value.split(",")[1]);
					
						var row = control.rows[intRow];
						var oid = row[0];
						var aco = skapi.AcoFromOid(oid);
						if (aco) {
							skapi.SelectAco(aco);
						}
					}
				});

			// Footer.
			var btnExit = new SkunkSchema.PushButton({
				parent: layout, 
				name  : "btnExit", 
				text  : "Exit"
			})
				.setAnchor(layout, "BOTTOMRIGHT", "BOTTOMRIGHT")
				.setWidth(30)
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					view.removeControls();
					callback();
				});

			view.showControls(true);
		};
	}();
	var icon_textures = 0x6000000; // Icon textures begin at 0x6000000.
		
	core.isNumeric = function isNumeric(n) {
		return !isNaN(parseFloat(n)) && isFinite(n);
	};
		
	return core;
}));

if (typeof LootProfile34 !== "undefined") {
	main = LootProfile34.main;
}

@echo off
set /p version="Version: "

"C:\Program Files\7-Zip\7z.exe" a ../LootProfile3_%version%.zip ../LootProfile3/ -xr!.git -xr!logs -xr!zip.bat -xr!.gitmodules -xr!.gitignore -x!LootProfile3/src/data/profiles/* -x!LootProfile3/src/data/inventory/* -x!LootProfile3/src/data/palettes/* -x!LootProfile3/src/data/worlds/*
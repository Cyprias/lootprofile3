/********************************************************************************************\
	File Name:      LibDisappeared.js
	Purpose:        Fires callback when a object disappears from memory.
	Creator:        Cyprias
	Date:           06/25/2016
	License:        MIT License	(http://opensource.org/licenses/MIT)
\*********************************************************************************************/

var MAJOR = "LibDisappeared-1.0";
var MINOR = 190108;

(function (factory) {
	
	var params = {};
	params.EventEmitter = (typeof EventEmitter !== "undefined" && EventEmitter) || require("SkunkSuite\\EventEmitter");
	if (typeof params.EventEmitter === "undefined") {
		throw new Error("EventEmitter.js isn't loaded.");
		return;
	}
	
    if (typeof module === 'object' && module.exports) {
        module.exports = factory( params );
	} else {
        LibDisappeared10 = factory( params );
	}
}(function ( params ) {
	var core;
	if (typeof LibStub !== "undefined") {
		core = LibStub.newLibrary(MAJOR, MINOR);
		if (!core) return LibStub(MAJOR);
	} else {
		core = {};
	}
	
	var debugging = false;

	var EventEmitter = params.EventEmitter;
	core.emitter = core.emitter || new EventEmitter();
	
	
	function fConsole(szMsg) {
		/******************************************************************************\
			fConsole: Print a message to console.
		\******************************************************************************/
		skapi.OutputSz(szMsg + "\n", opmConsole);
	};
		
	core.debug = function debug(szMsg) {
		/******************************************************************************\
			fDebug: Print a message to console if debugging is enabled.
		\******************************************************************************/
		if (debugging == true) {
			fConsole("[LibDisappeared.js] "+szMsg);
		}
	};

	core.objects = core.objects || [];
	var objects = core.objects;
	
	core.handler = core.handler || {};
	var handler = core.handler;
	handler.OnObjectCreate = function OnObjectCreate(aco) {
		core.debug("<OnObjectCreate> " + aco);
		if (objects.indexOf(aco.oid) > 0) return;
		objects.push(aco.oid);
	};
	
	handler.OnObjectCreatePlayer = function OnObjectCreatePlayer(aco) {
		core.debug("<OnObjectCreatePlayer> " + aco);
		if (objects.indexOf(aco.oid) > 0) return;
		objects.push(aco.oid);
	};
	
	handler.OnObjectDestroy = function OnObjectDestroy(aco) {
		for (var i=objects.length-1; i>=0; i--) {
			if (objects[i] == aco.oid) {
				core.debug("Removing " + aco.szName);
				objects.splice(i, 1);
				core.emitter.emit("Destroyed", aco.oid);
				break;
			}
		}
	};
	
	handler.OnTimer = function OnTimer(t) {
		if (t.tag != timer.tag) return;
		core.debug("<OnTimer> " + t.tag);
		t.cmsec = -1000;
		
		var oid;
		var aco;
		for (var i=(objects.length-1); i>=0; i--) {
			oid = objects[i];
			aco = skapi.AcoFromOid(oid);
			if (!aco || !aco.fExists) {
				objects.splice(i, 1);
				core.emitter.emit("Disappeared", oid);
			}
		}
	};
	
	
	skapi.AddHandler(evidOnObjectCreate, handler);
	skapi.AddHandler(evidOnObjectCreatePlayer, handler);
	skapi.AddHandler(evidOnTimer, handler);
	skapi.AddHandler(evidOnObjectDestroy, handler);
	

	var timer = skapi.TimerNew();
	timer.tag = "LibDisappeared";
	timer.cmsec = -1000;

	return core;
}));
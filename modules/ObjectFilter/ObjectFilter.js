/********************************************************************************************\
	File Name:      ObjectFilter.js
	Purpose:        Capture data received during Create Object.
	Creator:        Cyprias
	Date:           04/05/2015
	License:        MIT License	(http://opensource.org/licenses/MIT)
\*********************************************************************************************/

var MAJOR = "ObjectFilter-1.0";
var MINOR = 190105;

(function (factory) {
	var params = {};
	params.EventEmitter = (typeof EventEmitter !== "undefined" && EventEmitter) || require("SkunkSuite\\EventEmitter");
	if (typeof params.EventEmitter === "undefined") {
		throw new Error("EventEmitter.js isn't loaded.");
		return;
	}
	
    if (typeof module === 'object' && module.exports) {
        module.exports = factory( params );
	} else {
        ObjectFilter10 = factory( params );
	}
}(function ( params ) {
	var core;
	if (typeof LibStub !== "undefined") {
		core = LibStub.newLibrary(MAJOR, 190105);
		if (!core) return LibStub(MAJOR);
	} else {
		core = {};
	}

	var EventEmitter = params.EventEmitter;
	core.emitter = core.emitter || new EventEmitter();

	var mtyCreateObject     = 0xF7450000 - 0x100000000;
	var mtyUpdateObject     = 0xF7DB0000 - 0x100000000;

	var ignoreInPortalSpace = false; // Ignore objects when in portal space (like arriving in a dense area).
	
	var debugging = false;
	function fDebug(szMsg) {
		/******************************************************************************\
			fDebug: Print a message to console.
		\******************************************************************************/
		if (debugging == true) {
			skapi.OutputSz("[ObjectFilter] " + szMsg + "\n", opmConsole);
		}
	}
	
	function fError(szMsg) {
		/******************************************************************************\
			fError: Print a message to console
		\******************************************************************************/
		skapi.OutputSz("[ObjectFilter] " + szMsg + "\n", opmConsole);
	}
	
	function fSetDebugging(value) {
		/******************************************************************************\
			fSetDebugging: Set the debugging state of the module.
		\******************************************************************************/
		debugging = value;
	}
	
	core.setIngoreInPortalSpace = function fSetIngoreInPortalSpace(value) {
		ignoreInPortalSpace = value;
	};

	function fExtractPalettes(mbuf, paletteCount) {
		var palettes = [];

		var p;
		for ( var i = 0; i < paletteCount; i++ ) {
			p = {};
			p.palette     = mbuf.Get_PackedDWORD();
			p.offset      = mbuf.Get_BYTE();
			p.length      = mbuf.Get_BYTE();
			palettes.push(p);
		}

		return palettes;
	}

	function fExtractTextures(mbuf, textureCount) {
		var textures = [];
		var texture;
		for ( var i = 0; i < textureCount; i++ ) {
			texture = {};
			texture.index = mbuf.Get_BYTE();
			texture.old = mbuf.Get_PackedDWORD();
			texture["new"] = mbuf.Get_PackedDWORD();
			textures.push(texture);
		}
		return textures;
	}

	function fExtractModels(mbuf, modelCount) {
		var models = [];
		var model;
		for ( var i = 0; i < modelCount; i++ ) {
			model = {};
			model.index = mbuf.Get_BYTE();
			model.model = mbuf.Get_PackedDWORD();
			models.push(model);
		}
		return models;
	}
	
	function fExtractModelData(mbuf) {
		var data = {};

		data.eleven         = mbuf.Get_BYTE();
		data.paletteCount   = mbuf.Get_BYTE();
		data.textureCount   = mbuf.Get_BYTE();
		data.modelCount     = mbuf.Get_BYTE();
		
		if ( data.paletteCount & 0xFF ) {
			data.palette = mbuf.Get_PackedDWORD();
		}
		
		if (data.paletteCount > 0) {
			data.palettes = fExtractPalettes(mbuf, data.paletteCount);
		}
		
		if (data.textureCount > 0) {
			data.textures = fExtractTextures(mbuf, data.textureCount);
		}
		
		if (data.modelCount > 0) {
			data.models = fExtractModels(mbuf, data.modelCount);
		}

		mbuf.Align_DWORD();
		return data;
	}
	
	function fExtractPosition0(mbuf) {
		var data = {};
		data.landcell   = mbuf.Get_DWORD();
		data.x          = mbuf.Get_float();
		data.y          = mbuf.Get_float();
		data.z          = mbuf.Get_float();
		data.wQuat      = mbuf.Get_float();
		data.xQuat      = mbuf.Get_float();
		data.yQuat      = mbuf.Get_float();
		data.zQuat      = mbuf.Get_float();
		return data;
	}
	
	function fExtractPhysicsData(mbuf) {
		var data = {};
		
		data.flags      = mbuf.Get_DWORD();
		data.unknown    = mbuf.Get_DWORD();
		
		if (data.flags & 0x00010000) {
			data.byteCount = mbuf.Get_DWORD();
			if (data.byteCount > 0) {
				data.bytes = [];
				var _byte;
				for ( var i = 0; i < data.byteCount; i++ ) {
					_byte = mbuf.Get_BYTE();
					data.bytes.push(_byte);
				}
			}
			
			data.unknown100000 = mbuf.Get_DWORD();
		}
		
		if (data.flags & 0x00020000) {
			data.unknown20000 = mbuf.Get_DWORD();
		}
		
		if (data.flags & 0x00008000) {
			data.position = fExtractPosition0(mbuf);
		}
		
		if (data.flags & 0x00000002) {
			data.animations = mbuf.Get_DWORD();
		}
		
		if (data.flags & 0x00000800) {
			data.sounds = mbuf.Get_DWORD();
		}
		
		if (data.flags & 0x00001000) {
			data.unknown1000 = mbuf.Get_DWORD();
		}
		
		if (data.flags & 0x00000001) {
			data.model = mbuf.Get_DWORD();
		}
		
		if (data.flags & 0x00000020) {
			data.equipper = mbuf.Get_DWORD();
			data.equipperSlot = mbuf.Get_DWORD();
		}
		
		if (data.flags & 0x00000040) {
			data.equippedCount = mbuf.Get_DWORD();
			if (data.equippedCount > 0) {
				data.equipped = [];
				var e;
				for ( var i = 0; i < data.equippedCount; i++ ) {
					e = {};
					e.item = mbuf.Get_DWORD();
					e.slot = mbuf.Get_DWORD();
					data.equipped.push(e);
				}
			}
		}
		
		if (data.flags & 0x00000080) {
			data.scale = mbuf.Get_float();
		}
		
		if (data.flags & 0x00000100) {
			data.unknown100 = mbuf.Get_DWORD();
		}
		
		if (data.flags & 0x00000200) {
			data.unknown200 = mbuf.Get_DWORD();
		}
		
		if (data.flags & 0x00040000) {
			data.unknown40000 = mbuf.Get_float();
		}
		
		if (data.flags & 0x00000004) {
			data.dx = mbuf.Get_float();
			data.dy = mbuf.Get_float();
			data.dz = mbuf.Get_float();
		}
		
		if (data.flags & 0x00000008) {
			data.unknown8_1 = mbuf.Get_float();
			data.unknown8_2 = mbuf.Get_float();
			data.unknown8_3 = mbuf.Get_float();
		}
		
		if (data.flags & 0x00000010) {
			data.rx = mbuf.Get_float();
			data.ry = mbuf.Get_float();
			data.rz = mbuf.Get_float();
		}
		
		if (data.flags & 0x00002000) {
			data.unknown2000 = mbuf.Get_DWORD();
		}
		
		if (data.flags & 0x00004000) {
			data.unknown4000 = mbuf.Get_DWORD();
		}
		
		data.unknown1 = mbuf.Get_WORD();
		data.unknown2 = mbuf.Get_WORD();
		data.unknown3 = mbuf.Get_WORD();
		data.unknown4 = mbuf.Get_WORD();
		data.unknown5 = mbuf.Get_WORD();
		data.unknown6 = mbuf.Get_WORD();
		data.unknown7 = mbuf.Get_WORD();
		data.unknown8 = mbuf.Get_WORD();
		data.unknown9 = mbuf.Get_WORD();
			
		mbuf.Align_DWORD();
		
		return data;
	}
	
	function fExtractDwellingACL(mbuf) {
		var data = {};
		data.flags          = mbuf.Get_DWORD();
		data.open           = mbuf.Get_DWORD();
		data.allegiance     = mbuf.Get_DWORD();
		data.guestCount     = mbuf.Get_WORD();
		data.guestLimit     = mbuf.Get_WORD();
		if (data.guestCount > 0) {
			data.guestList = [];
			var g;
			for ( var i = 0; i < data.guestCount; i++ ) {
				g = {};
				g.guest = mbuf.Get_DWORD();
				g.storage = mbuf.Get_DWORD();
				data.guestList.push(g);
			}
		}
		return data;
	}
	
	function fExtractGameData(mbuf) {
		var data = {};
	
		data.flags1 = mbuf.Get_DWORD();
		data.name = mbuf.Get_String();
		data.type = mbuf.Get_PackedDWORD( );
		data.icon = mbuf.Get_PackedDWORD( );
		data.category = mbuf.Get_DWORD();
		data.behavior = mbuf.Get_DWORD();
		
		mbuf.Align_DWORD();

		if (data.behavior & 0x04000000) {
			data.flags2 = mbuf.Get_PackedDWORD( );
		}
		
		if (data.flags1 & 0x00000001) {
			data.namePlural = mbuf.Get_String();
		}
		
		if (data.flags1 & 0x00000002) {
			data.itemSlots = mbuf.Get_BYTE();
		}
		
		if (data.flags1 & 0x00000004) {
			data.packSlots = mbuf.Get_BYTE();
		}
		
		if (data.flags1 & 0x00000100) {
			data.ammunition = mbuf.Get_WORD();
		}
		
		if (data.flags1 & 0x00000008) {
			data.value = mbuf.Get_DWORD();
		}
		
		if (data.flags1 & 0x00000010) {
			data.unknown10 = mbuf.Get_DWORD();
		}
		
		if (data.flags1 & 0x00000020) {
			data.approachDistance = mbuf.Get_float();
		}
		
		if (data.flags1 & 0x00080000) {
			data.usableOn = mbuf.Get_DWORD();
		}
		
		if (data.flags1 & 0x00000080) {
			data.iconHighlight = mbuf.Get_DWORD();
		}
		
		if (data.flags1 & 0x00000200) {
			data.wieldType = mbuf.Get_BYTE();
		}
		
		if (data.flags1 & 0x00000400) {
			data.uses = mbuf.Get_WORD();
		}
		
		if (data.flags1 & 0x00000800) {
			data.usesLimit = mbuf.Get_WORD();
		}
		
		if (data.flags1 & 0x00001000) {
			data.stack = mbuf.Get_WORD();
		}
		
		if (data.flags1 & 0x00002000) {
			data.stackLimit = mbuf.Get_WORD();
		}
		
		if (data.flags1 & 0x00004000) {
			data.container = mbuf.Get_DWORD();
		}
		
		if (data.flags1 & 0x00008000) {
			data.equipper = mbuf.Get_DWORD();
		}
		
		if (data.flags1 & 0x00010000) {
			data.equipPossible = mbuf.Get_DWORD();
		}
		
		if (data.flags1 & 0x00020000) {
			data.equipActual = mbuf.Get_DWORD();
		}
		
		if (data.flags1 & 0x00040000) {
			data.coverage = mbuf.Get_DWORD();
		}
		
		if (data.flags1 & 0x00100000) {
			data.unknown100000 = mbuf.Get_BYTE();
		}
		
		if (data.flags1 & 0x00800000) {
			data.unknown800000 = mbuf.Get_BYTE();
		}
		
		if (data.flags1 & 0x08000000) {
			data.unknown8000000 = mbuf.Get_WORD();
		}
		
		if (data.flags1 & 0x01000000) {
			data.workmanship = mbuf.Get_float();
		}
		
		if (data.flags1 & 0x00200000) {
			data.burden = mbuf.Get_WORD();
		}
		
		if (data.flags1 & 0x00400000) {
			data.spell = mbuf.Get_WORD();
		}
		
		if (data.flags1 & 0x02000000) {
			data.owner = mbuf.Get_DWORD();
		}
		
		if (data.flags1 & 0x04000000) {
			data.acl = fExtractDwellingACL(mbuf);
		}
		
		if (data.flags1 & 0x20000000) {
			data.hookTypeUnknown = mbuf.Get_WORD();
			data.hookType = mbuf.Get_WORD();
		}
		
		if (data.flags1 & 0x00000040) {
			data.monarch = mbuf.Get_DWORD();
		}
		
		if (data.flags1 & 0x10000000) {
			data.hookableOn = mbuf.Get_WORD();
		}
		
		if (data.flags1 & 0x40000000) {
			data.iconOverlay = mbuf.Get_PackedDWORD();
		}

		if (data.flags2) {
			if (data.flags2 & 0x00000001) {
				data.iconUnderlay = mbuf.Get_PackedDWORD();
			}
		}
		
		if (data.flags1 & 0x80000000) {
			data.material = mbuf.Get_DWORD();
		}
		
		if (data.flags2) {
			if (data.flags2 & 0x00000002) {
				data.cooldownId = mbuf.Get_DWORD();
			}
			
			if (data.flags2 & 0x00000004) {
				data.cooldown_unk2 = mbuf.Get_DWORD();
				data.cooldown_unk3 = mbuf.Get_WORD();
				data.cooldown_unk4 = mbuf.Get_WORD();
			}
			
			if (data.flags2 & 0x00000008) {
				data.cooldown_unk5 = mbuf.Get_DWORD();
			}
		}
		
		mbuf.Align_DWORD();
		return data;
	}

	var handler = {};
	handler.OnRawServerMessage = function fOnRawServerMessage(mty, mbuf) {
		/******************************************************************************\
			fOnRawServerMessage: Process raw server messages.
		\******************************************************************************/

		if (_callbacks.length == 0) return;

		if (ignoreInPortalSpace == true && skapi.plig == pligInPortal) {
			return;
		}

		switch(mty) {
			case mtyCreateObject:
			case mtyUpdateObject: //mtyCreateObject & mtyUpdateObject have the same data.
				var payload = {};
				
				// Skip past the header.
				mbuf.SkipCb(4);

				var object = mbuf.Get_DWORD();
				//var payload = cachedObjects[object] || {};
				var payload = {};
				
				// Get the item's oid.
				payload.object = object

				//fDebug("object: " + object + " " + skapi.AcoFromOid(object));

				// Extract ModelData containing palettes, textures and models.
				payload.model = fExtractModelData(mbuf);

				// Extract PhysicsData.
				payload.physics = fExtractPhysicsData(mbuf);
				
				// Extract GameData.
				payload.game = fExtractGameData(mbuf);

				fFireCallback(payload);
				break;
		}
	};
	
	handler.OnStartPortalSelf = function OnStartPortalSelf(aco) {
		//ObjectFilter.removeCallback(fRawCreateObject);
	};
	
	handler.OnEndPortalSelf = function OnEndPortalSelf() {
		//ObjectFilter.addCallback(fRawCreateObject);
	};

	var _callbacks = [];
	core.addCallback = function fAddCallback(method, thisArg) {
		/******************************************************************************\
			fAddCallback: Add a evid callback.
		\******************************************************************************/
		_callbacks.push({method:method, thisArg:thisArg});
		if (_callbacks.length == 1) {
			skapi.AddHandler(mtyCreateObject,           handler);
			skapi.AddHandler(mtyUpdateObject,           handler);
		}
	};
	
	core.removeCallback = function fRemoveCallback(method, thisArg) {
		/******************************************************************************\
			fRemoveCallback: Remove a callback function.
		\******************************************************************************/
		for (var i=_callbacks.length-1; i>=0; i--) {
			if (_callbacks[i].method == method) {
				if (thisArg && thisArg != _callbacks[i].thisArg) continue;
				_callbacks.splice(i,1);
			}
		}
		if (_callbacks.length == 0) {
			skapi.RemoveHandler(mtyCreateObject,  handler);
			skapi.RemoveHandler(mtyUpdateObject,  handler);
		}
	}
	
	function fFireCallback() {
		/******************************************************************************\
			fFireCallback: Fire a registered callback.
		\******************************************************************************/
	//	fDebug(_callbacks.length + " callbacks to fire.");
		for ( var i=_callbacks.length-1; i >= 0; i-- ) {
			_callbacks[i].method.apply(_callbacks[i].thisArg, arguments);
		}
	};
	
	return core;
}));